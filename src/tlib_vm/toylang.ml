(* a toy compiler emitting VM code *)

(* TODO:
   - fix exceptions
   - pre-linked global variables
   - free_reg mutable?
   - support primitives: $(Primitive.name) calls it
   - "return" from an inner section
   - mark whether auto-currying is ok (curry.name)
   - named arguments
   - export w/o arg
   - what to do with computed exports?
   - make sequences behave as in omake ("7.3 Merging")
   - active values, lazy values
   - functions that can be called with different numbers of args
     (e.g. split)
 *)

open Omake_vm.Types
open Omake_vm.Vm
open Omake_vm
open Printf


type statement =
  | Assign of { var : string; vartype : vartype; expr : expr }
  | Section of block
  | Export of string list
  | Return of expr

 and block =
   statement list

 and expr =
   | Abstract of { params : string list; body : block }
   | Apply of { var : string; args : expr list }
   | Seq of expr list
   | Int of int
   | Str of string

 and vartype =
   | Static
   | Dynamic

module SMap = Map.Make(String)
module SSet = Set.Make(String)

type env = envvar SMap.t  (* maps from var name to position in environment *)
 and envvar =
   { env_rd : int;
     env_wr : int option    (* only for exported variables *)
   }

type funs =
  { mutable functions : closure option Lookup_table.t }

type reg_manager =
  { free_reg : int ref;    (* the first free register in the reg bank *)
    num_reg : int ref;     (* number of registers in the reg bank *)
    glb_reg : int;         (* the register with the globals *)
    outer_glb_reg : int option; (* globals of the surrounding section *)
    static : int SMap.t;   (* maps name of static var from cur frame to reg *)
    outer : int SMap.t;    (* maps name of static var from outer frame to reg *)
    num_funs : int ref;
    alloc : alloc;
    funs : funs
  }

let domain map =
  SMap.fold
    (fun key _ set -> SSet.add key set)
    map
    SSet.empty

let rec used_static_vars_of_block static block =
  (* return the subset of [static] that is really accessed in [block],
     assuming that [static] are the static vars around [block] (i.e. the
     potentially free vars in [block])
   *)
  match block with
    | Assign { var; vartype=Static; expr } :: block' ->
        let s1 = used_static_vars_of_expr static expr in
        let s2 = used_static_vars_of_block (SSet.remove var static) block' in
        SSet.union s1 s2
    | Assign { var; vartype=Dynamic; expr } :: block' ->
        let s1 = used_static_vars_of_expr static expr in
        let s2 = used_static_vars_of_block static block' in
        SSet.union s1 s2
    | Section block :: block' ->
        let s1 = used_static_vars_of_block static block in
        let s2 = used_static_vars_of_block static block' in
        SSet.union s1 s2
    | Export _ :: block' ->
        used_static_vars_of_block static block'
    | Return expr :: _ ->
        used_static_vars_of_expr static expr
    | [] ->
        SSet.empty

and used_static_vars_of_expr static expr =
  match expr with
    | Abstract { params; body } ->
        let static' =
          List.fold_left
            (fun set param -> SSet.remove param set)
            static
            params in
        used_static_vars_of_block static' body
    | Apply { var; args } ->
        let s1 =
          if SSet.mem var static then SSet.singleton var else SSet.empty in
        List.fold_left
          (fun set arg -> SSet.union set (used_static_vars_of_expr static arg))
          s1
          args
    | Seq exprs ->
        List.fold_left
          (fun set arg -> SSet.union set (used_static_vars_of_expr static arg))
          SSet.empty
          exprs
    | Int _ | Str _ ->
        SSet.empty

let exported_vars block =
  SSet.of_list
    (List.flatten
       (List.map
          (function
           | Export names -> names
           | _ -> []
          )
          block
       )
    )

let temp_regs rm n =
  rm.num_reg := max !(rm.num_reg) (!(rm.free_reg) + n - 1);
  !(rm.free_reg)

let new_reg rm =
  let r = !(rm.free_reg) in
  incr rm.free_reg;
  rm.num_reg := max !(rm.num_reg) (r+1);
  r

let rec transl_block rm fun_arity env block =
  List.fold_left
    (fun (instr, rm) st ->
      let (st_instr, st_rm) = transl_statement rm fun_arity env st in
      (instr @ st_instr, st_rm)
    )
    ([], rm)
    block

and transl_function rm env params block =
  if not (List.exists (function Return _ -> true | _ -> false) block) then
    failwith "transl_function: no Return statement";
  let arity = List.length params in
  let static =
    List.fold_left
      (fun map (reg,param) -> SMap.add param reg map)
      SMap.empty
      (List.mapi (fun i param -> (i+1,param)) params) in
  let rm_sub =
    { free_reg = ref (arity+1);
      num_reg = ref (arity+1);
      glb_reg = 0;
      outer_glb_reg = None;
      static;
      outer = SMap.empty;
      num_funs = rm.num_funs;
      alloc = rm.alloc;
      funs = rm.funs
    } in
  let (instr, rm_out) = transl_block rm_sub arity env block in
  let instr =
    [ I_function { arity_min=arity; arity_max=arity; arity_names=0
                   ; framesize = !(rm_out.num_reg) } ] @ instr in
  let instr =
    Vm.relocate (Array.of_list instr) in
  let fun_name = sprintf "Lfun_%d" !(rm.num_funs) in
  incr rm.num_funs;
  let clos =
    Vm.closure
      ~entry:0 ~arity_min:arity ~arity_max:arity ~autocurry:true
      ~names:[| |] fun_name (Vm.code rm.alloc instr) in
  rm.funs.functions <-
    Lookup_table.add clos.clos_slot (Some clos) rm.funs.functions;
  clos.clos_slot


and transl_statement rm fun_arity env st =
  match st with
    | Assign { var; vartype = Static; expr } ->
        let reg =
          try SMap.find var rm.static
          with Not_found ->
            new_reg rm in
        let instr = transl_expr rm env expr reg in
        let static = SMap.add var reg rm.static in
        let rm = { rm with static } in
        (instr, rm)
    | Assign { var; vartype = Dynamic; expr } ->
        if SMap.mem var rm.static then
          failwith ("Variable assigned as dynamic variable although already declared as static in this scope: " ^ var);
        let treg0 = temp_regs rm 2 in
        let treg1 = treg0 + 1 in
        let instr = transl_expr rm env expr treg0 in
        let instr =
          instr @
            [ I_load { reg=treg1; value=valuebox R_string var };
              I_locglobalvar { reg=treg1; namereg=treg1; glbreg=rm.glb_reg };
              I_addglobalvari { reg=treg0; glbreg=rm.glb_reg; slotreg=treg1 }
            ] in
        (instr, rm)
    | Section block ->
        let old_free_reg = !(rm.free_reg) in
        let glb_reg = new_reg rm in
        let outer =
          SMap.fold
            (fun name reg map ->
              SMap.add name reg map
            )
            rm.static
            rm.outer in
        let outer_glb_reg = Some rm.glb_reg in
        let rm1 =
          { rm with glb_reg; outer_glb_reg; outer; static = SMap.empty } in
        let (instr, rm2) = transl_block rm1 (*CHECK:*)0 env block in
        let instr =
          [ I_move { reg=glb_reg; src_reg = rm.glb_reg } ]
          @ instr in
        rm.free_reg := old_free_reg;
        (instr, rm)
    | Export names ->
        let outer_glb_reg =
          match rm.outer_glb_reg with
            | Some r -> r
            | None -> failwith "Cannot export without surrounding section" in
        let static_names, dynamic_names =
          List.partition
            (fun name ->
              SMap.mem name rm.outer || SMap.mem name env
            )
            names in
        let static_exports =
          List.flatten
            (List.map
               (fun name ->
                 let treg0 = temp_regs rm 2 in
                 let treg1 = treg0 + 1 in
                 let to_dest dest_reg =
                   try
                     (* static-to-static export *)
                     let src_reg = SMap.find name rm.static in
                     [ I_move { reg = dest_reg; src_reg } ]
                   with Not_found ->
                     (* dynamic-to-static export *)
                     [ I_load { reg=treg0; value=valuebox R_string name };
                       I_locglobalvar { reg=treg0; namereg=treg0; glbreg=rm.glb_reg};
                       I_getglobalvari { reg=dest_reg; slotreg=treg0;
                                         glbreg=rm.glb_reg
                                       }
                     ] in
                 match SMap.find name rm.outer with
                   | dest_reg ->
                       to_dest dest_reg
                   | exception Not_found ->
                       ( match (SMap.find name env).env_wr with
                           | None -> assert false
                           | Some envidx ->
                               to_dest treg1 @
                                 [ I_envmove { envidx; input = treg1 } ]
                       )
               )
               static_names
            ) in
        let dynamic_exports =
          List.flatten
            (List.map
               (fun name ->
                 try
                   (* static-to-dynamic export *)
                   let src_reg = SMap.find name rm.static in
                   let treg0 = temp_regs rm 1 in
                   [ I_load { reg=treg0; value=valuebox R_string name };
                     I_locglobalvar { reg=treg0; namereg=treg0;
                                      glbreg=outer_glb_reg};
                     I_addglobalvari { reg=src_reg; slotreg=treg0;
                                       glbreg=outer_glb_reg
                                     }
                   ]
                 with Not_found ->
                   (* dynamic-to-dynamic export *)
                   let treg0 = temp_regs rm 2 in
                   let treg1 = treg0 + 1 in
                   [ I_load { reg=treg0; value=valuebox R_string name };
                     I_locglobalvar { reg=treg0; namereg=treg0;
                                      glbreg=rm.glb_reg};
                     I_getglobalvari { reg=treg0; slotreg=treg0;
                                       glbreg=rm.glb_reg
                                     };
                     I_load { reg=treg0; value=valuebox R_string name };
                     I_locglobalvar { reg=treg1; namereg=treg1;
                                      glbreg=outer_glb_reg};
                     I_addglobalvari { reg=treg0; slotreg=treg1;
                                       glbreg=outer_glb_reg
                                     }
                   ]
               )
               dynamic_names
            ) in
        (static_exports @ dynamic_exports, rm)
    | Return expr ->
        if fun_arity = 0 then
          failwith "Return not allowed here";
        let reg = new_reg rm in
        let instr = transl_expr rm env expr reg in
        let instr =
          instr @ [ I_return { reg; glbreg=rm.glb_reg } ] in
        (instr, rm)

and transl_expr rm env expr reg =
  match expr with
    | Abstract { params; body } ->
        let old_free_reg = !(rm.free_reg) in
        let exported =
          exported_vars body in
        let exported_to_static =
          SSet.inter (domain rm.static) exported in
        let surround_static =
          SSet.union
            (SSet.union (domain rm.static) (domain rm.outer))
            (domain env) in
        let body_static =
          List.fold_left
            (fun set param -> SSet.remove param set)
            surround_static
            params in
        let used_static =
          used_static_vars_of_block body_static body in
        let all_static =
          (* these need to be put into the environment: *)
          SSet.union used_static exported_to_static in
        let env_info, env_regs, _ =
          List.fold_left
            (fun (infos,regs,i) var ->
              let (reg_rd,instr_rd) =
                transl_var ~nodynamic:true rm env var in
              if SSet.mem var exported_to_static then
                let reg_wr = new_reg rm in
                let instr_wr = [ I_envind { reg=reg_wr; indreg=reg_rd } ] in
                let e = { env_rd = i; env_wr = Some(i+1) } in
                ( (var,e,instr_rd@instr_wr) :: infos,
                  reg_wr :: reg_rd :: regs,
                  i+2
                )
              else
                let e = { env_rd = i; env_wr = None } in
                ( (var,e,instr_rd) :: infos, reg_rd :: regs, i+1 )
            )
            ([], [], 0)
            (SSet.elements all_static) in
        let env_regs = List.rev env_regs in
        let env_reg = new_reg rm in
        let env_instrs =
          List.flatten
            (List.map (fun (_,_,instr) -> instr) env_info)
          @ [ I_env { reg=env_reg; inputs=Array.of_list env_regs } ] in
        let body_env =
          List.fold_left
            (fun map (var,e,_) -> SMap.add var e map)
            SMap.empty
            env_info in
        let clos_slot = transl_function rm body_env params body in
        let closreg = new_reg rm in
        rm.free_reg := old_free_reg;
        env_instrs @
          [ I_getsharedfun { reg=closreg; glbreg=rm.glb_reg; slot=clos_slot };
            I_closure { reg; closreg; env=env_reg }
          ]
    | Apply { var; args=[] } ->
        let old_free_reg = !(rm.free_reg) in
        let (var_reg, instr) = transl_var rm env var in
        rm.free_reg := old_free_reg;
        instr @ [ I_move { reg; src_reg=var_reg } ]
    | Apply { var; args } ->
        (* TODO: transl to I_applyjump when in tail position *)
        let old_free_reg = !(rm.free_reg) in
        let l =
          List.map
            (fun expr ->
              let reg = new_reg rm in
              let instr = transl_expr rm env expr reg in
              (instr, reg)
            )
            args in
        let l_instr =
          List.flatten (List.map fst l) in
        let l_reg =
          Array.of_list (List.map snd l) in
        let closreg, closinstr = transl_var rm env var in
        rm.free_reg := old_free_reg;
        l_instr @
          closinstr @
            [ I_apply { reg; closreg; glbreg=rm.glb_reg; args=l_reg; named=[||] } ]
    | Seq exprs ->
        let old_free_reg = !(rm.free_reg) in
        let l =
          List.map
            (fun expr ->
              let reg = new_reg rm in
              let instr = transl_expr rm env expr reg in
              (instr, reg)
            )
            exprs in
        let l_instr =
          List.flatten (List.map fst l) in
        let l_reg =
          Array.of_list (List.map snd l) in
        rm.free_reg := old_free_reg;
        l_instr @
          [ I_seq { reg; inputs = l_reg } ]
    | Int n ->
        let instr =
          [ I_load { reg; value = valuebox R_int n } ]  in
        instr
    | Str s ->
        let instr =
          [ I_load { reg; value = valuebox R_string s } ]  in
        instr

and transl_var ?(nodynamic=false) rm env var =
  try
    let reg =
      try SMap.find var rm.static
      with Not_found -> SMap.find var rm.outer in
    (reg, [])
  with
    | Not_found ->
        try
          let envidx = (SMap.find var env).env_rd in
          let reg = new_reg rm in
          (reg, [ I_getenv { reg; envidx } ])
        with
          | Not_found ->
              assert(not nodynamic);
              let reg = new_reg rm in
              let instr =
                [ I_load { reg; value = valuebox R_string var };
                  I_locglobalvar { reg; namereg=reg; glbreg=rm.glb_reg };
                  I_getglobalvari { reg; slotreg=reg; glbreg=rm.glb_reg };
                ] in
              (reg, instr)

type context =
  { mutable rm : reg_manager;
    mutable glb : global;
    mutable regs : valuebox array;
    mutable code : Vm.instruction array;
  }

let create_context() =
  let alloc = create_alloc() in
  let funs = { functions = Lookup_table.create alloc.alloc_functions None } in
  let rm =
    { free_reg = ref 1;
      num_reg = ref 1;
      glb_reg = 0;
      outer_glb_reg = None;
      static = SMap.empty;
      outer = SMap.empty;
      num_funs = ref 0;
      alloc;
      funs;
    } in
  let sh = Shared.create alloc in
  let glb = Global.create sh in
  let regs = [| valuebox R_global glb |] in
  { rm; glb; regs; code = [| |] }

let run ctx block =
  let instr, rm = transl_block ctx.rm 0 SMap.empty block in
  let instr = instr @ [ I_endsnippet ] in
  let instr = Vm.relocate (Array.of_list instr) in
  let regs_missing = !(rm.num_reg) - Array.length ctx.regs in
  let regs =
    Array.append
      ctx.regs (Array.make regs_missing (valuebox R_uninitialized ())) in
  ctx.regs <- regs;
  let glb = global_of_vbox regs.(0) in
  let code = Vm.code ctx.rm.alloc instr in
  ctx.code <- Vm.instructions code;
  Vm.run_snippet [| |] glb regs code;
  ctx.glb <- global_of_vbox regs.(0)


(*
#use "topfind";;
#require "tlib_vm";;
open Omake_vm;;
open Toylang;;
open Types;;
Printexc.record_backtrace true;;
let ctx = create_context();;
let instr, rm = transl_block ctx.rm 0 SMap.empty [ Assign{var="x"; vartype=Static; expr=Int 5} ];;
run ctx [ Assign{var="x"; vartype=Static; expr=Int 5} ];;

let p1 =
  [ Assign{var="f1";
           vartype=Dynamic;
           expr=Abstract{params=["a"; "b"];
                         body=[ Return(Seq[Apply{var="a"; args=[]};
                                           Apply{var="b"; args=[]}
                                          ]
                                      )
                              ]
                        }
          }
  ];;

run ctx p1;;

let p2 =
  [ Assign{var="x";
           vartype=Static;
           expr=Apply{var="f1"; args=[ Str "u"; Str "v" ] }
          }
  ];;

run ctx p2;;

let p3 =
  [ Assign{var="x";
           vartype=Static;
           expr=Apply{var="f1"; args=[ Str "u" ] }
          };
    Assign{var="y";
           vartype=Static;
           expr=Apply{var="x"; args=[ Str "v" ] }
          }
  ];;

run ctx p3;;

let p4 =
  [ Assign{var="f2";
           vartype=Dynamic;
           expr=Abstract{params=["a"];
                         body=[ Return(Abstract{params=["b"];
                                                body=[ Return(Seq[Apply{var="a"; args=[]};
                                                                  Apply{var="b"; args=[]}])
                                                     ]
                                               }
                                      )
                              ]
                        }
          }
  ];;

run ctx p4;;

let p5 =
  [ Assign{var="x";
           vartype=Static;
           expr=Apply{var="f2"; args=[ Str "u"; Str "v" ] }
          }
  ];;

run ctx p5;;

let p6 =
  [ Assign{
      var="f3";
      vartype=Dynamic;
      expr=Abstract{
        params=["a"];
        body=[
          Assign{
            var="g";
            vartype=Static;
            expr=Abstract{
              params=["b";"c"];
              body=[
                Return(Seq[Apply{var="a";args=[]};
                           Apply{var="b";args=[]};
                           Apply{var="c";args=[]};
                          ])
              ]}
          };
          Return(Apply{var="g";args=[Apply{var="a";args=[]}]})
        ]
      }}
  ];;
run ctx p6;;
let p7 =
  [ Assign{
      var="h";
      vartype=Static;
      expr=Apply{var="f3";args=[Str "u"]}};
    Assign{
      var="x";
      vartype=Static;
      expr=Apply{var="h";args=[Str "v"]}};
  ];;
run ctx p7;;


Vm.instructions(Lookup_table.get 0 ctx.glb.shared.shared_functions).clos_code;;


 *)
