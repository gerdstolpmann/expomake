open Printf

module T = Omake_vm.Types
module Seq = Omake_vm.Seq

let cfg = Omake_vm.Config.standard_config

let compile ?(with_stdlib=false) text =
  let alloc = T.create_alloc() in
  let sh = Omake_vm.Shared.create alloc in
  let glb = Omake_vm.Global.create sh in
  if with_stdlib then
    Omake_dsl.Stdlib.add glb;
  let prog = Omake_ast.Lex.parse_string text in
  let stlist = Omake_dsl.Compiler.compile prog in
  let sn = Omake_ir.Compiler.create_snippet glb in
  Omake_ir.Compiler.compile_snippet sn stlist;
  (sn, glb)

let run (sn, glb) =
  Omake_ir.Compiler.run_snippet glb sn;
  Omake_ir.Compiler.value_of_snippet sn

let cycle ?with_stdlib text =
  run (compile ?with_stdlib text)

let string_list v =
  List.map (T.string_of_stringval) (Seq.to_list (T.seq_of_vbox cfg v))

let debug_slist v =
  String.concat "," (List.map (sprintf "%S") (string_list v))

let string_array v =
  List.map (T.string_of_vbox cfg) (Seq.to_list (T.array_of_vbox cfg v))


let test001() =
  let text = {|
x = 42
value $(x)
|} in
  let v = cycle text in
  T.int_of_vbox cfg v = 42

let test002() =
  let text = {|
x = 42
y = $(x)
value $(y)
|} in
  let v = cycle text in
  T.int_of_vbox cfg v = 42

let test003() =
  let text = {|
global.x = 42
value $(x)
|} in
  let v = cycle text in
  T.int_of_vbox cfg v = 42

let test004() =
  let text = {|
private.x = 42
value $(x)
|} in
  let v = cycle text in
  T.int_of_vbox cfg v = 42

let test010() =
  let text = {|
x = 42
section
    x = 43
value $(x)
|} in
  let v = cycle text in
  T.int_of_vbox cfg v = 42

let test011() =
  let text = {|
global.x = 42
section
    x = 43
value $(x)
|} in
  let v = cycle text in
  T.int_of_vbox cfg v = 42

let test012() =
  let text = {|
private.x = 42
section
    x = 43
value $(x)
|} in
  let v = cycle text in
  T.int_of_vbox cfg v = 42

let test020() =
  let text = {|
x = 42
section
    x = 43
    export
value $(x)
|} in
  let v = cycle text in
  T.int_of_vbox cfg v = 43

let test021() =
  let text = {|
global.x = 42
section
    x = 43
    export
value $(x)
|} in
  let v = cycle text in
  T.int_of_vbox cfg v = 43

let test022() =
  let text = {|
private.x = 42
section
    x = 43
    export
value $(x)
|} in
  let v = cycle text in
  T.int_of_vbox cfg v = 42
                          
let test023() =
  let text = {|
private.x = 42
section
    x = 43
    export x
value $(x)
|} in
  let v = cycle text in
  T.int_of_vbox cfg v = 43


let test030() =
  let text = {|
global.x = 42
global.y = foo
section
    x = 43
    y = bar
    export x
value $(x)$(y)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "43foo"

let test031() =
  let text = {|
private.x = 42
private.y = foo
section
    x = 43
    y = bar
    export x
value $(x)$(y)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "43foo"

let test032() =
  let text = {|
private.x = x
global.x = 42
global.y = foo
section
    declare private.x
    x = 43
    y = bar
    export global.x
value $(x)$(y)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "42foo"

let test033() =
  let text = {|
private.x = x
declare global.x global.y
x = 42
y = foo
section
    private.x = 43
    y = bar
    export x
value $(x)$(y)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "42foo"

let test050() =
  let text = {|
x = 34
y = 115
z = X
if $(primitive.int-lt $(x), $(y))
    z = LT
else
    z = GE
value $(z)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "X"

let test051() =
  let text = {|
x = 34
y = 115
z = X
if $(primitive.int-lt $(x), $(y))
    z = LT
    export z
else
    z = GE
    export z
value $(z)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "LT"

let test052() =
  let text = {|
x = 34
y = 115
z = X
if $(primitive.int-lt $(y), $(x))
    z = LT
    export z
else
    z = GE
    export z
value $(z)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "GE"
                             
let test060() =
  let text = {|
incr(n) =
    value $(primitive.int-add $(n), 1)

value $(incr 41)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "42"

let test061() =
  let text = {|
incr(~n) =
    value $(primitive.int-add $(n), 1)

value $(incr ~n=41)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "42"

let test062() =
  let text = {|
incr(~n, ?i=1) =
    value $(primitive.int-add $(n), $(i))

value $(incr ~n=41)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "42"

let test063() =
  let text = {|
incr(~n, ?i=1) =
    value $(primitive.int-add $(n), $(i))

value $(incr ~i=2, ~n=40)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "42"

(* Remember that "=>" args must precede normal args *)
let test064() =
  let text = {|
incr(~n, f) =
    value $(f $(n), 1)

value $(incr x, y => $(primitive.int-add $(x), $(y)), ~n=41)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "42"

let test070() =
  let text = {|
incr(n) =
    primitive.int-add($(n), 1)

incr(41)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "42"

let test071() =
  let text = {|
incr(~n) =
    primitive.int-add($(n), 1)

incr(~n=41)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "42"

let test072() =
  let text = {|
incr(~n, ?i=1) =
    primitive.int-add($(n), $(i))

incr(~n=41)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "42"

let test073() =
  let text = {|
incr(~n, ?i=1) =
    primitive.int-add($(n), $(i))

incr(~i=2, ~n=40)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "42"

(* Remember that "=>" args must precede normal args *)
let test074() =
  let text = {|
incr(~n, f) =
    f($(n), 1)

incr(x, y => $(primitive.int-add $(x), $(y)), ~n=41)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "42"

(* With command syntax, we can also use "..." *)
let test075() =
  let text = {|
incr(~n, f) =
    f($(n), 1)

incr(x, y => ..., ~n=41)
    primitive.int-add($(x), $(y))
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "42"


let test080() =
  let text = {|
x = 1 2 3
x += 4 5 6
value $(x)
|} in
  let v = cycle text in
  string_list v = [ "1"; "2"; "3"; "4"; "5"; "6" ]

let test081() =
  let text = {|
x = 1 "2 3"
x += 4 $"5 6"
value $(x)
|} in
  let v = cycle text in
  string_list v = [ "1"; "\"2 3\""; "4"; "5 6" ]

let test082() =
  let text = {|
x = 1 2 3
y = 4 5
z = 6
value $(x) $(y) $(z)
|} in
  let v = cycle text in
  string_list v = [ "1"; "2"; "3"; "4"; "5"; "6" ]

let test083() =
  let text = {|
x = 1 2 3
y = 4 5
z = 6
value "$(x)" $"$(y)" $(z)
|} in
  let v = cycle text in
  string_list v = [ "\"1 2 3\""; "4 5"; "6" ]

let test084() =
  let text = {|
x = 1 2 3
y = 4 5
z = 6
value $(x)$(y) $(z)
|} in
  let v = cycle text in
  string_list v = [ "1"; "2"; "34"; "5"; "6" ]

let test090() =
  let text = {|
x[] = 1 2 3
x[] += 4 5 6
|} in
  let v = cycle text in
  string_array v = [ "1"; "2"; "3"; "4"; "5"; "6" ]

let test091() =
  let text = {|
x1 = 1 2
x2 = 3
x[] = $(x1) $(x2)
|} in
  let v = cycle text in
  string_array v = [ "1"; "2"; "3" ]

let test092() =
  let text = {|
x1 = $"1 2"
x2 = 3
x[] = $(x1) $(x2)
|} in
  let v = cycle text in
  string_array v = [ "1 2"; "3" ]
  
let test093() =
  let text = {|
x1[] = 1 2 3
x2[] = 4 5 6
x[] = $(x1) $(x2)
|} in
  let v = cycle text in
  string_array v = [ "1"; "2"; "3"; "4"; "5"; "6" ]

let test094() =
  let text = {|
x1[] = 1 2 3
x2[] = 4 5 6
x[] = $(x1)$(x2)
|} in
  let v = cycle text in
  string_array v = [ "1"; "2"; "3"; "4"; "5"; "6" ]
  
let test100() =
  let text = {|
x[] =
    1
    2
    3
x[] +=
    4
    5
    6
|} in
  let v = cycle text in
  string_array v = [ "1"; "2"; "3"; "4"; "5"; "6" ]

let test101() =
  let text = {|
x1 = 1  2
x2 = 3
x[] =
    $(x1)
    $(x2)
|} in
  let v = cycle text in
  string_array v = [ "1 2"; "3" ]

let test102() =
  let text = {|
x1 = $"1  2"
x2 = 3
x[] =
    $(x1)
    $(x2)
|} in
  let v = cycle text in
  string_array v = [ "1  2"; "3" ]
  
let test103() =
  let text = {|
x1[] = 1 2 3
x2[] = 4 5 6
x[] =
    $(x1)
    $(x2)
|} in
  let v = cycle text in
  string_array v = [ "1"; "2"; "3"; "4"; "5"; "6" ]

let test110() =
  let text = {|
x. =
    foo = 1
value $(x.foo)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "1"

let test111() =
  let text = {|
x. =
    foo = 1
    bar = $(foo)
value $(x.bar)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "1"

let test112() =
  let text = {|
x. =
    foo = 1
    bar = $(foo)
x. +=
    foo = 2
value $(x.bar)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "1"
  
let test113() =
  let text = {|
x. =
    foo = 1
    bar() =
        value $(foo)
value $(x.bar)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "1"

let test114() =
  let text = {|
x. =
    foo = 1
    bar() =
       value $(foo)
x. +=
    foo = 2
value $(x.bar)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "2"

let test115() =
  let text = {|
x. =
    private.foo = 1
    bar() =
       value $(foo)
x. +=
    foo = 2
value $(x.bar)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "1"

let test116() =
  let text = {|
x. =
    foo = 1
    bar() =
       value $(foo)
y. =
    extends $(x)
    foo = 2
value $(y.bar)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "2"

let test117() =
  let text = {|
x. =
    class X
    foo = 1
y. =
    extends $(x)
    class Y
    foo = 2
z. =
    extends $(y)
    class Z
    callfoo() =
        value $(X::foo)
value $(z.callfoo)
|} in
  let v = cycle text in
  T.string_of_vbox cfg v = "1"



(* From test 500 onwards we can use stdlib! *)

let test500() =
  let text = {|
value hello stdlib
|} in
  let v = cycle ~with_stdlib:true text in
  T.string_of_vbox cfg v = "hello stdlib"

let test501() =
  let text = {|
private.x = 0
private.y = 0
while $(primitive.int-lt $(y), 5)
   x = $(primitive.int-add $(x), $(y))
   y = $(primitive.int-add $(y), 1)
value $(x)
|} in
  let v = cycle ~with_stdlib:true text in
  T.string_of_vbox cfg v = "15"


  
let tests =
  [ "test001", test001;
    "test002", test002;
    "test003", test003;
    "test004", test004;
    "test010", test010;
    "test011", test011;
    "test012", test012;
    "test020", test020;
    "test021", test021;
    "test022", test022;
    "test023", test023;
    "test030", test030;
    "test031", test031;
    "test032", test032;
    "test033", test033;
    "test050", test050;
    "test051", test051;
    "test052", test052;
    "test060", test060;
    "test061", test061;
    "test062", test062;
    "test063", test063;
    "test064", test064;
    "test070", test070;
    "test071", test071;
    "test072", test072;
    "test073", test073;
    "test074", test074;
    "test075", test075;
    "test080", test080;
    "test081", test081;
    "test082", test082;
    "test083", test083;
    "test084", test084;
    "test090", test090;
    "test091", test091;
    "test092", test092;
    (* "test093", test093; *)  (* FIXME: auto-coercion from array to seq *)
    (* "test094", test094; *)  (* same *)
    "test100", test100;
    "test101", test101;
    "test102", test102;
    "test103", test103;
    "test110", test110;
    "test111", test111;
    "test112", test112;
    "test113", test113;
    "test114", test114;
    "test115", test115;
    "test116", test116;
    "test117", test117;

    "test500", test500;
    "test501", test501;
  ]

