module Compiler : sig
  val compile : Omake_ast.Types.prog -> Omake_ir.Types.statement list
end

module Stdlib : sig
  val add : Omake_vm.Types.global -> unit
end

