(* TODO:
    - while-loop-impl needs to be private!
    - define raise-break (for raising a "Break" exception)
    - catch the "Break" exception in while-loop
 *)

let text = {|

Stdlib. =
   global.while-loop-impl =
   global.while-loop-impl(cond, body, val) =
      export
      if $(cond)
         private.val = $(body 0)
         value $(while-loop-impl $(cond), $(body), $(val))
      else
         value $(val)

   while-loop(cond, body) =
      private.val =
      value $(while-loop-impl $(cond), $(body), $(val))
|}

let add glb =
  let prog = Omake_ast.Lex.parse_string text in
  let stlist = Compiler.compile prog in
  let sn = Omake_ir.Compiler.create_snippet glb in
  Omake_ir.Compiler.compile_snippet sn stlist;
  Omake_ir.Compiler.run_snippet glb sn

