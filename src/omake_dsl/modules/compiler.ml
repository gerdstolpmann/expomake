module I = Omake_ir.Types
module A = Omake_ast.Types
module T = Omake_vm.Types
module Seq = Omake_vm.Seq
module SSet = Set.Make(String)

(* TODO:
    - KeyDef and KeyExp: need maps for this
    - include, .INCLUDE, .SUBDIRS etc: need a way to compile/eval
      snippet by snippet
    - RuleExp, ShellExp: just call a function of the stdlib
    - throw, catch
    - fix up runtime exceptions
    - while
    - lazy evaluation
 *)

type context =
  {
    export_ok : bool;
    export : (string * I.vartype option) list;
    export_glb : bool;
    export_obj : bool;
    in_obj : bool;
  }

let syntax_error loc msg arg =
  raise (A.Syntax_error (loc, msg, arg))

let syntax_error_unit loc msg =
  syntax_error loc msg A.Error_unit

let syntax_error_str loc msg s =
  syntax_error loc msg (A.Error_string s)

let syntax_error_ast msg a =
  syntax_error (Omake_ast.Util.loc_of_exp a) msg (A.Error_ast a)

let get_vartype loc prefix =
  match prefix with
    | A.Global -> Some I.G
    | A.Private -> Some I.P
    | A.Shared -> Some I.S
    | A.This -> Some I.T
    | A.Unqualified -> None
    | A.Primitive ->
        (* There's I.Prim but we don't want to return it in all cases
           as we have some hard-wired primitives, see [apply] below.
         *)
        syntax_error_unit loc "The 'primitive' modifier is not allowed here"

let primitives =  (* only the hard-wired ones *)
  [ "length", `Monop I.MLength;
    "wait", `Monop I.MWait;
    "test", `Monop I.MTest;
    "nth", `Binop I.BNth;
    "int-add", `Binop I.BAdd;
    "int-sub", `Binop I.BSub;
    "int-mul", `Binop I.BMul;
    "int-div", `Binop I.BDiv;
    "int-mod", `Binop I.BMod;
    "int-eq", `Binop I.BEq;
    "int-ne", `Binop I.BNe;
    "int-lt", `Binop I.BLt;
    "int-le", `Binop I.BLe;
  ]

let primitives =
  List.fold_left
    (fun acc (name, op) -> T.SMap.add name op acc)
    T.SMap.empty
    primitives

let apply loc qvar named anon =
  let prefix = A.(qvar.qprefix) in
  if prefix = A.Primitive then (
    if named <> [] then
      syntax_error_unit loc "Unexpected named arguments";
    match T.SMap.find A.(qvar.qname) primitives with
      | `Monop monop ->
          ( match anon with
              | [ e1 ] ->
                  I.MonOp{monop; arg=e1}
              | _ ->
                  syntax_error_unit loc "Wrong number of arguments"
          )
      | `Binop binop ->
          ( match anon with
              | [ e1; e2 ] ->
                  I.BinOp{binop; arg1=e1; arg2=e2}
              | _ ->
                  syntax_error_unit loc "Wrong number of arguments"
          )
      | exception Not_found ->
          I.Apply { var=A.(qvar.qname); vartype=Some I.Prim; named; anon }
  ) else
    let vartype = get_vartype loc prefix in
    I.Apply { var=A.(qvar.qname); vartype; named; anon }

let apply_method base fields named anon =
  let rec descend base fields =
    match fields with
      | [] ->
          if named=[] && anon=[] then
            base
          else
            I.LetLocal
              { var="base";
                expr=base;
                inexpr=I.Apply { var="base"; vartype=Some I.L; named; anon }
              }
      | field :: fields' ->
          descend
            (I.Field { this=base; cls=None; name=field })
            fields' in
  descend base fields

let var name vartype =
  I.Apply { var=name; vartype; anon=[]; named=[] }

let this =
  I.Apply { var="this"; vartype=Some I.L; anon=[]; named=[] }

let empty_seq =
  I.Literal (T.v_seq Seq.empty)

let empty_block =
  { I.st=[];
    I.export=[];
    I.export_glb=false;
    I.export_obj=false
  }

let is_null_exp =
  function
  | A.NullExp _ -> true
  | _ -> false

let unqual qname =
  A.{ qprefix = Unqualified; qcurry = false; qname }

let stdlib_qvar =
  unqual "Stdlib"

type clause_syms =
  | CCase | CDefault | CWhen | CFinally | CDo
  | CCatch of string

let clause_syms =
  [ "case", CCase;
    "default", CDefault;
    "when", CWhen;
    "finally", CFinally;
    "do", CDo
  ]

let clause_set =
  List.fold_right SSet.add (List.map fst clause_syms) SSet.empty

let is_clause qvar =
  A.(qvar.qprefix = Unqualified && not qvar.qcurry &&
       SSet.mem qvar.qname clause_set)

let rec collect_cases cases el =
   match el with
     | (A.CommandExp (v, e, body, loc) :: el') when is_clause v ->
         let sym = List.assoc A.(v.qname) clause_syms in
         collect_cases ((sym, e, body, loc) :: cases) el
     | (A.CatchExp (v1, v2, body, loc) :: el) ->
         let sym = CCatch(v1) in
         collect_cases
           ((sym, A.StringOtherExp (v2, loc), body, loc) :: cases) el
    | _ ->
         List.rev cases, el

let rec dest_while_cases collected dfldfl cases =
  match cases with
    | [] ->
        (List.rev collected, dfldfl)
    | (CDefault, e, body, loc) :: cases' ->
       if cases' <> [] then
         syntax_error_unit loc "the 'default' case must come last";
       if not(is_null_exp e) then
         syntax_error_unit loc "the 'default' case is not followed by an \
                                in-line expression";
       (List.rev collected, body)
    | (CCase, e, body, loc) :: cases' ->
        dest_while_cases ((e, body, loc) :: collected) dfldfl cases'
    | (_, _, _, loc) :: _ ->
        syntax_error_unit loc "only 'case' or 'default' allowed here"

let rec comp_block_stm_list ctx el =
  match el with
    | A.CommandExp(v, e1, body, loc) :: el' ->
        ( match A.(v.qname), A.(v.qprefix), A.(v.qcurry) with
            | _, _, true ->
                syntax_error_unit loc "The 'curry' modifier is not allowed here"
            | "include", A.Unqualified, false ->
                assert false  (* TODO *)
            | "if", A.Unqualified, false ->
                let ir_cond = comp_expr ctx e1 in
                let ir_then = comp_expr ctx (A.BodyExp(body, loc)) in
                comp_block_if
                  ctx ir_cond ir_then
                  (fun ctx ir el' ->
                    comp_block_stm_next ctx [I.Value ir] el'
                  )
                  el'
            | "section", A.Unqualified, false ->
                if not(is_null_exp e1) then
                  syntax_error_unit loc "Sections cannot have arguments";
                let e' = A.BodyExp(body, loc) in
                let ir = [ I.Value (comp_expr ctx e') ] in
                comp_block_stm_next ctx ir el'
            | "value", A.Unqualified, false ->
                (* This is different from old omake *)
                if body <> [] then
                  syntax_error_unit loc "illegal body for value";
                let ir = [ I.Value (comp_expr ctx e1) ] in
                comp_block_stm_next ctx ir el'
            | "declare", A.Unqualified, false ->
                comp_block_declare ctx loc e1 body el'
            | "return", A.Unqualified, false ->
                if body <> [] then
                  syntax_error_unit loc "illegal body for return";
                let ir = [ I.Value (comp_expr ctx e1); I.Return ] in
                comp_block_stm_next ctx ir el'
            | "open", A.Unqualified, false ->
                assert false  (* TODO *)
            | "autoload", A.Unqualified, false ->
                assert false  (* TODO *)
            | "set", A.Unqualified, false ->
                assert false  (* TODO *)
            | "while", A.Unqualified, false ->
                comp_block_while ctx e1 body loc el'
            | "export", A.Unqualified, false ->
                comp_block_export ctx loc e1 body el'
            | _ ->
                (* TODO: old omake also tests for e1="return" and handles
                   this is a return with identifier
                 *)
                if body <> [] then
                  syntax_error_unit loc ("illegal body for " ^ A.(v.qname));
                let e' = A.ApplyExp(A.CommandApply, v, [ A.ExpArg e1 ], loc) in
                let ir = [ I.Value (comp_expr ctx e') ] in
                comp_block_stm_next ctx ir el'
        )
    | A.VarDefExp(var, A.DefineString, defflag, e1, loc) :: el' ->
        comp_block_vardef_string ctx var defflag e1 loc el'
    | A.VarDefExp(var, A.DefineArray, defflag, e1, loc) :: el' ->
        comp_block_vardef_array_inline ctx var defflag e1 loc el'
    | A.VarDefBodyExp(var, A.DefineString, defflag, el, loc) :: el' ->
        let e1 = A.BodyExp(el, loc) in
        comp_block_vardef_string ctx var defflag e1 loc el'
    | A.VarDefBodyExp(var, A.DefineArray, defflag, el, loc) :: el' ->
        comp_block_vardef_array_body ctx var defflag el loc el'
    | A.ObjectDefExp(var, defflag, el, loc) :: el' ->
        comp_block_vardef_object ctx var defflag el loc el'
    | A.FunDefExp(var, params, el, loc) :: el' ->
        comp_block_fundef ctx var params el loc el'
    | A.RuleExp(multiple, target, pattern, source, commands, loc) :: el' ->
        assert false  (* TODO *)
    | A.ShellExp(e1, loc) :: el' ->
        assert false  (* TODO *)
    | A.CatchExp(v1, v2, el, loc) :: el' ->
        assert false  (* TODO *)
    | A.KeyDefExp(name, defkind, defflag, e1, loc) :: el' ->
        assert false  (* TODO *)
    | A.KeyDefBodyExp(name, defkind, defflag, el, loc) :: el' ->
        assert false  (* TODO *)
    | A.ClassExp(_, loc) :: el' ->
        syntax_error_unit loc "the 'class' specifier is not allowed here"
    | A.ExtendsExp(_, loc) :: el' ->
        syntax_error_unit loc "the 'extends' specifier is not allowed here"
    | (A.NullExp _
      | A.IntExp _
      | A.FloatExp _
      | A.StringOpExp _
      | A.StringIdExp _
      | A.StringIntExp _
      | A.StringFloatExp _
      | A.StringWhiteExp _
      | A.StringOtherExp _
      | A.StringKeywordExp _
      | A.QuoteExp _
      | A.QuoteStringExp _
      | A.SequenceExp _
      | A.ArrayExp _
      | A.ApplyExp _
      | A.SuperApplyExp _
      | A.MethodApplyExp _
      | A.BodyExp _
      | A.KeyExp _
      )  :: el' ->
        let ir = I.Value (comp_expr ctx (List.hd el)) in
        comp_block_stm_next ctx [ir] el'
    | [] ->
        (ctx, [ I.Value empty_seq])

and comp_block_stm_next ctx ir el' =
  if el' = [] then
    (ctx, ir)
  else
    let (ctx, ir') = comp_block_stm_list ctx el' in
    (ctx, ir @ ir')

and comp_block_if ctx ir_cond ir_then process_next el =
  let if_then_else ir_else =
    I.If { cond = ir_cond;
           ontrue = ir_then;
           onfalse = ir_else;
         } in
  let fallback el' =
    let ir = if_then_else empty_seq in
    process_next ctx ir el' in
  match el with
    | A.CommandExp(v, e1, body, loc) :: el' ->
        ( match A.(v.qname), A.(v.qprefix), A.(v.qcurry) with
            | "elsif", A.Unqualified, false ->
                let ir_cond1 = comp_expr ctx e1 in
                let ir_then1 = comp_expr ctx (A.BodyExp(body, loc)) in
                comp_block_if
                  ctx ir_cond1 ir_then1
                  (fun ctx ir_else el' ->
                    let ir = if_then_else ir_else in
                    process_next ctx ir el'
                  )
                  el'
            | "else", A.Unqualified, false ->
                if not (is_null_exp e1) then
                  syntax_error_unit loc "unexpected argument for else";
                let ir_else = comp_expr ctx (A.BodyExp(body, loc)) in
                let ir = if_then_else ir_else in
                process_next ctx ir el'
            | _ ->
                fallback el
        )
    | _ ->
        fallback el

and comp_block_declare ctx loc e1 el el' =
  let wlist =
    try
      A.words e1 @ List.flatten (List.map A.words el)
    with
      | Failure msg ->
          syntax_error_unit loc msg in
  let vars =
    try
      List.map
        (fun word ->
          let var, rest = A.parse_qvar_from_string word in
          if rest <> [] then
            syntax_error_unit loc "no method fields allowed in declare";
          var
        )
        wlist
    with
      | Failure msg ->
          syntax_error_unit loc msg in
  let ir =
    List.map
      (fun qvar ->
        if A.(qvar.qcurry) then
          syntax_error_unit loc "cannot declare as 'curry'";
        let vartype =
          match get_vartype loc A.(qvar.qprefix) with
            | None ->
                syntax_error_unit loc "missing qualifier in declare"
            | Some vt -> vt in
        I.Declare { var = A.(qvar.qname);
                    vartype;
                  }
      )
      vars in
  comp_block_stm_next ctx ir el'

and comp_block_export ctx loc e1 el el' =
  if not ctx.export_ok then
    syntax_error_unit loc "this is already the top-level scope";
  let wlist =
    try
      A.words e1 @ List.flatten (List.map A.words el)
    with
      | Failure msg ->
          syntax_error_unit loc msg in
  let vars =
    try
      List.map
        (fun word ->
          let var, rest = A.parse_qvar_from_string word in
          if rest <> [] then
            syntax_error_unit loc "no method fields allowed in export";
          if A.(var.qcurry) then
            syntax_error_unit loc "cannot export as 'curry'";
          ( match A.(var.qprefix) with
              | A.Global -> ()
              | A.Private -> ()
              | A.This ->
                  syntax_error_unit loc "cannot export as 'this'";
              | A.Shared ->
                  syntax_error_unit loc "cannot export as 'shared'";
              | A.Primitive ->
                  syntax_error_unit loc "cannot export as 'primitive'";
              | A.Unqualified -> ()
          );
          var
        )
        wlist
    with
      | Failure msg ->
          syntax_error_unit loc msg in
  let ctx =
    if vars = [] then
      { ctx with
        export_glb = true;
        export_obj = ctx.in_obj;
      }
    else
      let export =
        List.map
          (fun var ->
            let vartype = get_vartype loc A.(var.qprefix) in
            A.(var.qname, vartype)
          )
          vars in
      { ctx with
        export
      } in
  comp_block_stm_next ctx [] el'

and comp_block_vardef_string ctx var defflag e1 loc el' =
  (* A variable definition x=<value> or x+=<value> *)
  if A.(var.qcurry) then
    syntax_error_unit loc "unsupported 'curry' modifier";
  let vartype = get_vartype loc A.(var.qprefix) in
  let ir_sub = comp_expr ctx e1 in
  let ir =
    [ I.Let { var = A.(var.qname);
              vartype;
              expr = ( match defflag with
                         | A.DefineNormal ->
                             ir_sub
                         | A.DefineAppend ->
                             let ir_old =
                               I.Apply { var = A.(var.qname);
                                         vartype;
                                         named=[]; anon=[]
                                       } in
                             I.MultiOp { multiop = I.NSeq;
                                         args = [ ir_old;
                                                  ir_sub
                                                ]
                                       }
                     );
            }
    ] in
  comp_block_stm_next ctx ir el'

and comp_block_vardef_array_inline ctx var defflag e1 loc el' =
  (* A variable definition x[]=<value> or x[]+=<value> *)
  if A.(var.qcurry) then
    syntax_error_unit loc "unsupported 'curry' modifier";
  let vartype = get_vartype loc A.(var.qprefix) in
  let ir_sub = comp_expr ctx e1 in
  let ir =
    [ I.Let { var = A.(var.qname);
              vartype;
              expr = ( match defflag with
                         | A.DefineNormal ->
                             I.MonOp { monop=I.MArraySeq;
                                       arg=ir_sub
                                     }
                         | A.DefineAppend ->
                             let ir_old =
                               I.Apply { var = A.(var.qname);
                                         vartype;
                                         named=[]; anon=[]
                                       } in
                             I.MultiOp { multiop = I.NArray;
                                         args = [ ir_old;
                                                  I.MonOp { monop=I.MArraySeq;
                                                            arg=ir_sub
                                                          }
                                                ]
                                       }
                     );
            }
    ] in
  comp_block_stm_next ctx ir el'

and comp_block_vardef_array_body ctx var defflag el loc el' =
  (* A variable definition x[]=<body> or x[]+=<body> *)
  if A.(var.qcurry) then
    syntax_error_unit loc "unsupported 'curry' modifier";
  let vartype = get_vartype loc A.(var.qprefix) in
  let ir_subs = List.map (comp_expr ctx) el in
  let ir =
    [ I.Let { var = A.(var.qname);
              vartype;
              expr = ( match defflag with
                         | A.DefineNormal ->
                             I.MultiOp { multiop=I.NArray;
                                         args=ir_subs
                                     }
                         | A.DefineAppend ->
                             let ir_old =
                               I.Apply { var = A.(var.qname);
                                         vartype;
                                         named=[]; anon=[]
                                       } in
                             I.MultiOp { multiop = I.NArray;
                                         args = ( ir_old :: ir_subs)
                                       }
                     );
            }
    ] in
  comp_block_stm_next ctx ir el'
  
and comp_block_vardef_object ctx var defflag el loc el' =
  (* A variable definition x.=<body> or x.+=<body> *)
  if A.(var.qcurry) then
    syntax_error_unit loc "unsupported 'curry' modifier";
  let vartype = get_vartype loc A.(var.qprefix) in
  let specifiers, el =
    List.partition
      (function
       | A.ClassExp _ | A.ExtendsExp _ -> true
       | _ -> false
      )
      el in
  let parent_opt, cls = dest_obj_specifiers specifiers None None in
  let extends =
    match defflag with
      | A.DefineNormal ->
          ( match parent_opt with
              | None ->
                  I.Object "Object"
              | Some parent ->
                  comp_expr ctx parent
          )
      | A.DefineAppend ->
          if parent_opt <> None then
            syntax_error_unit loc "when appending the 'extends' specifier is \
                                   not allowed";
          I.Apply { var = A.(var.qname);
                    vartype;
                    named=[]; anon=[]
                  } in
  let ctx = { ctx with export_ok = true } in   (* CHECK *)
  let (ctx, stm) = comp_block_stm_list ctx el in
  let body = { I.st = stm;
               I.export = ctx.export;
               I.export_glb = ctx.export_glb;
               I.export_obj = ctx.export_obj;
             } in
  let ir =
    [ I.Let { var = A.(var.qname);
              vartype;
              expr = I.Exemplar { extends;
                                  cls;
                                  body;
                                }
            }
    ] in
  comp_block_stm_next ctx ir el'

and dest_obj_specifiers el parent_opt cls_opt =
  match el with
    | A.ClassExp(classes, loc) :: el' ->
        if cls_opt <> None then
          syntax_error_unit loc "only one 'class' specifier is allowed";
        if List.length classes > 1 then
          syntax_error_unit loc "only one class may be specified";
        dest_obj_specifiers el' parent_opt (Some (List.hd classes))
    | A.ExtendsExp(e, loc) :: el' ->
        if parent_opt <> None then
          syntax_error_unit loc "only one 'extends' specifier is allowed";
        dest_obj_specifiers el' (Some e) cls_opt
    | _ :: _ ->
        assert false
    | [] ->
        (parent_opt, cls_opt)

and comp_block_fundef ctx var params el loc el' =
  let autocurry = A.(var.qcurry) in
  let ir_fun =
    comp_abstract ctx autocurry params el in
  let vartype = get_vartype loc A.(var.qprefix) in
  let ir =
    [ I.Let { var = A.(var.qname);
              vartype;
              expr = ir_fun
            }
    ] in
  comp_block_stm_next ctx ir el'

and comp_block_while ctx e1 body loc el' =
  (* The translation of "while e1 | body" is:
     $(Stdlib.while-loop $(e1), $(body))

     The translation of
     "while e1 | case c1 body1 | case c2 body2 ... | default dbody":
     $(Stdlib.while-loop $(e1), $(if $(c1), $(body1), ... $(dbody)... ))

     If there is no default, we substitute $(Stdlib.raise-break)
   *)
  let cases, el' = collect_cases [] el' in
  let cases =
    if body = [] then
      cases
    else
      let default = (CDefault, A.NullExp loc, body, loc) in
      cases @ [default] in
  let dfldfl =
    [A.MethodApplyExp (A.NormalApply, stdlib_qvar, [ "raise-break" ],[], loc)]
  in
  let conditions, default =
    dest_while_cases [] dfldfl cases in
  let ir_e1 = comp_expr ctx e1 in
  let ir_body =
    List.fold_right
      (fun (cond,case,caseloc) acc ->
        I.If { cond = comp_expr ctx cond;
               ontrue = comp_expr ctx (A.BodyExp(case,caseloc));
               onfalse = acc
             }
      )
      conditions
      (comp_expr ctx (A.BodyExp(default, loc))) in
  let ir_body =
    I.Abstract { named=[]; anon=[""]; arity_min=1; autocurry=false;
                 body={ empty_block with
                        I.export_glb=true;
                        I.st=[I.Value ir_body];
                      };
               } in
  let ir =
    I.Value
      (apply_method
         (apply loc stdlib_qvar [] [])
         [ "while-loop" ] [] [ir_e1; ir_body]) in
  comp_block_stm_next ctx [ir] el'


and comp_expr ctx e =
  match e with
    | A.NullExp loc ->  (* take this as an empty sequence *)
        I.MultiOp { multiop = I.NSeq; args = [] }
    | A.IntExp (i, loc) ->
        I.Literal (T.v_int i)
    | A.FloatExp (x, loc) ->
        I.Literal (T.v_float x)
    | A.StringOpExp (s, loc)
    | A.StringIdExp (s, loc)
    | A.StringIntExp (s, loc)
    | A.StringFloatExp (s, loc)
    | A.StringOtherExp (s, loc)
    | A.StringKeywordExp (s, loc)
    | A.StringWhiteExp (s, loc) ->
        I.Literal (T.v_string s)
    | A.QuoteExp (el, loc) ->
        let il = List.map (comp_expr ctx) el in
        I.Coerce { usertype = T.T_string;
                   arg = I.MultiOp { multiop = I.NConcat; args = il }
                 }
    | A.QuoteStringExp (c, el, loc) ->
        let il = List.map (comp_expr ctx) el in
        I.MonOp
          { monop = I.MQuote c;
            arg = I.MultiOp { multiop = I.NConcat; args = il }
          }
    | A.SequenceExp ([e], _) ->
        comp_expr ctx e
    | A.SequenceExp (el, loc) ->
        let il = comp_expr_seq ctx el [] in
        I.MultiOp { multiop = I.NSeq; args = il }
    | A.ArrayExp (el, loc) ->
        let il = List.map (comp_expr ctx) el in
        I.MultiOp { multiop = I.NArray; args = il }
    | A.ApplyExp ((A.NormalApply | A.CommandApply), v, args, loc) ->
        let named, anon = comp_arguments ctx args in
        apply loc v named anon
    | A.ApplyExp (A.LazyApply, v, args, loc) ->
        assert false  (* TODO *)
    | A.ApplyExp (A.EagerApply, v, args, loc) ->
        assert false  (* TODO *)
    | A.SuperApplyExp ((A.NormalApply | A.CommandApply), super, v, args, loc) ->
        let named, anon = comp_arguments ctx args in
        let base = I.Field { this; cls=Some super; name=v } in
        apply_method base [] named anon
    | A.SuperApplyExp (A.LazyApply, super, v, args, loc) ->
        assert false  (* TODO *)
    | A.SuperApplyExp (A.EagerApply, super, v, args, loc) ->
        assert false  (* TODO *)
    | A.MethodApplyExp ((A.NormalApply | A.CommandApply), v,fields,args,loc) ->
        let named, anon = comp_arguments ctx args in
        apply_method (apply loc v [] []) fields named anon
    | A.MethodApplyExp (A.LazyApply, v, fields, args, loc) ->
        assert false  (* TODO *)
    | A.MethodApplyExp (A.EagerApply, v, fields, args, loc) ->
        assert false  (* TODO *)
    | A.BodyExp (el, loc) ->
        let ctx = { ctx with export_ok = true } in
        let (ctx, stm) = comp_block_stm_list ctx el in
        let body = { I.st = stm;
                     I.export = ctx.export;
                     I.export_glb = ctx.export_glb;
                     I.export_obj = ctx.export_obj;
                   } in
        I.Block body
    | A.KeyExp (strategy, v, loc) ->
        assert false  (* TODO *)
    | A.CommandExp (_, _, _, loc)
    | A.VarDefExp (_, _, _, _, loc)
    | A.VarDefBodyExp (_, _, _, _, loc)
    | A.KeyDefExp (_, _, _, _, loc)
    | A.KeyDefBodyExp (_, _, _, _, loc)
    | A.ObjectDefExp (_, _, _, loc)
    | A.FunDefExp (_, _, _, loc)
    | A.RuleExp (_, _, _, _, _, loc)
    | A.ShellExp (_, loc)
    | A.CatchExp (_, _, _, loc)
    | A.ExtendsExp (_, loc)
    | A.ClassExp (_, loc) ->
        syntax_error_unit loc "misplaced expression"

and comp_expr_seq ctx el acc =
  match el with
    | [] when acc=[] ->
        []
    | [] ->
        let il = List.rev acc in
        [ I.MultiOp { multiop = I.NConcat; args = il } ]
    | A.StringWhiteExp (_, _) :: el' ->
        let il = List.rev acc in
        let i1 = I.MultiOp { multiop = I.NConcat; args = il } in
        i1 ::
          comp_expr_seq ctx el' []
    | A.NullExp _ :: el' ->
        comp_expr_seq ctx el' acc
    | A.SequenceExp (el1, _) :: el' ->
        comp_expr_seq ctx (el1 @ el') acc
    | e :: el' ->
        comp_expr_seq ctx el' (comp_expr ctx e :: acc)

and comp_arguments ctx args =
  let named =
    List.flatten
      (List.map
         (function
          | A.KeyArg (key, e) ->
              [ key, comp_expr ctx e ]
          | _ ->
              []
         )
         args
      ) in
  let anon =
    List.flatten
      (List.map
         (function
          | A.ExpArg e ->
              [ comp_expr ctx e ]
          | A.ArrowArg(params, e) ->
              let autocurry = false in
              [ comp_abstract ctx autocurry params [e] ]
          | _ ->
              []
         )
         args
      ) in
  (named, anon)

and comp_abstract ctx autocurry params el =
  let anon =
    List.flatten
      (List.map
         (function
          | A.NormalParam(name, loc) ->
              [ name ]
          | _ ->
              []
         )
         params
      ) in
  let named_with_defaults =
    List.flatten
      (List.map
         (function
          | A.OptionalParam(name, e_default, loc) ->
              [ (name, Some (comp_expr ctx e_default)) ]
          | A.RequiredParam(name, loc) ->
              [ (name, None) ]
          | _ ->
              []
         )
         params
      ) in
  let named_with_defaults =
    List.sort (fun (n1,_) (n2,_) -> String.compare n1 n2) named_with_defaults in
  let named = List.map fst named_with_defaults in
  let initcode =
    List.flatten
      (List.map
         (function
          | (name, Some e_default) ->
              (* TODO: prevent that e_default sees the other uninitialized
                 parameters *)
              [ I.Effect
                  (I.If { cond=I.IsUninitialized
                                 (var name (Some I.P));
                          ontrue=I.Block
                                   { empty_block with
                                     I.export = [ (name, Some I.P) ];
                                     I.st = [ I.Let { var=name;
                                                      vartype=Some I.P;
                                                      expr=e_default
                                                    }
                                            ]
                                   };
                          onfalse=I.Literal(T.v_false);
                        }
                  )
              ]
          | (name, None) ->
              (* TODO: raise exception when parameter is uninitialized *)
              []
         )
         named_with_defaults
      ) in
  let (ctx, stm) = comp_block_stm_list ctx el in
  I.Abstract
      { named;
        anon;
        arity_min=List.length anon;
        autocurry;
        body = { I.st = initcode @ stm;
                 I.export = ctx.export;
                 I.export_glb = ctx.export_glb;
                 I.export_obj = ctx.export_obj;
               }
      }

let compile (el : Omake_ast.Types.prog) : Omake_ir.Types.statement list =
  let ctx =
    { export_ok = false;
      export = [];
      export_glb = false;
      export_obj = false;
      in_obj = false;
    } in
  let ctx, ir = comp_block_stm_list ctx el in
  ir

