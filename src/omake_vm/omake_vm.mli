module Seq : sig
  (** A sequence is an (immutable) hybrid of list and array *)

  type 't seq =
    private
    | Skip of int * 't seq
    | Concat of (* left length: *) int * 't seq * (* right length: *) int * 't seq
    | Vector of 't array

  val length : 't seq -> int
  val get : 't seq -> int -> 't
  val hd : 't seq -> 't
  val tl : 't seq -> 't seq
  val skip : int -> 't seq -> 't seq   (* skip first n elements *)

  val is_empty : 't seq -> bool
  val empty : 't seq
  val make : int -> 't -> 't seq
  val init : int -> (int -> 't) -> 't seq

  val of_array : 't array -> 't seq
  val of_list : 't list -> 't seq

  val to_array_list : 't seq -> 't array list
  val to_array : 't seq -> 't array
  val to_list : 't seq -> 't list

  val append : 't seq -> 't seq -> 't seq
  val concat : 't seq list -> 't seq
  val concata : 't seq array -> 't seq
  val sub : 't seq -> int -> int -> 't seq
  val copy : 't seq -> 't seq
  val rev : 't seq -> 't seq

  val iter : ('t -> unit) -> 't seq -> unit
  val iteri : (int -> 't -> unit) -> 't seq -> unit
  val map : ('t -> 's) -> 't seq -> 's seq
  val mapi : (int -> 't -> 's) -> 't seq -> 's seq
  val fold_left : ('a -> 'b -> 'a) -> 'a -> 'b seq -> 'a
  val fold_right : ('b -> 'a -> 'a) -> 'b seq -> 'a -> 'a

  type 't map_type =
    | Single of 't
    | Flatten of 't seq

  val flat_map : ('s -> 't map_type) -> 's seq -> 't seq
  val flat_mapi : (int -> 's -> 't map_type) -> 's seq -> 't seq

  val mem : 'a -> 'a seq -> bool
  val find : ('a -> bool) -> 'a seq -> 'a
  val filter : ('a -> bool) -> 'a seq -> 'a seq
  val exists : ('a -> bool) -> 'a seq -> bool
  val for_all : ('a -> bool) -> 'a seq -> bool

  val optimize : 'a seq -> 'a seq
end

module Lookup_table : sig
  (** Manages global lookup tables *)

  (* Impl:
      - need mapping from names to slots
      - data structure consists of two parts:
         * mapping from slots to values (primary lookup)
         * array of values (secondary lookup)
   *)

  type name = string
  type slot = int

  (** Allocators map names to slots. Allocators are changed in-place
      by [add_slot]. Normally, an allocator for a type of lookup table
      is shared by the whole omake process, which ensures that every
      lookup table of the given type uses the same slot numbers.
   *)
  type allocator

  val create_alloc : unit -> allocator
  val add_slot : name -> allocator -> slot
  val find_slot : name -> allocator -> slot  (* or Not_found *)
  val length : allocator -> int

  (** Lookup tables are functional maps from slots to values *)
  type 'a t

  val create : allocator -> 'a -> 'a t
    (** [create alloc dfl]: create a table where all allocated slots
        are mapped to [dfl]
     *)
  val add : slot -> 'a -> 'a t -> 'a t
  val get : slot -> 'a t -> 'a
  val allocator : 'a t -> allocator
  val optimize : 'a t -> 'a t
end

module Types : sig
  module SMap : Map.S with type key = string

  type global_kind =
    | Global_function
    | Global_variable
    | Global_type

   and alloc =
     { alloc_functions : Lookup_table.allocator;
       alloc_variables : Lookup_table.allocator;
       mutable alloc_dummy : int;    (* allow == for alloc *)
     }

   and shared =
     { alloc : alloc;
       mutable shared_functions : closure option Lookup_table.t;
     }

  and global =
    { shared : shared;
      global_variables : valuebox Lookup_table.t;
      (* global_types : XXX Lookup_table.t; *)
      (* global_procenv : string Lookup_table.t *)
      (* also here: stdin, stdout, stderr, cwd, ... *)
    }

  and closure = private
    { clos_slot : Lookup_table.slot;    (* of Global_function *)
      clos_entry : int;
      clos_env : valuebox array;
      clos_args : valuebox Seq.seq;     (** anonymous arguments already passed *)
      clos_arity_min : int;             (** min number of anon args *)
      clos_arity_max : int;             (** max number of anon args *)
      clos_autocurry : bool;            (** enable auto-currying; requires [clos_arity_min = clos_arity_max] *)
      clos_names : string array;        (** named args, in ascending order *)
      clos_code : code;
    }

  and code

  (* TODO: For correct coercion between bool/int/float and string we need
     to keep the original string of a parsed value. E.g.
     V_intstring of int * string
   *)

  and stringval = private
    | V_bool of bool
    | V_int of int
    | V_float of float
    | V_string of string
    | V_seq of stringval Seq.seq
    | V_quote of char * stringval

  and _ repr =
    | R_uninitialized : unit repr
    | R_bool : bool repr
    | R_int : int repr
    | R_float : float repr
    | R_string : string repr
    | R_seq : stringval Seq.seq repr
    | R_quote : char -> stringval repr
    | R_array : valuebox Seq.seq repr
    | R_dynamic : valuebox repr
    | R_closure : closure repr
    | R_lazy : lazyval repr
    | R_ref : reference repr
    | R_global : global repr
    | R_regind : (frame_id * int) repr
    | R_env : valuebox array repr
    | R_object : (cls * unit) repr
    | R_extend : (cls * object_extension) repr
    | R_update : object_extension repr
    | R_handle : Omake_os.Handle.t repr
    | R_custom : custom repr

(*
    | R_as : 'a repr * 'b repr -> ('a * 'b) repr
        (* coerce the first type to the second *)
        (* THINK: make 'b lazy *)

VBox(R_as(R_string, R_bool), ("true", true))
VBox(R_as(R_extend, R_seq), (obj, "sequence view"))

Simplify stringval: remove V_bool, V_int, V_float.

R_as may be chained: R_as(R_as(t_orig,t_intermediate),t_final)
Coercion operation tries to keep these chains short: it tries
to do the coerction for t_orig, then t_intermediate, then t_final,
and it removes all coercions no longer needed.

All primitives coerce their parameters automatically to the expected type.

Methods are NOT searched by trying coercions, though.
 *)

  and lazyval =
    Lz of { mutable value : valuebox;
            compute : closure
          }

  (** References have limited use only! Only to be used locally for creating
      cyclic values
   *)
  and reference = private
    Ref of { mutable value : valuebox }

  and _ value = private
    | V : 'a repr * 'a -> 'a value

  and valuebox = private
    | VBox : 'a repr * 'a -> valuebox

  and cls =
    | Class of string

  and usertype =
    | T_unit
    | T_bool
    | T_int
    | T_float
    | T_string
    | T_seq
    | T_array
    | T_tuple of usertype array
    | T_fun of {arity:int}
    (* add: T_class *)

  and object_extension = private
    Objext of { base : valuebox;
                fields : valuebox SMap.t;
              }

  and frame_id
  and custom = ..

  type primitive = global -> valuebox array -> valuebox

  type config =
    Config of
      { class_of : valuebox -> string;
        field_of : valuebox -> cls option -> string -> valuebox;
        coerce_to : usertype -> valuebox -> valuebox;
        debug_string : custom -> string;
        primitives : (string * primitive) array;
      }

  val v_false : stringval
  val v_true : stringval
  val v_bool : bool -> stringval
  val v_int : int -> stringval
  val v_float : float -> stringval
  val v_string : string -> stringval
  val v_seq : stringval Seq.seq -> stringval
  val v_quote : char * stringval -> stringval

  val value : 'a repr -> 'a -> 'a value
  val valuebox : 'a repr -> 'a -> valuebox

  exception Unexpected_type_of_value
  exception Undefined_value
  exception No_such_field

  (** Extractors for internal values. These extractors do not handle
      dynamic or lazy values.
   *)

  val global_of_vbox : valuebox -> global
  val env_of_vbox : valuebox -> valuebox array
  val reference_of_vbox : valuebox -> reference

  (** Extractors for user-visible values. These extractors follow
      dynamic values, and they can extract from lazy values so far
      these are already computed. For custom values, the [coerce_to]
      function of [config] is called.
   *)

  val closure_of_vbox : config -> valuebox -> closure
  val array_of_vbox : config -> valuebox -> valuebox Seq.seq
  val string_of_vbox : config -> valuebox -> string
  val int_of_vbox : config -> valuebox -> int
  val bool_of_vbox : config -> valuebox -> bool
  val float_of_vbox : config -> valuebox -> float
  val seq_of_vbox : config -> valuebox -> stringval Seq.seq
  val stringval_of_vbox : config -> valuebox -> stringval
  val handle_of_vbox : config -> valuebox -> Omake_os.Handle.t

  val coerce : config -> usertype -> valuebox -> valuebox
  (** Coerces to the given type. Follows dynamic values, and extracts from
      already computed lazy values.
   *)

  val string_of_stringval : stringval -> string
  val flat_vbox_of_stringval : stringval -> valuebox
  val vbox_of_stringval : stringval -> valuebox

  val length_of_vbox : config -> valuebox -> int
  val nth_of_vbox : config -> int -> valuebox -> valuebox

  val concat_vbox : config -> valuebox array -> stringval Seq.seq

  val extend_vbox : config -> valuebox -> cls option ->
                    (string*valuebox) list -> valuebox
  val update_vbox : config -> valuebox ->
                    (string*valuebox) list -> valuebox
  val field_of_vbox : config -> valuebox -> cls option -> string -> valuebox
  val class_of_vbox : config -> valuebox -> cls

  val create_alloc : unit -> alloc

  val debug_string_of_vbox : config -> valuebox -> string
end

module Vm : sig
  (* TODO: exceptions *)

  type instruction =
    | I_function of { arity_min : int; arity_max : int; arity_names : int;
                      framesize : int }
      (** Beginning of a function body taking [arity] arguments, and
          a frame of [framesize] registers. If there are less than [nargs]
          arguments, the function returns immediately a closure where
          [clos_args] is set to the existing arguments.
          The convention is that the register 0 is set to the globals,
          and the registers 1 to [nargs] are set to the arguments.
       *)
    | I_return of { reg:int; glbreg:int }
      (** The register [reg] contains the return value. [glbreg] contains
          the new globals. [arity] arguments are
          removed from the beginning of the argument list. If any arguments
          remain and the register contains a closure, the arguments are added to
          [clos_args]. If now enough arguments are present so that the closure
          becomes callable, the call is done (as jump).
       *)
    | I_apply of { reg:int; glbreg:int; closreg:int; named:(string * int) array;
                   args:int array }
      (** Calls the closure in [closreg] with the arguments from the
          registers in [args]. The return value is stored in [reg].
          The register [glbreg] contains the globals passed on to the closure.
          This register is also filled with the globals coming from the closure.
          If there are not enough arguments so that the closure is callable,
          a new closure is returned where the given args are added to
          [clos_args].
       *)
    | I_applyjump of { glbreg:int; closreg:int; named:(string * int) array;
                       args:int array }
      (** Jumps to the closure in [closreg] with the arguments from the
          registers in [args]. This is logically the same as I_apply followed
          by I_return, and so the following also applies: [arity] arguments are
          removed from the beginning of the argument list of the current
          function (NOT the called one). The remaining arguments are added
          to the arg list of the called function. If the function is now
          callable (enough args), it is jumped to the entry point. Otherwise,
          the closure to call is returned.
       *)
    | I_thread of { reg:int; glbreg:int; closreg:int; named:(string * int) array;
                    args:int array }
       (** Like [I_apply] but the new function is started in a new thread.
           The current thread also continues to run.
        *)
    | I_closure of { reg:int; closreg:int; env:int}
       (** Creates a new closure by modifying the environment of the closure
           in [closreg] and storing the resulting closure in [reg]. The
           environment must be an array of values.
        *)
    | I_endsnippet
    | I_primitive of { prim:int; reg:int; args:int array; glbreg:int}
    | I_move of { reg : int; src_reg : int }
    | I_load of { reg : int; value : Types.valuebox }
    | I_getenv of { reg : int; envidx : int }
    | I_envind of { reg : int; indreg : int }
    | I_envmove of { envidx : int; input : int }
    | I_env of { reg:int; inputs: int array }
    | I_locglobalvar of { reg : int; namereg : int; glbreg:int }
      (** Looks up a variable by name and puts the slot number into [reg].
          If the variable does not exist, a new slot is allocated.
        *)
    | I_locsharedfun of { reg : int; namereg : int; glbreg:int }
    | I_testvar of { reg : int; namereg : int; glbreg:int }
      (** Checks whether a variable with this name exists. Put the bool
          result into [reg]
       *)
    | I_testfun of { reg : int; namereg : int; glbreg:int }
    | I_getglobalvar of { reg : int; glbreg:int; slot : int }
    | I_getglobalvari of { reg : int; glbreg:int; slotreg : int }
    | I_getsharedfun of { reg : int; glbreg:int; slot : int }
    | I_getsharedfuni of { reg : int; glbreg:int; slotreg : int }
    | I_addglobalvar of { reg : int; glbreg:int; slot : int }
    | I_addglobalvari of { reg : int; glbreg:int; slotreg : int }
      (** put the value in [reg] into the [slot] of the globals *)
    | I_coerce of { reg:int; input:int; utype:Types.usertype }
    | I_lazy of { reg:int; input:int }
    | I_force1 of { reg:int; tmpreg:int; glbreg:int }
    | I_force2 of { reg:int; tmpreg:int; glbreg:int }
    | I_seq of { reg:int; inputs:int array }
      (** Creates a new sequence from the values in [inputs] and puts it into
          [reg]. If the inputs are already sequences, the value is flattened.
       *)
    | I_array of { reg:int; inputs:int array }
      (** Creates a new array from the values in [inputs] and puts it into
          [reg].
       *)
    | I_array_seq of { reg:int; input:int }
      (** Creates a new array from the elements of a sequence *)
    | I_concat of { reg:int; inputs:int array }
      (** concatenates the elements in [inputs] *)
    | I_quote of { qchar:char; reg:int; input:int }
      (** Creates a string by quoting the string or sequecne in [input] *)
    | I_object of { reg:int; cls:string }
      (** The empty object with class attribute [cls] *)
    | I_extend of { reg:int; cls:string option; in1:int; name:string; in2:int }
      (** Adds to the object in [in1] a mapping of [name] to [in2] and sets
          the class attribute to [cls] *)
    | I_update of { reg:int; in1:int; name:string; in2:int }
      (** This update of the object in [reg] doesn't modify the class *)
    | I_length of  { reg:int; input:int}
      (** Length of arrays or sequences *)
    | I_nth of { reg:int; in1:int; in2:int}
      (** The nth-element of an array of sequence (n in [in1], the array or
          sequence in [in2])
       *)
    | I_add of { reg:int; in1:int; in2:int}   (** Integer addition *)
    | I_sub of { reg:int; in1:int; in2:int}   (** Integer subtraction *)
    | I_mul of { reg:int; in1:int; in2:int}   (** Integer multiplication *)
    | I_div of { reg:int; in1:int; in2:int}   (** Integer division *)
    | I_mod of { reg:int; in1:int; in2:int}   (** Integer modulo *)
    | I_eq of { reg:int; in1:int; in2:int}    (** Integer equality *)
    | I_ne of { reg:int; in1:int; in2:int}    (** Integer inqquality *)
    | I_lt of { reg:int; in1:int; in2:int}    (** Integer less-than *)
    | I_le of { reg:int; in1:int; in2:int}    (** Integer less-or-equal-than *)
    | I_isuninit of { reg:int; in1:int }
    | I_if of { in1:int; ontrue:jump; onfalse:jump}
     (** If [in1] is true, advances the PC by [ontrue], otherwise by
         [onfalse]
      *)
    | I_jump of { jump:jump }
    | I_settrap of { reg:int; handler:jump }
    | I_deltrap
    | I_raise of { reg:int }
    | I_objget of { reg:int; in1:int; name:string; cls:string option }
    | I_objcls of { reg:int; in1:int }
    | I_ref of { reg:int; in1:int }
      (** Creates a reference in [reg] initially with content [in1] *)
    | I_mutate of { reg:int; in1:int }
      (** The reference in [reg] is set to the value in [in1] *)
    | I_test of { reg:int; handle:int}
      (** Tests whether the handle in register [handle] is able to do I/O
          and puts this information into [reg] as an array like [I_wait].
       *)
    | I_wait of { reg:int; handle:int }
      (** Wait until the handle in register [handle] can do I/O. The handle
          can be an [Async_set] (aggregate wait) but also any other handle
          (single wait). When any of the handles in the set or the single
          handle can do I/O the execution of the thread resumes. The register
          [reg] is set to an array containing the tags of the handles that
          can do I/O. In the aggregate case, the tags are taken from the
          [Async_set] container. In the single wait case, the tag is
          the string "READY". The array is empty when no handle can do I/O
          but we ran into a timeout.
       *)
    | I_label of string
    (* TODO: start closure as coroutine *)

   and jump =
     | Rel of int
     | Lab of string

  val relocate : instruction array -> instruction array
  val code : Types.alloc -> instruction array -> Types.code
  val instructions : Types.code -> instruction array

  val run : Types.config -> Types.global -> Types.closure -> Types.valuebox Seq.seq ->
            Types.global * Types.valuebox

  val run_snippet : Types.config -> Types.global ->
                    Types.valuebox array ->
                    Types.code -> unit

  val closure : entry:int -> arity_min:int -> arity_max:int ->
                autocurry:bool -> names:string array -> string -> Types.code ->
                Types.closure
end

module Global : sig
  type t = Types.global

  val create : Types.shared -> t
  val add_variable_slot : string -> t -> Lookup_table.slot
  val add_variable : Lookup_table.slot -> Types.valuebox -> t -> t
(*
  val add_typee_slot : string -> t -> t * Lookup_table.slot
  val add_type : Lookup_table.slot -> XXX -> t -> t
 *)
  val optimize : t -> t
end

module Shared : sig
  type t = Types.shared

  val create : Types.alloc -> t
  val add_function_slot : string -> t -> Lookup_table.slot
  val add_function : Lookup_table.slot -> Types.closure -> t -> unit
  val optimize : t -> unit
end

module Config : sig
  val standard_config : Types.config
end
