open Printf
module SMap = Map.Make(String)

type global_kind =
  | Global_function
  | Global_variable
  | Global_type

 and alloc =
   { alloc_functions : Lookup_table.allocator;
     alloc_variables : Lookup_table.allocator;
     mutable alloc_dummy : int;    (* allow == for alloc *)
   }

 and shared =
   { alloc : alloc;
     mutable shared_functions : closure option Lookup_table.t;
   }

and global =
  { shared : shared;
    global_variables : valuebox Lookup_table.t;
    (* global_types : XXX Lookup_table.t; *)
    (* global_procenv : string Lookup_table.t *)
    (* also here: stdin, stdout, stderr, cwd, ... *)
  }

and closure =
  { clos_slot : Lookup_table.slot;    (* of Global_function *)
    clos_entry : int;
    clos_env : valuebox array;
    clos_args : valuebox Seq.seq;
    clos_arity_min : int;
    clos_arity_max : int;
    clos_autocurry : bool;
    clos_names : string array;
    clos_code : code;
  }

and code = ..
  (* extended in Vm *)

and stringval =
  | V_bool of bool
  | V_int of int
  | V_float of float
  | V_string of string
  | V_seq of stringval Seq.seq
  | V_quote of char * stringval

and _ repr =
  | R_uninitialized : unit repr
  | R_bool : bool repr
  | R_int : int repr
  | R_float : float repr
  | R_string : string repr
  | R_seq : stringval Seq.seq repr
  | R_quote : char -> stringval repr
  | R_array : valuebox Seq.seq repr
  | R_dynamic : valuebox repr
  | R_closure : closure repr
  | R_lazy : lazyval repr
  | R_ref : reference repr
  | R_global : global repr
  | R_regind : (frame_id * int) repr
  | R_env : valuebox array repr
  | R_object : (cls * unit) repr
  | R_extend : (cls * object_extension) repr
  | R_update : object_extension repr
  | R_handle : Omake_os.Handle.t repr
  | R_custom : custom repr
      (* R_regind: pair of register bank and reg number. This is for
         storing private export destinations in environments

         R_env: this form of array must only be used for environments
       *)

and lazyval =
  Lz of { mutable value : valuebox;
          compute : closure
        }

and reference =
  Ref of { mutable value : valuebox }

and _ value =
  | V : 'a repr * 'a -> 'a value

and valuebox =
  | VBox : 'a repr * 'a -> valuebox

and cls =
  | Class of string

and usertype =
  | T_unit
  | T_bool
  | T_int
  | T_float
  | T_string
  | T_seq
  | T_array
  | T_tuple of usertype array
  | T_fun of {arity:int}
(* add: T_class *)

 and object_extension =
   Objext of { base : valuebox;
               fields : valuebox SMap.t;
             }

and frame_id = int
and custom = ..

type primitive = global -> valuebox array -> valuebox

type config =
  Config of
    { class_of : valuebox -> string;
      field_of : valuebox -> cls option -> string -> valuebox;
      coerce_to : usertype -> valuebox -> valuebox;
      debug_string : custom -> string;
      primitives : (string * primitive) array;
    }

let next_frame_id = ref 0
let get_frame_id() = let k = !next_frame_id in incr next_frame_id; k

let create_alloc() =
  { alloc_functions = Lookup_table.create_alloc();
    alloc_variables = Lookup_table.create_alloc();
    alloc_dummy = 0
  }

let v_false = V_bool false
let v_true = V_bool true
let v_bool b = if b then v_true else v_false
let v_int k = V_int k
let v_float x = V_float x
let v_string s = V_string s
let v_quote (q,sv) = V_quote(q,sv)

let v_normseq s =
  (* auto-flattening *)
  if Seq.exists (function V_seq _ -> true | _ -> false) s then
    Seq.flat_map
      (function
       | V_seq s1 -> Seq.Flatten s1
       | other -> Seq.Single other
      )
      s
  else
    s

let v_seq s = V_seq(v_normseq s)

let normarray s =
  (* auto-flattening *)
  if Seq.exists (function VBox(R_array,_) -> true | _ -> false) s then
    Seq.flat_map
      (function
       | VBox(R_array,a1) -> Seq.Flatten a1
       | other -> Seq.Single other
      )
      s
  else
    s

let value : type t . t repr -> t -> t value =
  fun repr x ->
  match repr with
    | R_seq ->
        V(repr, v_normseq x)
    | R_array ->
        V(repr, normarray x)
    | _ ->
        V(repr, x)

let valuebox : type t . t repr -> t -> valuebox =
  fun repr x ->
  match repr with
    | R_seq ->
        VBox(repr, v_normseq x)
    | R_array ->
        VBox(repr, normarray x)
    | _ ->
        VBox(repr, x)

let vbox_false = VBox(R_bool, false)
let vbox_true = VBox(R_bool, true)

let vbox_of_bool =
  function
  | false -> vbox_false
  | true -> vbox_true

exception Unexpected_type_of_value
exception Undefined_value
exception No_such_field

let forced_value : lazyval -> valuebox =
  function
  | Lz lz ->
      ( match lz.value with
          | VBox(R_uninitialized, _) ->
              failwith "forced_value: found unevaluated lazy value"
          | _ ->
              lz.value
      )

let global_of_vbox : valuebox -> global =
  function
  | VBox(R_global, g) -> g
  | _ -> raise Unexpected_type_of_value

let env_of_vbox : valuebox -> valuebox array =
  function
  | VBox(R_env, a) -> a
  | _ -> raise Unexpected_type_of_value

let reference_of_vbox : valuebox -> reference =
  function
  | VBox(R_ref, r) -> r
  | _ -> raise Unexpected_type_of_value

let rec closure_of_vbox config : valuebox -> closure =
  function
  | VBox(R_closure, c) -> c
  | VBox(R_dynamic, vbox) -> closure_of_vbox config (vbox:valuebox)
  | VBox(R_lazy, lz) -> closure_of_vbox config (forced_value lz)
  | VBox(R_uninitialized, _) -> raise Undefined_value
  | VBox(R_custom, _) as vbox ->
      let Config cfg = config in
      closure_of_vbox config (cfg.coerce_to (T_fun{arity=0}) vbox)
  | VBox(R_ref, Ref r) -> closure_of_vbox config r.value
  | _ -> raise Unexpected_type_of_value

let rec array_of_vbox config : valuebox -> valuebox Seq.seq =
  function
  | VBox(R_array, a) -> a
  | VBox(R_dynamic, vbox) -> array_of_vbox config (vbox:valuebox)
  | VBox(R_lazy, lz) -> array_of_vbox config (forced_value lz)
  | VBox(R_uninitialized, _) -> raise Undefined_value
  | VBox(R_custom, _) as vbox ->
      let Config cfg = config in
      array_of_vbox config (cfg.coerce_to T_array vbox)
  | VBox(R_ref, Ref r) -> array_of_vbox config r.value
  | _ -> raise Unexpected_type_of_value

let rec handle_of_vbox config : valuebox -> Omake_os.Handle.t =
  function
  | VBox(R_handle, h) -> h
  | VBox(R_dynamic, vbox) -> handle_of_vbox config (vbox:valuebox)
  | VBox(R_lazy, lz) -> handle_of_vbox config (forced_value lz)
  | VBox(R_uninitialized, _) -> raise Undefined_value
  | VBox(R_custom, _) as vbox ->
      let Config cfg = config in
      handle_of_vbox config (cfg.coerce_to T_array vbox)
  | VBox(R_ref, Ref r) -> handle_of_vbox config r.value
  | _ -> raise Unexpected_type_of_value

let rec seq_of_vbox config vbox =
  match vbox with
    | VBox(R_bool, b) -> Seq.make 1 (V_bool b)
    | VBox(R_int, i) -> Seq.make 1 (V_int i)
    | VBox(R_float, f) -> Seq.make 1 (V_float f)
    | VBox(R_string, s) -> Seq.make 1 (V_string s)
    | VBox(R_seq, s) -> s
    | VBox(R_quote q, s) -> Seq.make 1 (V_quote(q,s))
    | VBox(R_dynamic, vbox) -> seq_of_vbox config (vbox:valuebox)
    | VBox(R_lazy, lz) -> seq_of_vbox config (forced_value lz)
    | VBox(R_uninitialized, _) -> raise Undefined_value
    | VBox(R_custom, _) as vbox ->
        let Config cfg = config in
        seq_of_vbox config (cfg.coerce_to T_seq vbox)
    | VBox(R_ref, Ref r) -> seq_of_vbox config r.value
    | _ -> raise Unexpected_type_of_value

let rec string_of_stringval =
  function
  | V_bool b -> string_of_bool b
  | V_int i -> string_of_int i
  | V_float x -> string_of_float x
  | V_string s -> s
  | V_seq s -> string_of_stringval_seq s
  | V_quote(q, s) -> string_of_stringval_quote q s

and string_of_stringval_seq s =
  String.concat " " (List.map string_of_stringval (Seq.to_list s))

and string_of_stringval_quote q s =
  let qs = String.make 1 q in
  String.concat "" [qs; string_of_stringval s; qs]

let flat_vbox_of_stringval =
  function
  | V_bool b -> VBox(R_bool, b)
  | V_int i -> VBox(R_int, i)
  | V_float x -> VBox(R_float, x)
  | V_string s -> VBox(R_string, s)
  | V_seq s -> VBox(R_string, string_of_stringval_seq s)
  | V_quote(q,s) -> VBox(R_quote q, s)

let rec vbox_of_stringval =
  function
  | V_bool b -> VBox(R_bool, b)
  | V_int i -> VBox(R_int, i)
  | V_float x -> VBox(R_float, x)
  | V_string s -> VBox(R_string, s)
  | V_seq s -> VBox(R_seq, s)
  | V_quote(q,s) -> VBox(R_quote q, s)

let rec string_of_vbox config : valuebox -> string =
  function
  | VBox(R_bool, b) -> string_of_bool b
  | VBox(R_int, i) -> string_of_int i
  | VBox(R_float, x) -> string_of_float x
  | VBox(R_string, s) -> s
  | VBox(R_seq, s) -> string_of_stringval_seq s
  | VBox(R_quote q, s) -> string_of_stringval_quote q s
  | VBox(R_dynamic, vbox) -> string_of_vbox config (vbox:valuebox)
  | VBox(R_lazy, lz) -> string_of_vbox config (forced_value lz)
  | VBox(R_uninitialized, _) -> raise Undefined_value
  | VBox(R_custom, _) as vbox ->
      let Config cfg = config in
      string_of_vbox config (cfg.coerce_to T_string vbox)
  | VBox(R_ref, Ref r) -> string_of_vbox config r.value
  | _ ->
      raise Unexpected_type_of_value

let int_of_vbox config : valuebox -> int =
  function
  | VBox(R_int, i) -> i
  | vbox ->
      let s = string_of_vbox config vbox in
      ( try int_of_string s
        with Failure _ -> raise Unexpected_type_of_value
      )

let bool_of_vbox config : valuebox -> bool =
  function
  | VBox(R_bool, b) -> b
  | vbox ->
      let s = string_of_vbox config vbox in
      ( try bool_of_string s
        with Failure _ -> raise Unexpected_type_of_value
      )

let float_of_vbox config : valuebox -> float =
  function
  | VBox(R_float, x) -> x
  | vbox ->
      let s = string_of_vbox config vbox in
      ( try float_of_string s
        with Failure _ -> raise Unexpected_type_of_value
      )
        
let rec stringval_of_vbox config : valuebox -> stringval =
  function
  | VBox(R_bool, b) -> V_bool b
  | VBox(R_int, i) -> V_int i
  | VBox(R_float, x) -> V_float x
  | VBox(R_string, s) -> V_string s
  | VBox(R_seq, s) -> V_seq s
  | VBox(R_quote q, s) -> V_quote(q,s)
  | VBox(R_dynamic, vbox) -> stringval_of_vbox config (vbox:valuebox)
  | VBox(R_lazy, lz) -> stringval_of_vbox config (forced_value lz)
  | VBox(R_uninitialized, _) -> raise Undefined_value
  | VBox(R_custom, _) as vbox ->
      let Config cfg = config in
      stringval_of_vbox config (cfg.coerce_to T_seq vbox)
  | VBox(R_ref, Ref r) -> stringval_of_vbox config r.value
  | _ -> raise Unexpected_type_of_value

let rec length_of_vbox config : valuebox -> int =
  (* number of seq or array elements *)
  function
  | VBox(R_bool, _) -> 1
  | VBox(R_int, _) -> 1
  | VBox(R_float, _) -> 1
  | VBox(R_string, _) -> 1
  | VBox(R_seq, s) -> Seq.length s
  | VBox(R_quote q, s) -> 1
  | VBox(R_array,s) -> Seq.length s
  | VBox(R_dynamic, vbox) -> length_of_vbox config (vbox:valuebox)
  | VBox(R_lazy, lz) -> length_of_vbox config (forced_value lz)
  | VBox(R_uninitialized, _) -> raise Undefined_value
  | VBox(R_custom, _) as vbox ->
      let Config cfg = config in
      length_of_vbox config (cfg.coerce_to T_array vbox)
  | VBox(R_ref, Ref r) -> length_of_vbox config r.value
  | _ -> raise Unexpected_type_of_value

let rec nth_of_vbox config : int -> valuebox -> valuebox =
  fun n ->
  function
  | VBox(R_bool, _) as vbox -> if n=0 then vbox else raise Not_found
  | VBox(R_int, _) as vbox -> if n=0 then vbox else raise Not_found
  | VBox(R_float, _) as vbox -> if n=0 then vbox else raise Not_found
  | VBox(R_string, _) as vbox -> if n=0 then vbox else raise Not_found
  | VBox(R_quote q, _) as vbox -> if n=0 then vbox else raise Not_found
  | VBox(R_seq, s) ->
      if n >= 0 && n < Seq.length s then
        flat_vbox_of_stringval (Seq.get s n)
      else
        raise Not_found
  | VBox(R_array,s) ->
      if n >= 0 && n < Seq.length s then
        Seq.get s n
      else
        raise Not_found
  | VBox(R_dynamic, vbox) -> nth_of_vbox config n (vbox:valuebox)
  | VBox(R_lazy, lz) -> nth_of_vbox config n (forced_value lz)
  | VBox(R_uninitialized, _) -> raise Undefined_value
  | VBox(R_custom, _) as vbox ->
      let Config cfg = config in
      nth_of_vbox config n (cfg.coerce_to T_array vbox)
  | VBox(R_ref, Ref r) -> nth_of_vbox config n r.value
  | _ -> raise Unexpected_type_of_value

let unit_vbox = VBox(R_seq, Seq.empty)

let rec coerce_to_unit config : valuebox -> valuebox =
  function
    | VBox(R_seq, s) ->
        if Seq.length s <> 0 then raise Unexpected_type_of_value;
        unit_vbox
    | VBox(R_dynamic, vbox) -> coerce_to_unit config (vbox:valuebox)
    | VBox(R_lazy, lz) -> coerce_to_unit config (forced_value lz)
    | VBox(R_uninitialized, _) -> raise Undefined_value
    | VBox(R_custom, _) as vbox ->
        let Config cfg = config in
        coerce_to_unit config (cfg.coerce_to T_unit vbox)
    | VBox(R_ref, Ref r) -> coerce_to_unit config r.value
    | _ ->
        raise Unexpected_type_of_value

let coerce_to_int config v =
  match v with
    | VBox(R_int, _) -> v
    | _ -> VBox(R_int, int_of_vbox config v)

let coerce_to_bool config v =
  match v with
    | VBox(R_bool, _) -> v
    | _ -> VBox(R_bool, bool_of_vbox config v)

let coerce_to_float config v =
  match v with
    | VBox(R_float, _) -> v
    | _ -> VBox(R_float, float_of_vbox config v)

let coerce_to_string config v =
  match v with
    | VBox(R_string, _) -> v
    | _ -> VBox(R_string, string_of_vbox config v)

let rec coerce_to_seq config v =
  match v with
    | VBox(R_bool, _) -> v
    | VBox(R_int, _) -> v
    | VBox(R_float, _) -> v
    | VBox(R_string, _) -> v
    | VBox(R_seq, _) -> v
    | VBox(R_quote _, _) -> v
    | VBox(R_dynamic, vbox) -> coerce_to_seq config (vbox:valuebox)
    | VBox(R_lazy, lz) -> coerce_to_seq config (forced_value lz)
    | VBox(R_uninitialized, _) -> raise Undefined_value
    | VBox(R_custom, _) as vbox ->
        let Config cfg = config in
        coerce_to_seq config (cfg.coerce_to T_seq vbox)
    | VBox(R_ref, Ref r) -> coerce_to_seq config r.value
    | _ -> raise Unexpected_type_of_value

let rec coerce_to_array config v =
  match v with
    | VBox(R_array, _) -> v
    | VBox(R_dynamic, vbox) -> coerce_to_array config (vbox:valuebox)
    | VBox(R_lazy, lz) -> coerce_to_array config (forced_value lz)
    | VBox(R_uninitialized, _) -> raise Undefined_value
    | VBox(R_custom, _) as vbox ->
        let Config cfg = config in
        coerce_to_array config (cfg.coerce_to T_array vbox)
    | VBox(R_ref, Ref r) -> coerce_to_array config r.value
    | _ -> raise Unexpected_type_of_value

let rec coerce_to_fun config arity v =
  match v with
    | VBox(R_closure, clos) ->
        if clos.clos_arity_min < arity then
          raise Unexpected_type_of_value;
        v
    | VBox(R_dynamic, vbox) -> coerce_to_fun config arity (vbox:valuebox)
    | VBox(R_lazy, lz) -> coerce_to_fun config arity (forced_value lz)
    | VBox(R_uninitialized, _) -> raise Undefined_value
    | VBox(R_custom, _) as vbox ->
        let Config cfg = config in
        coerce_to_fun config arity (cfg.coerce_to (T_fun{arity}) vbox)
    | VBox(R_ref, Ref r) -> coerce_to_fun config arity r.value
    | _ ->
        raise Unexpected_type_of_value


let rec coerce config utype =
  match utype with
    | T_unit -> coerce_to_unit config
    | T_bool -> coerce_to_bool config
    | T_int -> coerce_to_int config
    | T_float -> coerce_to_float config
    | T_string -> coerce_to_string config
    | T_seq -> coerce_to_seq config
    | T_array -> coerce_to_array config
    | T_fun {arity} -> coerce_to_fun config arity
    | T_tuple tup ->
        (fun v ->
          let v = coerce_to_array config v in
          let a = array_of_vbox config v in
          if Seq.length a <> Array.length tup then
            raise Unexpected_type_of_value;
          let a =
            Seq.mapi
              (fun i x ->
                let u = tup.(i) in
                coerce config u x
              )
              a in
          VBox(R_array, a)
        )

let concat_as_strings (sl:stringval list) =
  let l1 = List.map string_of_stringval sl in
  V_string(String.concat "" l1)

let rec concat_vbox config elems =
  let seqs = Array.map (seq_of_vbox config) elems in
  let n = Array.length seqs in
  let rec concat (buf: stringval Seq.seq) (buf_last: stringval list) k =
    (* invariant: the so far gathered stringvals are:
        - the stringvals in the sequence buf, plus
        - if buf_last <> []: the single stringval obtained by reversing the
          list buf_last and concatenating it
     *)
    if k < n then (
      let seq = seqs.(k) in
      let l = Seq.length seq in
      if l=0 then
        concat buf buf_last (k+1)
      else
        match buf_last with
          | [] ->
              assert(Seq.is_empty buf);
              let last = Seq.get seq (l-1) in
              let butlast = Seq.sub seq 0 (l-1) in
              concat butlast [last] (k+1)
          | _ ->
              if l=1 then
                let buf_last' = (Seq.hd seq) :: buf_last in
                concat buf buf_last' (k+1)
              else
                let head = Seq.hd seq in
                let middle = Seq.sub seq 1 (l-2) in
                let last = Seq.get seq (l-1) in
                let s = concat_as_strings (List.rev (head :: buf_last)) in
                let x = Seq.make 1 s in
                let buf' = Seq.concat [ buf; x; middle ] in
                concat buf' [last] (k+1)
    )
    else
      match buf_last with
        | [] -> buf
        | _ ->
            let s = concat_as_strings (List.rev buf_last) in
            Seq.append buf (Seq.make 1 s) in
  concat Seq.empty [] 0

let rec class_of_vbox config vbox =
  match vbox with
    | VBox(R_uninitialized, ()) -> raise Undefined_value
    | VBox(R_dynamic, vbox) -> class_of_vbox config (vbox:valuebox)
    | VBox(R_lazy, lz) -> class_of_vbox config (forced_value lz)
    | VBox(R_ref, Ref r) -> class_of_vbox config r.value
    | VBox(R_object, (c,_)) -> c
    | VBox(R_extend, (c,_)) -> c
    | VBox(R_update, Objext ox) -> class_of_vbox config ox.base
    | _ ->
        let Config cfg = config in
        Class(cfg.class_of vbox)


let rec extend_vbox config base c_opt fields =
  let add_fields m =
    List.fold_left
      (fun acc (n,v) -> SMap.add n v acc)
      m
      fields in
  match base with
    | VBox(R_uninitialized, ()) -> raise Undefined_value
    | VBox(R_dynamic, vbox) -> extend_vbox config (vbox:valuebox) c_opt fields
    | VBox(R_lazy, lz) -> extend_vbox config (forced_value lz) c_opt fields
    | VBox(R_ref, Ref r) -> extend_vbox config r.value c_opt fields
    | VBox(R_extend, (Class c', Objext ox')) ->
        ( match c_opt with
            | None ->
                let fields = add_fields ox'.fields in
                let ox = Objext { base=ox'.base; fields } in
                VBox(R_extend, (Class c', ox))
            | Some (Class c) when c=c' ->
                let fields = add_fields ox'.fields in
                let ox = Objext { base=ox'.base; fields } in
                VBox(R_extend, (Class c, ox))
            | Some (Class c) ->
                let fields = add_fields SMap.empty in
                let ox = Objext { base; fields } in
                VBox(R_extend, (Class c, ox))
        )
    | _ ->
        let Class c =
          match c_opt with
            | None -> class_of_vbox config base
            | Some c -> c in
        let fields = add_fields SMap.empty in
        let ox = Objext { base; fields } in
        VBox(R_extend, (Class c, ox))

let rec update_vbox config base fields =
  let add_fields m =
    List.fold_left
      (fun acc (n,v) -> SMap.add n v acc)
      m
      fields in
  match base with
    | VBox(R_uninitialized, ()) -> raise Undefined_value
    | VBox(R_dynamic, vbox) -> update_vbox config (vbox:valuebox) fields
    | VBox(R_lazy, lz) -> update_vbox config (forced_value lz) fields
    | VBox(R_ref, Ref r) -> update_vbox config r.value fields
    | VBox(R_update, Objext ox) ->
        let fields = add_fields ox.fields in
        VBox(R_update, Objext { ox with fields })
    | _ ->
        let fields = add_fields SMap.empty in
        VBox(R_update, Objext { base; fields })

let rec field_of_vbox config vbox cls_opt name =
  match vbox with
    | VBox(R_uninitialized, ()) -> raise Undefined_value
    | VBox(R_dynamic, vbox) -> field_of_vbox config (vbox:valuebox) cls_opt name
    | VBox(R_lazy, lz) -> field_of_vbox config (forced_value lz) cls_opt name
    | VBox(R_ref, Ref r) -> field_of_vbox config r.value cls_opt name
    | VBox(R_object, _) -> raise No_such_field
    | VBox(R_extend, (Class c, Objext ox)) ->
        let eligible =
          match cls_opt with
            | Some (Class c') -> c = c'
            | None -> true in
        ( try
            if not eligible then raise Not_found;
            SMap.find name ox.fields
          with Not_found ->
            field_of_vbox config ox.base cls_opt name
        )
    | VBox(R_update, Objext ox) ->
        ( try
            if cls_opt <> None then raise Not_found;
            SMap.find name ox.fields
          with Not_found ->
            field_of_vbox config ox.base cls_opt name
        )
    | _ ->
        let Config cfg = config in
        cfg.field_of vbox cls_opt name

let rec debug_string_of_vbox config : valuebox -> string =
  function
  | VBox(R_uninitialized, ()) -> "<uninit>"
  | VBox(R_bool, b) -> sprintf "<bool %B>" b
  | VBox(R_int, i) -> sprintf "<int %d>" i
  | VBox(R_float, x) -> sprintf "<float %f>" x
  | VBox(R_string, s) -> sprintf "<string %S>" s
  | VBox(R_seq, s) ->
      let s' =
        Seq.map
          string_of_stringval
          s in
      sprintf "<seq %s>" (String.concat " " (Seq.to_list s'))
  | VBox(R_quote q, s) ->
      sprintf "<quote %S>" (string_of_stringval s)
  | VBox(R_array, s) ->
      let s' = Seq.map (debug_string_of_vbox config) s in
      sprintf "<array %s>" (String.concat " " (Seq.to_list s'))
  | VBox(R_dynamic, vbox) ->
      sprintf "<dynamic %s>" (debug_string_of_vbox config (vbox : valuebox))
  | VBox(R_lazy, Lz lz) ->
      sprintf "<lazy %s>" (debug_string_of_vbox config lz.value)
  | VBox(R_closure, _) -> "<closure>"
  | VBox(R_global, _) -> "<global>"
  | VBox(R_regind, _) -> "<regind>"
  | VBox(R_env, _) -> "<env>"
  | VBox(R_handle, _) -> "<handle>"
  | VBox(R_custom, c)  ->
      let Config cfg = config in
      cfg.debug_string c
  | VBox(R_ref, Ref r) -> debug_string_of_vbox config r.value
  | VBox(R_object, (Class cls, _)) -> "<object <class " ^ cls ^ ">>"
  | VBox(R_extend, (Class cls, Objext ox)) ->
      "<extend " ^ debug_string_of_vbox config ox.base ^
        " <class " ^ cls ^ "> " ^
          (String.concat
             " "
             (List.map
                (fun (k,v) -> k ^ "=" ^ debug_string_of_vbox config v)
                (SMap.bindings ox.fields)
          )) ^
            ">"
  | VBox(R_update, Objext ox) ->
      "<update " ^ debug_string_of_vbox config ox.base ^
        " " ^
          (String.concat
             " "
             (List.map
                (fun (k,v) -> k ^ "=" ^ debug_string_of_vbox config v)
                (SMap.bindings ox.fields)
          )) ^
            ">"

(* Now the defs for Shared and Global: *)

module Shared = struct
  type t = shared

  let create alloc =
    let shared_functions = Lookup_table.create alloc.alloc_functions None in
    { alloc ; shared_functions }

  let add_function_slot name sh =
    Lookup_table.add_slot name sh.alloc.alloc_functions

  let add_function slot clos sh =
    let shfun = Lookup_table.add slot (Some clos) sh.shared_functions in
    sh.shared_functions <- shfun

  let optimize sh =
    sh.shared_functions <- Lookup_table.optimize sh.shared_functions
end

module Global = struct
  type t = global
             
  let create shared =
    let dfl_var = VBox(R_uninitialized, ()) in
    { shared;
      global_variables =
        Lookup_table.create shared.alloc.alloc_variables dfl_var;
    }

  let add_variable_slot name glb =
    Lookup_table.add_slot name glb.shared.alloc.alloc_variables

  let add_variable slot var glb =
    let glbvar = Lookup_table.add slot var glb.global_variables in
    { glb with
      global_variables = glbvar
    }

  let optimize glb =
    Shared.optimize glb.shared;
    { glb with
      global_variables = Lookup_table.optimize glb.global_variables;
    }
end
                  
