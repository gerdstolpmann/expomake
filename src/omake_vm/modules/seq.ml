type 't seq =
  | Skip of int * 't seq
  | Concat of int * 't seq * int * 't seq
  | Vector of 't array

type 't map_type =
  | Single of 't
  | Flatten of 't seq

let empty = Vector [| |]

let length s =
  let rec count n =
    function
    | Skip(n_skip,s') -> count (n-n_skip) s'
    | Concat(nl,_,nr,_) -> n + nl + nr
    | Vector vec -> n + Array.length vec in
  count 0 s

let is_empty s =
  length s = 0

let rec get s k =
  if k < 0 then invalid_arg "Seq.get";
  match s with
    | Skip(n_skip,s') ->
        get s' (k+n_skip)
    | Concat(nl, sl, nr, sr) ->
        if k < nl then
          get sl k
        else
          get sr (k-nl)
    | Vector vec ->
        if k >= Array.length vec then invalid_arg "Seq.get";
        vec.(k)

let skip k s =
  if length s < k then invalid_arg "Seq.skip";
  match s with
    | Skip(n_skip,s') -> Skip(n_skip+k,s')
    | _ -> Skip(k,s)


let hd s = get s 0
let tl s = skip 1 s

let make n x =
  Vector(Array.make n x)

let init n f =
  Vector(Array.init n f)

let of_array a =
  Vector(Array.copy a)

let of_list l =
  Vector(Array.of_list l)

let to_array_list s =
  let l = ref [] in
  let rec construct n =
    function
    | Skip(n_skip,s') ->
        construct (n+n_skip) s'
    | Concat(nl,sl,nr,sr) ->
        if n < nl then (
          construct 0 sr;
          construct n sl;
        ) else
          construct (n-nl) sr
    | Vector vec ->
        l := Array.sub vec n (Array.length vec - n) :: !l in
  construct 0 s;
  !l

let to_array s =
  Array.concat(to_array_list s)

let optimize s =
  Vector(to_array s)

let to_list s =
  Array.to_list(to_array s)

let append s1 s2 =
  let n1 = length s1 in
  let n2 = length s2 in
  Concat(n1,s1,n2,s2)

let concata sarr =
  let rec build l r =
    if l = r then
      sarr.(l)
    else
      let m = l + ((r-l)/2) in
      let sl = build l m in
      let sr = build (m+1) r in
      let nl = length sl in
      let nr = length sr in
      Concat(nl,sl,nr,sr) in
  build 0 (Array.length sarr-1)

let concat slist = concata (Array.of_list slist)
                           
let sub s p n =
  let len = length s in
  if p < 0 || n < 0 || n >= len || p > len - n then
    invalid_arg "Seq.sub";
  let l = ref [] in
  let rec construct p n =
    function
    | Skip(n_skip,s') ->
        construct (p+n_skip) n s'
    | Concat(nl,sl,nr,sr) ->
        if p < nl then (
          let ml = min (nl-p) n in
          if n > ml then construct 0 (n-ml) sr;
          construct p ml sl;
        ) else
          construct (p-nl) n sr
    | Vector vec ->
        l := Vector(Array.sub vec p n) :: !l in
  construct p n s;
  concat !l

let copy = optimize

let rev_array a =
  let a' = Array.copy a in
  let n = Array.length a in
  for i = 0 to n - 1 do
    a'.(n-1-i) <- a.(i)
  done;
  a'

let rev s =
  let l = to_array_list s in
  let l = List.rev_map rev_array l in
  Vector(Array.concat l)

let fold_left f acc s =
  let acc = ref acc in
  let rec walk p =
    function
    | Skip(n_skip, s') ->
        walk (p+n_skip) s'
    | Concat(nl,sl,nr,sr) ->
        if p < nl then (
          walk p sl;
          walk 0 sr;
        ) else
          walk (p-nl) sr
    | Vector vec ->
        for i = p to Array.length vec - 1 do
          acc := f !acc vec.(i)
        done in
  walk 0 s;
  !acc

let fold_right f s acc =
  let acc = ref acc in
  let rec walk p =
    function
    | Skip(n_skip, s') ->
        walk (p+n_skip) s'
    | Concat(nl,sl,nr,sr) ->
        if p < nl then (
          walk 0 sr;
          walk p sl;
        ) else
          walk (p-nl) sr
    | Vector vec ->
        for i = Array.length vec - 1 downto p do
          acc := f vec.(i) !acc
        done in
  walk 0 s;
  !acc
   
let iter f s =
  fold_left (fun () x -> f x) () s

let iteri f s =
  ignore(fold_left (fun i x -> f i x; i+1) 0 s)
            
let map f s =
  let l = ref [] in
  fold_left (fun () x -> l := f x :: !l) () s;
  of_list(List.rev !l)

let mapi f s =
  let l = ref [] in
  ignore(fold_left (fun i x -> l := f i x :: !l; i+1) 0 s);
  of_list(List.rev !l)

let flat_mapi f s =
  (* intentionally make Single fast and punish Flatten *)
  let l = ref [] in
  ignore(
      fold_left
        (fun i x ->
          ( match f i x with
              | Single x1 -> l := x1 :: !l
              | Flatten xl -> l := List.rev_append (to_list xl) !l
          );
          i + 1
        )
        0 s
    );
  of_list(List.rev !l)

let flat_map f s =
  flat_mapi (fun i x -> f x) s

let mem x s =
  let exception Found in
  try
    iter (fun y -> if x=y then raise Found) s;
    false
  with Found -> true

let find : type t . (t->bool) -> t seq -> t =
  fun f s ->
  let exception Found of t in
  try
    iter (fun x -> if f x then raise(Found x)) s;
    raise Not_found
  with Found x -> x

let filter f s =
  let l = ref [] in
  fold_left (fun () x -> if f x then l := x :: !l) () s;
  of_list(List.rev !l)

let exists f s =
  let exception Found in
  try
    iter (fun x -> if f x then raise Found) s;
    false
  with Found -> true

let for_all f s =
  let exception Found in
  try
    iter (fun x -> if not (f x) then raise Found) s;
    true
  with Found -> false

(*
let s1 = of_array [| 1;2;3 |];;
let s2 = of_array [| 4;5;6 |];;
let s = append s1 (tl s2);;
length s;;
to_array s;;
get s 0;;
get s 1;;
get s 2;;
get s 3;;
get s 4;;
let u = concat [ of_list [1;2;3]; of_list [4;5;6]; of_list[7] ];;



 *)
