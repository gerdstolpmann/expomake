(* TODO:

   - debug trace
   - backtraces, locations
   - additional classes (map, etc.)
 *)

open Types
open Printf

type instruction =
  | I_function of { arity_min : int; arity_max : int; arity_names : int;
                    framesize : int }
  | I_return of { reg:int; glbreg:int }
  | I_apply of { reg:int; glbreg:int; closreg:int; named:(string * int) array;
                 args:int array }
  | I_applyjump of { glbreg:int; closreg:int; named:(string * int) array;
                     args:int array }
  | I_thread of { reg:int; glbreg:int; closreg:int; named:(string * int) array;
                  args:int array }
  | I_closure of { reg:int; closreg:int; env:int}
  | I_endsnippet
  | I_primitive of { prim:int; reg:int; args:int array; glbreg:int }
  | I_move of { reg : int; src_reg : int }
  | I_load of { reg : int; value : Types.valuebox }
  | I_getenv of { reg : int; envidx : int }
  | I_envind of { reg : int; indreg : int }
  | I_envmove of { envidx : int; input : int }
  | I_env of { reg:int; inputs: int array }
  | I_locglobalvar of { reg : int; namereg : int; glbreg:int }
  | I_locsharedfun of { reg : int; namereg : int; glbreg:int }
  | I_testvar of { reg : int; namereg : int; glbreg:int }
  | I_testfun of { reg : int; namereg : int; glbreg:int }
  | I_getglobalvar of { reg : int; glbreg:int; slot : int }
  | I_getglobalvari of { reg : int; glbreg:int; slotreg : int }
  | I_getsharedfun of { reg : int; glbreg:int; slot : int }
  | I_getsharedfuni of { reg : int; glbreg:int; slotreg : int }
  | I_addglobalvar of { reg : int; glbreg:int; slot : int }
  | I_addglobalvari of { reg : int; glbreg:int; slotreg : int }
  | I_coerce of { reg:int; input:int; utype:Types.usertype }
  | I_lazy of { reg:int; input:int }
  | I_force1 of { reg:int; tmpreg:int; glbreg:int }
  | I_force2 of { reg:int; tmpreg:int; glbreg:int }
  | I_seq of { reg:int; inputs:int array }
  | I_array of { reg:int; inputs:int array }
  | I_array_seq of { reg:int; input:int }
  | I_concat of { reg:int; inputs:int array }
  | I_quote of { qchar:char; reg:int; input:int }
  | I_object of { reg:int; cls:string }
  | I_extend of { reg:int; cls:string option; in1:int; name:string; in2:int }
  | I_update of { reg:int; in1:int; name:string; in2:int }
  | I_length of  { reg:int; input:int}
  | I_nth of { reg:int; in1:int; in2:int}
  | I_add of { reg:int; in1:int; in2:int}
  | I_sub of { reg:int; in1:int; in2:int}
  | I_mul of { reg:int; in1:int; in2:int}
  | I_div of { reg:int; in1:int; in2:int}
  | I_mod of { reg:int; in1:int; in2:int}
  | I_eq of { reg:int; in1:int; in2:int}
  | I_ne of { reg:int; in1:int; in2:int}
  | I_lt of { reg:int; in1:int; in2:int}
  | I_le of { reg:int; in1:int; in2:int}
  | I_isuninit of { reg:int; in1:int }
  | I_if of { in1:int; ontrue:jump; onfalse:jump}
  | I_jump of { jump:jump }
  | I_settrap of { reg:int; handler:jump }
  | I_deltrap
  | I_raise of { reg:int }
  | I_objget of { reg:int; in1:int; name:string; cls:string option }
  | I_objcls of { reg:int; in1:int }
  | I_ref of { reg:int; in1:int }
  | I_mutate of { reg:int; in1:int }
  | I_test of { reg:int; handle:int}
  | I_wait of { reg:int; handle:int }
  | I_label of string

 and jump =
   | Rel of int
   | Lab of string


type section =
  { instr : instruction array;
    alloc : alloc;
    run : (frame -> (* pc: *) int -> (* count: *) int -> instruction -> unit) array;
  }

 and result =
   { rvalue : valuebox;
     rglb : global
   }

 and frame =
   { id : frame_id;
     section : section;
     thread : int;
     vm : vm;
     mutable regs : valuebox array;
     glb : global;
     args : valuebox Seq.seq;
     named : valuebox array;
     mutable effarity : int;
     clos : closure;
     mutable trap : trap option
   }

 and trap =
   { trap_reg : int;
     trap_pc : int
   }

 and primitive = global -> valuebox array -> valuebox

 and state =
   | Running
   | Waiting of Omake_os.Handle.t Omake_os.Async_set.t
   | Ready of Omake_os.Types.wait_tag list
   | Finished

 and vm =
   { mutable current : (frame * int) array;
     (* (frame,pc) thread_array. This is not updated for the current thread *)
     mutable stack : (frame * int * int * int) Stack.t array;
     (* (frame, pc, ret_reg, glb_reg) call_stack thread_array *)
     mutable state : state array;
     (* state thread_array *)
     mutable result : result option;
     (* result of thread 0 *)
     config : config;
   }


type Types.code += Section of section

let thread_limit = 4000

let debug = ref false


let dummy_frame vm old_fr i =
   { id = get_frame_id();
     section = old_fr.section;
     thread = i;
     vm;
     regs = [| |];
     glb = old_fr.glb;
     args = Seq.empty;
     named = [| |];
     effarity = 0;
     clos = old_fr.clos;
     trap = None;
   }

let section_of_code code =
  match code with
    | Section s -> s
    | _ -> failwith "Unexpected type of code"

let section_of_closure clos =
  section_of_code clos.clos_code

let validate_names (names:string array) =
  let exception Error in
  try
    ignore(Array.fold_left
             (fun last name ->
               if name <= last then raise Error;
               name
             )
             ""
             names
          );
    true
  with Error -> false

let closure ~entry ~arity_min ~arity_max ~autocurry ~names name code =
  let section = section_of_code code in
  if arity_min < 0 ||
       arity_max < arity_min ||
         arity_min + Array.length names = 0 ||
           (autocurry && Array.length names > 0 && arity_min > 0) ||
             not (validate_names names) ||
               entry < 0 ||
                 entry >= Array.length section.instr
  then
    invalid_arg "Omake_vm.Vm.closure";
  let clos_slot = Lookup_table.add_slot name section.alloc.alloc_functions in
  { clos_slot;
    clos_entry = entry;
    clos_env = [| |];
    clos_args = Seq.empty;
    clos_arity_min = arity_min;
    clos_arity_max = arity_max;
    clos_autocurry = autocurry;
    clos_names = names;
    clos_code = code
  }

let wait_set h =
  let open Omake_os in
  match h with
    | Handle.H_async_set aset ->
        aset
    | _ ->
        let aset = Async_set.create() in
        Async_set.add
          [Types.Wait_string "READY"] h (Handle.handle h) aset

let wait_tags tags =
  let array =
    Seq.map
      (fun tag ->
        let seq =
          Seq.map
            (function
             | Omake_os.Types.Wait_int i -> V_int i
             | Omake_os.Types.Wait_string s -> V_string s
            )
            (Seq.of_list tag) in
        valuebox R_seq seq
      )
      (Seq.of_list tags) in
  valuebox R_array array

let runnable =
  function
  | Running -> true
  | Ready _ -> true
  | _ -> false

let rec yield fr pc =
  let vm = fr.vm in
  let thread = fr.thread in
  vm.current.(thread) <- (fr, pc);
  switch vm thread

and switch vm old_thread =
  let n = Array.length vm.state in
  let i = ref (if old_thread+1 = n then 0 else old_thread+1) in
  while !i <> old_thread && not (runnable vm.state.(!i)) do
    incr i;
    if !i = n then i := 0
  done;
  match vm.state.(!i) with
    | Running ->
        if !debug then
          eprintf "SWITCH to running thread %d\n%!" !i;
        let (fr, pc) = vm.current.(!i) in
        exec1 fr pc 0
    | Ready tags ->
        if !debug then
          eprintf "SWITCH to resuming thread %d\n%!" !i;
        let (fr, pc) = vm.current.(!i) in
        resume_i_wait fr pc tags
    | _ ->
        wait vm old_thread

and wait vm old_thread =
  if !debug then eprintf "WAIT\n%!";
  let waiting =
    Array.fold_left
      (fun acc (i,state) ->
        match state with
          | Waiting aset ->
              (Omake_os.Types.Wait_int i, aset) :: acc
          | _ ->
              acc
      )
      []
      (Array.mapi (fun i st -> (i,st)) vm.state) in
  if !debug then eprintf "  #waiting threads: %d\n%!" (List.length waiting);
  let agg_set = Omake_os.Async_set.union waiting in
  let runnable = Omake_os.Async_set.wait agg_set infinity in
  let ht = Hashtbl.create 31 in
  List.iter
    (fun tag ->
      match tag with
        | Omake_os.Types.Wait_int i :: rtag ->
            let l = try Hashtbl.find ht i with Not_found -> [] in
            Hashtbl.replace ht i (rtag :: l)
        | _ ->
            assert false
    )
    runnable;
  Hashtbl.iter
    (fun i tags ->
      if !debug then eprintf "READY thread %d\n%!" i;
      vm.state.(i) <- Ready (List.rev tags)
    )
    ht;
  switch vm old_thread

and all_finished vm =
  Array.fold_left
    (fun acc st ->
      acc && st=Finished
    )
    true
    vm.state

and exec1 fr pc count =
  if count >= thread_limit then
    yield fr pc
  else (
    let section = fr.section in
    section.run.(pc) fr pc count section.instr.(pc)
  )

and resume_i_wait fr pc tags =
  let section = fr.section in
  let instr = section.instr.(pc) in
  let vm = fr.vm in
  match instr with
    | I_wait p ->
        vm.state.(fr.thread) <- Running;
        fr.regs.(p.reg) <- wait_tags tags;
        exec1 fr (pc+1) 0
    | _ ->
        assert false

let [@inline always] exec fr pc count =
  (* TODO: use Array.unsafe_get. This requires that we check that the last
     instruction is a return or jump
   *)
  (* This repeats exec1 because OCaml does not inline recursive functions *)
  if count >= thread_limit then
    yield fr pc
  else (
    let section = fr.section in
    section.run.(pc) fr pc count section.instr.(pc)
  )

let exit_thread vm old_thread =
  if !debug then eprintf "EXIT thread %d\n%!" old_thread;
  vm.state.(old_thread) <- Finished;
  if all_finished vm then (
    if !debug then eprintf "FINISHED\n%!";
    ()
  ) else
    switch vm old_thread

let return_pop fr rvalue rglb count =
  if !debug then eprintf "  pop\n%!";
  match Stack.pop fr.vm.stack.(fr.thread) with
    | (fr_old, pc_old, ret_reg, glb_reg) ->
        fr_old.regs.(ret_reg) <- rvalue;
        fr_old.regs.(glb_reg) <- VBox(R_global, rglb);
        exec fr_old pc_old count
    | exception Stack.Empty ->
        if fr.thread = 0 then
          fr.vm.result <- Some { rvalue; rglb };
        exit_thread fr.vm fr.thread

let return fr xargs rvalue rglb count =
  match rvalue with
    | VBox(R_closure, clos) ->
        let clos' =
          { clos with
            clos_args = Seq.append clos.clos_args xargs
          } in
        if !debug then
          eprintf "  closure #args=%d\n%!" (Seq.length clos'.clos_args);
        if Seq.length clos'.clos_args = 0 then
          return_pop fr rvalue rglb count
        else
          let fr' =
            { fr with
              regs = [| |];
              effarity = 0;
              args = clos'.clos_args;
              named = [| |];
              clos = clos';
              section = section_of_closure clos'
            } in
          exec fr' clos'.clos_entry count
    | _ ->
        return_pop fr rvalue rglb count

let uninit = VBox(R_uninitialized, ())

let apply fr clos glb call_named call_args count =
  let args = Seq.append clos.clos_args call_args in
  let section = section_of_closure clos in
  let pc = clos.clos_entry in
  if pc < 0 || pc >= Array.length section.instr then
    failwith "Vm.apply";
  let regs = [| |] in
  let i = ref 0 in
  let n = Array.length call_named in
  let named =
    Array.map
      (fun name ->
        let k = !i in
        if k < n && fst(call_named.(k)) = name then (
          incr i;
          snd(call_named.(k))
        )
        else
          uninit
      )
      clos.clos_names in
  let fr_new = { fr with section; regs; named; args; glb; clos } in
  exec fr_new pc count

let i_function fr pc count instr =
  match instr with
    | I_function p ->
        if !debug then
          eprintf "I_function slot=%d #args=%d\n%!"
                  fr.clos.clos_slot (Seq.length fr.args);
        assert(p.arity_min = fr.clos.clos_arity_min &&
                 p.arity_max = fr.clos.clos_arity_max &&
                   p.arity_names = Array.length fr.clos.clos_names
              );
        if Array.length fr.named <> p.arity_names then
          failwith "i_function: called with wrong number of named args";
        let n_args = Seq.length fr.args in
        if n_args < p.arity_min then (
          if not fr.clos.clos_autocurry then
            failwith "i_function: called with too few args";
          assert(p.arity_names = 0);
          let rclos =
            { fr.clos with
              clos_args = fr.args
            } in
          let rvalue = VBox(R_closure, rclos) in
          return_pop fr rvalue fr.glb count
        ) else (
          if n_args > p.arity_max && not fr.clos.clos_autocurry then
            failwith "i_function: called with too many args";
          fr.regs <- Array.make p.framesize (valuebox R_uninitialized ());
          fr.regs.(0) <- VBox(R_global, fr.glb);
          Array.iteri
            (fun i arg ->
              fr.regs.(i+1) <- arg
            )
            fr.named;
          let anon_offs = Array.length fr.named + 1 in
          Seq.iteri
            (fun i arg ->
              if i < p.arity_max then
                fr.regs.(i+anon_offs) <- arg
            )
            fr.args;
          fr.effarity <- min p.arity_max (Seq.length fr.args);
          exec fr (pc+1) count
        )
    | _ -> assert false

let i_return fr pc count instr =
  match instr with
    | I_return p ->
        if !debug then eprintf "I_return from slot=%d\n%!" fr.clos.clos_slot;
        let rvalue = fr.regs.(p.reg) in
        let rglb = global_of_vbox fr.regs.(p.glbreg) in
        let xargs = Seq.skip fr.effarity fr.args in
        if !debug then (
          eprintf "  #excessargs=%d\n%!" (Seq.length xargs);
          printf "  ret val: %s\n%!" (debug_string_of_vbox fr.vm.config rvalue);
        );
        return fr xargs rvalue rglb count
    | _ -> assert false


let i_apply fr pc count instr =
  match instr with
    | I_apply p ->
        if !debug then eprintf "I_apply from slot=%d\n%!" fr.clos.clos_slot;
        Stack.push (fr,pc+1,p.reg,p.glbreg) fr.vm.stack.(fr.thread);
        let clos = closure_of_vbox fr.vm.config fr.regs.(p.closreg) in
        if !debug then eprintf "  to slot=%d\n%!" clos.clos_slot;
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        let call_args =
          Seq.init (Array.length p.args) (fun i -> fr.regs.(p.args.(i))) in
        let call_named =
          Array.map (fun (n,i) -> (n,fr.regs.(i))) p.named in
        if !debug then (
          eprintf "  #args=%d\n%!" (Seq.length call_args);
          Array.iter
            (fun (n,i) ->
              eprintf "  name %s=%s\n%!"
                      n (debug_string_of_vbox fr.vm.config fr.regs.(i))
            )
            p.named;
          Array.iteri
            (fun k i ->
              eprintf "  anon %d=%s\n%!"
                      k (debug_string_of_vbox fr.vm.config fr.regs.(i))
            )
            p.args;
        );
        apply fr clos glb call_named call_args count
    | _ -> assert false

let i_applyjump fr pc count instr =
  match instr with
    | I_applyjump p ->
        if !debug then eprintf "I_applyjump\n%!";
        let clos = closure_of_vbox fr.vm.config fr.regs.(p.closreg) in
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        let xargs = Seq.skip fr.effarity fr.args in
        let call_args =
          Seq.init (Array.length p.args) (fun i -> fr.regs.(p.args.(i))) in
        let eff_args = Seq.append call_args xargs in
        let call_named =
          Array.map (fun (n,i) -> (n,fr.regs.(i))) p.named in
        apply fr clos glb call_named eff_args count
    | _ -> assert false

let unused_thread vm =
  let exception Found of int in
  try
    let state = vm.state in
    for i = 0 to Array.length state - 1 do
      if state.(i) = Finished then raise (Found i)
    done;
    raise Not_found
  with Found i -> i

let new_thread vm old_fr glb =
  let i =
    try
      unused_thread vm
    with
      | Not_found ->
          let n_old = Array.length vm.state in
          let n_new = 2*n_old in
          let current =
            Array.init
              n_new
              (fun i -> if i < n_old then vm.current.(i) else
                          let dfr = dummy_frame vm old_fr i in
                          (dfr,(-1))) in
          let stack =
            Array.init
              n_new
              (fun i -> if i < n_old then vm.stack.(i) else Stack.create()) in
          let state =
            Array.init
              n_new
              (fun i -> if i < n_old then vm.state.(i) else Finished) in
          vm.current <- current;
          vm.stack <- stack;
          vm.state <- state;
          n_old in
  vm.state.(i) <- Running;
  vm.stack.(i) <- Stack.create();
  let fr = dummy_frame vm old_fr i in
  { fr with glb }

let i_thread fr pc count instr =
  match instr with
    | I_thread p ->
        if !debug then eprintf "I_thread from slot=%d\n%!" fr.clos.clos_slot;
        let clos = closure_of_vbox fr.vm.config fr.regs.(p.closreg) in
        if !debug then eprintf "  to slot=%d\n%!" clos.clos_slot;
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        let call_args =
          Seq.init (Array.length p.args) (fun i -> fr.regs.(p.args.(i))) in
        let call_named =
          Array.map (fun (n,i) -> (n,fr.regs.(i))) p.named in
        if !debug then (
          eprintf "  #args=%d\n%!" (Seq.length call_args);
          Array.iter
            (fun (n,i) ->
              eprintf "  name %s=%s\n%!"
                      n (debug_string_of_vbox fr.vm.config fr.regs.(i))
            )
            p.named;
          Array.iteri
            (fun k i ->
              eprintf "  anon %d=%s\n%!"
                      k (debug_string_of_vbox fr.vm.config fr.regs.(i))
            )
            p.args;
        );
        let fr_new = new_thread fr.vm fr glb in
        let o = fr.thread in
        fr.vm.current.(o) <- (fr, pc+1);
        fr.regs.(p.reg) <- valuebox R_int fr_new.thread;
        if !debug then eprintf "STARTING thread %d\n%!" fr_new.thread;
        apply fr_new clos glb call_named call_args 0;
    | _ -> assert false

let i_lazy fr pc count instr =
  match instr with
    | I_lazy p ->
        if !debug then eprintf "I_lazy\n%!";
        let v1 = fr.regs.(p.input) in
        let clos = closure_of_vbox fr.vm.config v1 in
        let lz = Lz { value=VBox(R_uninitialized, ());
                      compute=clos
                    } in
        let v2 = VBox(R_lazy, lz) in
        fr.regs.(p.reg) <- v2;
        exec fr (pc+1) (count+1)
    | _ ->
        assert false

let i_force1 fr pc count instr =
  match instr with
    | I_force1 p ->
        assert(p.reg <> p.tmpreg);
        if !debug then eprintf "I_force1\n%!";
        let v = fr.regs.(p.reg) in
        ( match v with
            | VBox(R_lazy,Lz lz) ->
                ( match lz.value with
                    | VBox(R_uninitialized, _) ->
                        Stack.push
                          (fr,pc+1,p.tmpreg,p.glbreg) fr.vm.stack.(fr.thread);
                        let clos = lz.compute in
                        let glb = global_of_vbox fr.regs.(p.glbreg) in
                        let call_args =
                          Seq.make 1 (VBox(R_seq, Seq.empty)) in
                        if !debug then eprintf "  running lazy body\n%!";
                        apply fr clos glb [| |] call_args count
                        (* this continues at pc+1 where we expect I_force2 *)
                    | _ ->
                        fr.regs.(p.reg) <- lz.value;
                        exec fr (pc+2) (count+1)
                )
            | _ ->
                exec fr (pc+2) (count+1)
        )
    | _ -> assert false

let i_force2 fr pc count instr =
  match instr with
    | I_force2 p ->
        assert(p.reg <> p.tmpreg);
        if !debug then eprintf "I_force2\n%!";
        let v = fr.regs.(p.reg) in
        ( match v with
            | VBox(R_lazy,Lz lz) ->
                ( match lz.value with
                    | VBox(R_uninitialized, _) ->
                        lz.value <- fr.regs.(p.tmpreg)
                    | _ ->
                        ()
                )
            | _ ->
                ()
        );
        exec fr (pc+1) (count+1)
    | _ -> assert false


let i_closure fr pc count instr =
  match instr with
    | I_closure p ->
        if !debug then eprintf "I_closure\n%!";
        let clos = closure_of_vbox fr.vm.config fr.regs.(p.closreg) in
        let clos' =
          { clos with
            clos_env = env_of_vbox fr.regs.(p.env);
          } in
        fr.regs.(p.reg) <- VBox(R_closure,clos');
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_primitive fr pc count instr =
  match instr with
    | I_primitive p ->
        if !debug then eprintf "I_primitive\n%!";
        let Config cfg = fr.vm.config in
        if p.prim < 0 || p.prim >= Array.length cfg.primitives then
          failwith "Vm.i_primitive";
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        let call_args = Array.map (fun i -> fr.regs.(p.args.(i))) p.args in
        let res = (snd cfg.primitives.(p.prim)) glb call_args in
        fr.regs.(p.reg) <- res;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_move fr pc count instr =
  match instr with
    | I_move p ->
        if !debug then eprintf "I_move\n%!";
        fr.regs.(p.reg) <- fr.regs.(p.src_reg);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_load fr pc count instr =
  match instr with
    | I_load p ->
        if !debug then eprintf "I_load\n%!";
        fr.regs.(p.reg) <- p.value;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_env fr pc count instr =
  match instr with
    | I_env p ->
        if !debug then (
          eprintf "I_env\n%!";
          Array.iteri
            (fun k i ->
              eprintf "  env_%d := reg_%d = %s\n%!"
                      k i (debug_string_of_vbox fr.vm.config fr.regs.(i))
            )
            p.inputs;
        );
        let env = Array.map (fun i -> fr.regs.(i)) p.inputs in
        fr.regs.(p.reg) <- VBox(R_env, env);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_getenv fr pc count instr =
  match instr with
    | I_getenv p ->
        if !debug then eprintf "I_getenv\n%!";
        let env = fr.clos.clos_env in
        if p.envidx < 0 || p.envidx >= Array.length env then
          failwith "Vm.i_getenv";
        let v = env.(p.envidx) in
        if !debug then
          eprintf "  reg_%d := env_%d = %s\n%!"
                  p.reg p.envidx (debug_string_of_vbox fr.vm.config v);
        fr.regs.(p.reg) <- v;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_envind fr pc count instr =
  match instr with
    | I_envind p ->
        if !debug then eprintf "I_envind\n%!";
        fr.regs.(p.reg) <- VBox(R_regind, (fr.id, p.indreg));
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_envmove fr pc count instr =
  match instr with
    | I_envmove p ->
        if !debug then eprintf "I_envmove\n%!";
        let env = fr.clos.clos_env in
        if p.envidx < 0 || p.envidx >= Array.length env then
          failwith "Vm.i_envmove";
        ( match env.(p.envidx) with
            | VBox(R_regind, (frame_id, frame_reg)) ->
                let exception Found of frame in
                ( try
                    Stack.iter
                      (fun (sfr,_,_,_) ->
                        if sfr.id = frame_id then raise (Found sfr)
                      )
                      fr.vm.stack.(fr.thread);
                    failwith "i_envmove: cannot move value to the target frame which is not a caller"
                  with Found sfr ->
                    if !debug then (
                      eprintf "  frame_id=%d frame_reg=%d input=%d\n"
                             frame_id frame_reg p.input;
                      eprintf "  old value: %s\n"
                              (debug_string_of_vbox
                                 fr.vm.config sfr.regs.(frame_reg));
                      eprintf "  new value: %s\n%!"
                              (debug_string_of_vbox
                                 fr.vm.config fr.regs.(p.input));
                    );
                    sfr.regs.(frame_reg) <- fr.regs.(p.input)
                );
                exec fr (pc+1) (count+1)
            | _ ->
                failwith "i_envmove"
        )
    | _ -> assert false

let i_locglobalvar fr pc count instr =
  match instr with
    | I_locglobalvar p ->
        if !debug then eprintf "I_locglobalvar\n%!";
        let name = string_of_vbox fr.vm.config fr.regs.(p.namereg) in
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        let slot =
          try Lookup_table.find_slot name glb.shared.alloc.alloc_variables
          with Not_found ->
             Global.add_variable_slot name glb in
        fr.regs.(p.reg) <- VBox(R_int, slot);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_locsharedfun fr pc count instr =
  match instr with
    | I_locsharedfun p ->
        if !debug then eprintf "I_locsharedfun\n%!";
        let name = string_of_vbox fr.vm.config fr.regs.(p.namereg) in
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        let slot =
          try Lookup_table.find_slot name glb.shared.alloc.alloc_functions
          with Not_found ->
             Shared.add_function_slot name glb.shared in
        fr.regs.(p.reg) <- VBox(R_int, slot);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_testvar fr pc count instr =
  match instr with
    | I_testvar p ->
        if !debug then eprintf "I_testvar\n%!";
        let name = string_of_vbox fr.vm.config fr.regs.(p.namereg) in
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        let ok =
          try ignore(Lookup_table.find_slot
                       name glb.shared.alloc.alloc_variables); true
          with Not_found -> false in
        fr.regs.(p.reg) <- VBox(R_bool, ok);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_testfun fr pc count instr =
  match instr with
    | I_testfun p ->
        if !debug then eprintf "I_testfun\n%!";
        let name = string_of_vbox fr.vm.config fr.regs.(p.namereg) in
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        let ok =
          try ignore(Lookup_table.find_slot
                       name glb.shared.alloc.alloc_functions); true
          with Not_found -> false in
        fr.regs.(p.reg) <- VBox(R_bool, ok);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_getglobalvar fr pc count instr =
  match instr with
    | I_getglobalvar p ->
        if !debug then eprintf "I_getglobalvar\n%!";
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        fr.regs.(p.reg) <- Lookup_table.get p.slot glb.global_variables;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_getglobalvari fr pc count instr =
  match instr with
    | I_getglobalvari p ->
        if !debug then eprintf "I_getglobalvari\n%!";
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        let slot = int_of_vbox fr.vm.config fr.regs.(p.slotreg) in
        fr.regs.(p.reg) <- Lookup_table.get slot glb.global_variables;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_getsharedfun fr pc count instr =
  match instr with
    | I_getsharedfun p ->
        if !debug then eprintf "I_getsharedfun\n%!";
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        ( match Lookup_table.get p.slot glb.shared.shared_functions with
            | None ->
                failwith ("No such shared function: slot=" ^
                            string_of_int p.slot)
            | Some clos ->
                fr.regs.(p.reg) <- VBox(R_closure, clos);
        );
        exec fr (pc+1) (count+1)
    | _ -> assert false
                  
let i_getsharedfuni fr pc count instr =
  match instr with
    | I_getsharedfuni p ->
        if !debug then eprintf "I_getsharedfuni\n%!";
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        let slot = int_of_vbox fr.vm.config fr.regs.(p.slotreg) in
        ( match Lookup_table.get slot glb.shared.shared_functions with
            | None ->
                failwith ("No such shared function: slot=" ^
                            string_of_int slot)
            | Some clos ->
                fr.regs.(p.reg) <- VBox(R_closure, clos);
        );
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_addglobalvar fr pc count instr =
  match instr with
    | I_addglobalvar p ->
        if !debug then eprintf "I_addglobalvar\n%!";
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        let glb' = Global.add_variable p.slot fr.regs.(p.reg) glb in
        fr.regs.(p.glbreg) <- VBox(R_global, glb');
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_addglobalvari fr pc count instr =
  match instr with
    | I_addglobalvari p ->
        if !debug then eprintf "I_addglobalvari\n%!";
        let slot = int_of_vbox fr.vm.config fr.regs.(p.slotreg) in
        let glb = global_of_vbox fr.regs.(p.glbreg) in
        let glb' = Global.add_variable slot fr.regs.(p.reg) glb in
        fr.regs.(p.glbreg) <- VBox(R_global, glb');
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_seq fr pc count instr =
  match instr with
    | I_seq p ->
        if !debug then eprintf "I_seq\n%!";
        let elements = Array.map (fun i -> fr.regs.(i)) p.inputs in
        let str_elements = Array.map (stringval_of_vbox fr.vm.config) elements in
        let v = valuebox R_seq (Seq.of_array str_elements) in
        fr.regs.(p.reg) <- v;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_array fr pc count instr =
  match instr with
    | I_array p ->
        if !debug then eprintf "I_array\n%!";
        let elements = Array.map (fun i -> fr.regs.(i)) p.inputs in
        let v = valuebox R_array (Seq.of_array elements) in
        fr.regs.(p.reg) <- v;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_array_seq fr pc count instr =
  match instr with
    | I_array_seq p ->
        if !debug then eprintf "I_array_seq\n%!";
        let seq = fr.regs.(p.input) in
        let elements = seq_of_vbox fr.vm.config seq in
        let elements = Seq.map vbox_of_stringval elements in
        let v = valuebox R_array elements in
        fr.regs.(p.reg) <- v;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_concat fr pc count instr =
  match instr with
    | I_concat p ->
        if !debug then eprintf "I_string\n%!";
        let elements = Array.map (fun i -> fr.regs.(i)) p.inputs in
        let v = concat_vbox fr.vm.config elements in
        fr.regs.(p.reg) <- VBox(R_seq, v);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_quote fr pc count instr =
  match instr with
    | I_quote p ->
        if !debug then eprintf "I_quote\n%!";
        let v = fr.regs.(p.input) in
        fr.regs.(p.reg) <- VBox(R_quote p.qchar, (stringval_of_vbox fr.vm.config v));
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_object fr pc count instr =
  match instr with
    | I_object p ->
        if !debug then eprintf "I_object\n%!";
        let v = VBox(R_object, (Class p.cls, ())) in
        fr.regs.(p.reg) <- v;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_extend fr pc count instr =
  match instr with
    | I_extend p ->
        if !debug then eprintf "I_extend\n%!";
        let base = fr.regs.(p.in1) in
        let cls =
          match p.cls with
            | None -> None
            | Some c -> Some(Class c) in
        let v = extend_vbox fr.vm.config base cls [p.name, fr.regs.(p.in2)] in
        fr.regs.(p.reg) <- v;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_update fr pc count instr =
  match instr with
    | I_update p ->
        if !debug then eprintf "I_update\n%!";
        let base = fr.regs.(p.in1) in
        let v = update_vbox fr.vm.config base [p.name, fr.regs.(p.in2)] in
        fr.regs.(p.reg) <- v;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_length fr pc count instr =
  match instr with
    | I_length p ->
        if !debug then eprintf "I_length\n%!";
        let v = fr.regs.(p.input) in
        let n = length_of_vbox fr.vm.config v in
        fr.regs.(p.reg) <- VBox(R_int, n);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_nth fr pc count instr =
  match instr with
    | I_nth p ->
        if !debug then eprintf "I_nth\n%!";
        let v = fr.regs.(p.in1) in
        let n = int_of_vbox fr.vm.config fr.regs.(p.in2) in
        fr.regs.(p.reg) <- nth_of_vbox fr.vm.config n v;   (* TODO: handle Not_found *)
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_add fr pc count instr =
  match instr with
    | I_add p ->
        if !debug then eprintf "I_add\n%!";
        let n1 = int_of_vbox fr.vm.config fr.regs.(p.in1) in
        let n2 = int_of_vbox fr.vm.config fr.regs.(p.in2) in
        fr.regs.(p.reg) <- VBox(R_int, n1+n2);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_sub fr pc count instr =
  match instr with
    | I_sub p ->
        if !debug then eprintf "I_sub\n%!";
        let n1 = int_of_vbox fr.vm.config fr.regs.(p.in1) in
        let n2 = int_of_vbox fr.vm.config fr.regs.(p.in2) in
        fr.regs.(p.reg) <- VBox(R_int, n1-n2);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_mul fr pc count instr =
  match instr with
    | I_mul p ->
        if !debug then eprintf "I_mul\n%!";
        let n1 = int_of_vbox fr.vm.config fr.regs.(p.in1) in
        let n2 = int_of_vbox fr.vm.config fr.regs.(p.in2) in
        fr.regs.(p.reg) <- VBox(R_int, n1*n2);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_div fr pc count instr =
  match instr with
    | I_div p ->
        if !debug then eprintf "I_div\n%!";
        let n1 = int_of_vbox fr.vm.config fr.regs.(p.in1) in
        let n2 = int_of_vbox fr.vm.config fr.regs.(p.in2) in
        fr.regs.(p.reg) <- VBox(R_int, n1/n2);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_mod fr pc count instr =
  match instr with
    | I_mod p ->
        if !debug then eprintf "I_mod\n%!";
        let n1 = int_of_vbox fr.vm.config fr.regs.(p.in1) in
        let n2 = int_of_vbox fr.vm.config fr.regs.(p.in2) in
        fr.regs.(p.reg) <- VBox(R_int, n1 mod n2);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_eq fr pc count instr =
  match instr with
    | I_eq p ->
        if !debug then eprintf "I_eq\n%!";
        let n1 = int_of_vbox fr.vm.config fr.regs.(p.in1) in
        let n2 = int_of_vbox fr.vm.config fr.regs.(p.in2) in
        fr.regs.(p.reg) <- vbox_of_bool(n1 = n2);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_ne fr pc count instr =
  match instr with
    | I_ne p ->
        if !debug then eprintf "I_ne\n%!";
        let n1 = int_of_vbox fr.vm.config fr.regs.(p.in1) in
        let n2 = int_of_vbox fr.vm.config fr.regs.(p.in2) in
        fr.regs.(p.reg) <- vbox_of_bool(n1 <> n2);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_lt fr pc count instr =
  match instr with
    | I_lt p ->
        if !debug then eprintf "I_lt\n%!";
        let n1 = int_of_vbox fr.vm.config fr.regs.(p.in1) in
        let n2 = int_of_vbox fr.vm.config fr.regs.(p.in2) in
        fr.regs.(p.reg) <- vbox_of_bool(n1 < n2);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_le fr pc count instr =
  match instr with
    | I_le p ->
        if !debug then eprintf "I_le\n%!";
        let n1 = int_of_vbox fr.vm.config fr.regs.(p.in1) in
        let n2 = int_of_vbox fr.vm.config fr.regs.(p.in2) in
        fr.regs.(p.reg) <- vbox_of_bool(n1 <= n2);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_isuninit fr pc count instr =
  match instr with
    | I_isuninit p ->
        if !debug then eprintf "I_isuninit\n%!";
        let v1 = fr.regs.(p.in1) in
        fr.regs.(p.reg) <- vbox_of_bool(v1 = VBox(R_uninitialized,()));
        exec fr (pc+1) (count+1)
    | _ -> assert false
                  
let rel =
  function
  | Rel k -> k
  | Lab _ -> failwith "Vm: found unrelocated label"

let i_if fr pc count instr =
  match instr with
    | I_if p ->
        if !debug then eprintf "I_if\n%!";
        ( match fr.regs.(p.in1) with
            | VBox(R_bool, false) ->
                exec fr (pc + rel p.onfalse) (count+1)
            | VBox(R_bool, true) ->
                exec fr (pc + rel p.ontrue) (count+1)
            | _ ->
                raise Unexpected_type_of_value
        )
    | _ -> assert false

let i_jump fr pc count instr =
  match instr with
    | I_jump p ->
        if !debug then eprintf "I_jump\n%!";
        exec fr (pc + rel p.jump) (count+1)
    | _ -> assert false

let i_coerce fr pc count instr =
  match instr with
    | I_coerce p ->
        if !debug then eprintf "I_coerce\n%!";
        fr.regs.(p.reg) <- coerce fr.vm.config p.utype fr.regs.(p.input);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_settrap fr pc count instr =
  match instr with
    | I_settrap p ->
        let trap_pc = pc + rel p.handler in
        if !debug then eprintf "I_settrap pc=%d trap=%d\n%!" pc trap_pc;
        fr.trap <- Some { trap_reg = p.reg; trap_pc };
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_deltrap fr pc count instr =
  if !debug then eprintf "I_deltrap\n%!";
  fr.trap <- None;
  exec fr (pc+1) (count+1)

let i_raise fr pc count instr =
  match instr with
    | I_raise p ->
        let stack = fr.vm.stack.(fr.thread) in
        if !debug then eprintf "I_raise stacklen=%d\n%!" (Stack.length stack);
        while
          not(Stack.is_empty stack) &&
            let sfr, _, _, _ = Stack.top stack in
            sfr.trap = None
        do
          if !debug then eprintf "  pop\n%!";
          ignore(Stack.pop stack)
        done;
        if Stack.is_empty stack then
          failwith "uncaught VM exception"  (* TODO *)
        else (
          if !debug then eprintf "  pop\n%!";
          let sfr, _, _, _ = Stack.pop stack in
          match sfr.trap with
            | None -> assert false
            | Some t ->
                if !debug then eprintf "  cont_pc=%d\n%!" t.trap_pc;
                sfr.regs.(t.trap_reg) <- fr.regs.(p.reg);
                sfr.trap <- None;
                exec sfr t.trap_pc (count+1)
        )
    | _ -> assert false

let i_objget fr pc count instr =
  match instr with
    | I_objget p ->
        if !debug then eprintf "I_objget\n%!";
        let cls =
          match p.cls with
            | None -> None
            | Some c -> Some(Class c) in
        let obj = fr.regs.(p.in1) in
        let v = field_of_vbox fr.vm.config obj cls p.name in
        fr.regs.(p.reg) <- v;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_objcls fr pc count instr =
  match instr with
    | I_objcls p ->
        if !debug then eprintf "I_objcls\n%!";
        let Class c = class_of_vbox fr.vm.config fr.regs.(p.in1) in
        fr.regs.(p.reg) <- VBox(R_string, c);
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_ref fr pc count instr =
  match instr with
    | I_ref p ->
        if !debug then eprintf "I_ref\n%!";
        let v = fr.regs.(p.in1) in
        let v = (* avoid refs of refs *)
          match v with
            | VBox(R_ref, Ref vr) -> vr.value
            | _ -> v in
        fr.regs.(p.reg) <- VBox(R_ref, Ref { value = v });
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_mutate fr pc count instr =
  match instr with
    | I_mutate p ->
        if !debug then eprintf "I_mutate\n%!";
        let Ref r = reference_of_vbox fr.regs.(p.reg) in
        let v = fr.regs.(p.in1) in
        let v = (* avoid refs of refs *)
          match v with
            | VBox(R_ref, Ref vr) -> vr.value
            | _ -> v in
        r.value <- v;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_test fr pc count instr =
  match instr with
    | I_test p ->
        if !debug then eprintf "I_test\n%!";
        let open Omake_os in
        let h = handle_of_vbox fr.vm.config fr.regs.(p.handle) in
        let aset = wait_set h in
        let tags = Async_set.wait aset 0.0 in
        fr.regs.(p.reg) <- wait_tags tags;
        exec fr (pc+1) (count+1)
    | _ -> assert false

let i_wait fr pc count instr =
  match instr with
    | I_wait p ->
        if !debug then eprintf "I_wait\n%!";
        let h = handle_of_vbox fr.vm.config fr.regs.(p.handle) in
        let aset = wait_set h in
        let vm = fr.vm in
        let thread = fr.thread in
        vm.state.(thread) <- Waiting aset;
        yield fr pc
    | _ -> assert false

let i_endsnippet fr pc count instr =
  assert(fr.thread = 0);
  exit_thread fr.vm 0

let relocate instrs =
  let labels = Hashtbl.create 7 in
  let find_label l =
    try Hashtbl.find labels l
    with Not_found ->
      failwith ("relocate: label not found: " ^ l) in
  let rel_jump pc =
    function
    | Rel k -> Rel k
    | Lab l -> Rel(find_label l - pc) in
  let pc = ref 0 in
  Array.iter
    (fun instr ->
      match instr with
        | I_label name ->
            if Hashtbl.mem labels name then
              failwith ("relocate: label already defined: " ^ name);
            Hashtbl.add labels name !pc
        | _ ->
            incr pc
    )
    instrs;
  let opc = !pc in
  let out = Array.make !pc (I_label "") in
  pc := 0;
  Array.iter
    (fun instr ->
      match instr with
        | I_jump p ->
            let instr' = I_jump { jump = rel_jump !pc p.jump } in
            out.(!pc) <- instr';
            incr pc
        | I_if p ->
            let instr' = I_if { p with
                                ontrue = rel_jump !pc p.ontrue;
                                onfalse = rel_jump !pc p.onfalse
                              } in
            out.(!pc) <- instr';
            incr pc
        | I_settrap p ->
            let instr' = I_settrap { p with handler=rel_jump !pc p.handler } in
            out.(!pc) <- instr';
            incr pc
        | I_label _ ->
            ()
        | _ ->
            out.(!pc) <- instr;
            incr pc
    )
    instrs;
  assert(!pc = opc);
  out

let code alloc instr =
  let run =
    Array.map
      (function
       | I_function _ -> i_function
       | I_return _ -> i_return
       | I_apply _ -> i_apply
       | I_applyjump _ -> i_applyjump
       | I_thread _ -> i_thread
       | I_closure _ -> i_closure
       | I_endsnippet -> i_endsnippet
       | I_primitive _ -> i_primitive
       | I_move _ -> i_move
       | I_load _ -> i_load
       | I_env _ -> i_env
       | I_getenv _ -> i_getenv
       | I_envind _ -> i_envind
       | I_envmove _ -> i_envmove
       | I_locglobalvar _ -> i_locglobalvar
       | I_locsharedfun _ -> i_locsharedfun
       | I_testvar _ -> i_testvar
       | I_testfun _ -> i_testfun
       | I_getglobalvar _ -> i_getglobalvar
       | I_getglobalvari _ -> i_getglobalvari
       | I_getsharedfun _ -> i_getsharedfun
       | I_getsharedfuni _ -> i_getsharedfuni
       | I_addglobalvar _ -> i_addglobalvar
       | I_addglobalvari _ -> i_addglobalvari
       | I_coerce _ -> i_coerce
       | I_lazy _ -> i_lazy
       | I_force1 _ -> i_force1
       | I_force2 _ -> i_force2
       | I_seq _ -> i_seq
       | I_array _ -> i_array
       | I_array_seq _ -> i_array_seq
       | I_concat _ -> i_concat
       | I_quote _ -> i_quote
       | I_object _ -> i_object
       | I_update _ -> i_update
       | I_extend _ -> i_extend
       | I_length _ -> i_length
       | I_nth _ -> i_nth
       | I_add _ -> i_add
       | I_sub _ -> i_sub
       | I_mul _ -> i_mul
       | I_div _ -> i_div
       | I_mod _ -> i_mod
       | I_eq _ -> i_eq
       | I_ne _ -> i_ne
       | I_lt _ -> i_lt
       | I_le _ -> i_le
       | I_isuninit _ -> i_isuninit
       | I_if _ -> i_if
       | I_jump _ -> i_jump
       | I_settrap _ -> i_settrap
       | I_deltrap -> i_deltrap
       | I_raise _ -> i_raise
       | I_objget _ -> i_objget
       | I_objcls _ -> i_objcls
       | I_ref _ -> i_ref
       | I_mutate _ -> i_mutate
       | I_test _ -> i_test
       | I_wait _ -> i_wait
       | I_label _ -> assert false
      )
      instr in
  Section { instr; run; alloc }

let instructions code =
  match code with
    | Section { instr; _ } -> instr
    | _ -> assert false


let run config glb clos call_args =
  let id = get_frame_id() in
  let vm =
    { current = [| |];
      stack = [| Stack.create() |];
      state = [| Running |];
      result = None;
      config
    } in
  let section =
    match clos.clos_code with
      | Section s -> s
      | _ -> failwith "Unexpected type of code" in
  let pc = clos.clos_entry in
  if pc < 0 || pc >= Array.length section.instr then
    failwith "Vm.run";
  let args = Seq.append clos.clos_args call_args in
  let regs = [| |] in
  let named = [| |] in
  let effarity = 0 in
  let fr =
    { id; vm; section; regs; args; glb; clos; named; effarity;
      thread=0; trap=None
    } in
  vm.current <- [| (fr, pc) |];
  section.run.(pc) fr pc 0 section.instr.(pc);
  match vm.result with
    | None ->
        failwith "Hit end of code section without return statement"
    | Some r ->
        (r.rglb, r.rvalue)


let run_snippet config glb regs code =
  let id = get_frame_id() in
  let clos =
    { clos_slot = (-1);
      clos_entry = 0;
      clos_env = [| |];
      clos_args = Seq.empty;
      clos_arity_min = 0;
      clos_arity_max = 0;
      clos_autocurry = false;
      clos_names = [| |];
      clos_code = code
    } in
  let vm =
    { current = [| |];
      stack = [| Stack.create() |];
      state = [| Running |];
      result = None;
      config
    } in
  let section = section_of_closure clos in
  let pc = clos.clos_entry in
  let args = Seq.empty in
  let named = [| |] in
  let effarity = 0 in
  let fr =
    { id; vm; section; regs; args; glb; clos; named; effarity;
      thread=0; trap=None
    } in
  vm.current <- [| (fr, pc) |];
  section.run.(pc) fr pc 0 section.instr.(pc);
  if vm.result <> None then
    failwith "Hit return in a snippet (outside function)";
  ()
