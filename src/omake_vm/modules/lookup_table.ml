module SMap = Map.Make(String)
module I = struct type t = int let compare : int->int->int = compare end
module IMap = Map.Make(I)

type allocator =
  { mutable num_slots : int;
    mutable slots : int SMap.t;
  }

type 'a t =
  { alloc : allocator;
    default : 'a;
    values0 : 'a array;
    values1 : 'a IMap.t
  }

type name = string
type slot = int

let create_alloc() =
  { num_slots = 0;
    slots = SMap.empty;
  }


let create alloc default =
  { alloc;
    default;
    values0 = [| |];
    values1 = IMap.empty
  }

let length alloc = alloc.num_slots

let add_slot name alloc =
  try
    SMap.find name alloc.slots
  with
    | Not_found ->
        let k = alloc.num_slots in
        alloc.num_slots <- k+1;
        alloc.slots <- SMap.add name k alloc.slots;
        k

let find_slot name alloc =
  SMap.find name alloc.slots

let add slot value tab =
  if slot < 0 || slot >= tab.alloc.num_slots then
    invalid_arg "Lookup_table.add";
  { tab with
    values1 = IMap.add slot value tab.values1
  }

let get slot tab =
  if slot < 0 || slot >= tab.alloc.num_slots then
    invalid_arg "Lookup_table.get";
  try
    IMap.find slot tab.values1
  with
    | Not_found when slot < Array.length tab.values0 ->
        tab.values0.(slot)
    | Not_found ->
        tab.default

let allocator tab = tab.alloc

let optimize tab =
  let values0 =
    Array.init
      tab.alloc.num_slots
      (fun i -> get i tab) in
  { tab with
    values0;
    values1 = IMap.empty
  }






