open Types

let standard_config =
  let rec class_of vbox =
    match vbox with
      | VBox(R_bool, _) -> "Bool"
      | VBox(R_int, _) -> "Int"
      | VBox(R_float, _) -> "Float"
      | VBox(R_string, _) -> "String"
      | VBox(R_seq, _) -> "Sequence"
      | VBox(R_quote _, _) -> "String"
      | VBox(R_array, _) -> "Array"
      | VBox(R_dynamic, vbox) -> class_of (vbox:valuebox)
      | VBox(R_closure, _) -> "Fun"
      | VBox(R_lazy, lz) -> class_of (forced_value lz)
      | VBox(R_ref, Ref r) -> class_of r.value
      | VBox(R_object, (Class c, _)) -> c
      | VBox(R_extend, (Class c, _)) -> c
      | VBox(R_update, Objext ox) -> class_of ox.base
      | _ -> "Unknown" in
  let field_of vbox cls_opt name =
    (* by default no field defs *)
    raise No_such_field in
  let coerce_to utype vbox =
    (* by default no extra coercions *)
    raise Unexpected_type_of_value in
  let debug_string custom = "<custom>" in
  let primitives = [| |] in
  Config { class_of; field_of; coerce_to; debug_string; primitives }


