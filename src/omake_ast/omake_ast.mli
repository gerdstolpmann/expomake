(** AST and parser.

    Note that this part has been copied over from the original OMake
    project, and hence the license is bound to be GPL.
 *)

module Types : sig
  type var = string

  (**
     Shell flags indicating whether a body needs to be read.
     Many parsing functions return whether a body follows, and which:
     - [NoBody]: no body follows
     - [OptBody]: a body may follow (subexpression)
     - [ColonBody]: a body will follow. In the interactive mode this
       is indicated by a colon at the end of the line, hence the name
     - [ArrayBody]: a body interpreted as array will follow. Used
       for [V[] =].
 *)
  type body_flag =
    | NoBody
    | OptBody
    | ColonBody
    | ArrayBody

  (**
     Function applications can be tagged as lazy or eager.
     Also used for variables etc. [LazyApply] is an _explicitly_ lazy
     application (e.g. [$`x]) and [EagerApply] an _explicitly_ eager application
     (e.g. [$,x]). Both overriding any evaluation strategy that was recorded
     with the variable.
     [CommandApply]: seeing this where a command is run (notation [Cmd(...)]).
    I guess it means that the result is discarded
   *)
  type apply_strategy =
    | LazyApply
    | EagerApply
    | NormalApply
    | CommandApply

  (** When a variable is defined, these are additional flags.
   *)
  type define_kind =
    | DefineString
    | DefineArray

  (** Whether the assignment is normal or an append *)
  type define_flag =
    | DefineNormal
    | DefineAppend

  type location = Lexing.position * Lexing.position

  type qprefix =
    | Private
    | Global
    | Shared
    | This
    | Primitive
    | Unqualified

  type qvar =
    { qprefix : qprefix;
      qcurry : bool;
      qname : string
    }

  module SMap : Map.S with type key = string

  (**
   Expressions.

   The [String*Exp] are all strings.  Normally, they are all interpreted
   the same way.

   - [NullExp]: sometimes used for missing expression, e.g. missing
                argument
   - [IntExp]:  an integer (only used when int parsing is obligatory)
   - [FloatExp]: a float (only used when int parsing is obligatory)
   - [StringOpExp]: string that parses as special char/string, e.g. "::"
                   or "=>"
   - [StringIdExp]: string that parses as name
   - [StringIntExp]: string that parses as integer
   - [StringFloatExp]: string that parses as float
   - [StringWhiteExp]: string that parses as whitespace
   - [StringKeywordExp]:  string that parses as keyword
   - [StringOtherExp]: other string
   - [QuoteExp]: range quoted with $""
   - [QuoteStringExp]: range in double quotes
   - [SequenceExp]: string sequence (concatenation)
   - [ArrayExp]: an argument is tagged as [ArrayExp] if it came from an
     array body (not returned by parser, later added by
     [Util.update_body_args])
   - [ApplyExp(CommandApply,...)]: call a command (syntax [Cmd(...)]).
     Args may be given as body or inline
   - [ApplyExp(_,...)]:  call a function ([$(name arg)] - resulting in a value)
   - [ApplyExp(...,var,[])];  look up a variable (like function call with
     empty args)
   - [MethodApplyExp]: call a method command (syntax [obj.Cmd(...)])
   - [SuperApplyExp]: call a super method command
   - [CommandExp]: the [var] is the symbol for the command to run.
     Used for builtins like "if", "match" etc.
   - [VarDefExp]: variable assignment (inline w/o body, [v = ...])
   - [VarDefBodyExp]: variable assignment (w/ body, [v =])
   - [ObjectDefExp]: object definition (w/body, [v. =])
   - [FunDefExp]: function definition (w/ body, [v(args) =])
   - [RuleExp]: a rule (2- or 3-place)
   - [ShellExp]: an external command to run (new process)
   - [BodyExp]: an argument is tagged as [BodyExp] if it came from a
     non-array body (not returned by parser, later added by
     [Util.update_body_args])
   - [CatchExp]: "catch" expression (unclear why this doesn't fit into
     [CommandExp])
   - [ExtendsExp]: "extends" expression
   - [ClassExp]: "class" expression
   - [KeyExp]: key lookup ([|name|])
   - [KeyDefExp]: key assignment (inline w/o body, [|name| = ...])
   - [KeyDefBodyExp]: key assignment (w/body, [|name| =])
  *)
  type exp =
    | NullExp         of location
    | IntExp          of int * location
    | FloatExp        of float * location
    | StringOpExp     of string * location
    | StringIdExp     of string * location
    | StringIntExp    of string * location
    | StringFloatExp  of string * location
    | StringWhiteExp  of string * location
    | StringOtherExp  of string * location
    | StringKeywordExp of string * location
    | QuoteExp        of exp list * location
    | QuoteStringExp  of char * exp list * location
    | SequenceExp     of exp list * location
    | ArrayExp        of exp list * location
    | ApplyExp        of apply_strategy * qvar * arg list * location
    | SuperApplyExp   of apply_strategy * var * var * arg list * location
    | MethodApplyExp  of apply_strategy * qvar * var list * arg list * location
    | CommandExp      of qvar * exp * exp list * location
    | VarDefExp       of qvar * define_kind * define_flag * exp * location
    | VarDefBodyExp   of qvar * define_kind * define_flag * exp list * location
    | ObjectDefExp    of qvar * define_flag * exp list * location
    | FunDefExp       of qvar * params * exp list * location
    | RuleExp         of bool * exp * exp * exp SMap.t * exp list * location
    | BodyExp         of exp list * location
    | ShellExp        of exp * location
    | CatchExp        of var * var * exp list * location
    | ExtendsExp      of exp * location
    | ClassExp        of string list * location
    | KeyExp          of apply_strategy * string * location
    | KeyDefExp       of string * define_kind * define_flag * exp * location
    | KeyDefBodyExp   of string * define_kind * define_flag * exp list * location

   and params = param list

   (** param = formal param of a lambda *)
   and param =
     | OptionalParam of var * exp * location
     | RequiredParam of var * location
     | NormalParam   of var * location

   (** arg = actual argument of an application *)
   and arg      =
     | KeyArg      of var * exp
     | ExpArg      of exp
     | ArrowArg    of param list * exp

   and parse_arg =
     | IdArg       of string * (string * location) option * location   (* Second string is always whitespace *)
     | NormalArg   of arg

   and args     = arg list

  type prog = exp list

  type error_arg =
    | Error_unit
    | Error_string of string
    | Error_ast of exp

  exception Syntax_error of location * string * error_arg

  val parse_qvar : string list -> qvar * string list
  val parse_qvar_from_string : string -> qvar * string list
  val words : exp -> string list
end

module Parse : sig
  type token
end

module Print : sig
  (** Collection of pretty-printers *)

  val pp_print_location : Format.formatter -> Types.location -> unit
  val pp_print_symbol : Format.formatter -> string -> unit
  val pp_print_qvar : Format.formatter -> Types.qvar -> unit
  val pp_print_method_name : Format.formatter -> (Types.qvar * string list) ->
                             unit
  val pp_print_strategy : Format.formatter -> Types.apply_strategy -> unit
  val pp_print_define_kind : Format.formatter -> Types.define_kind -> unit
  val pp_print_define_flag : Format.formatter -> Types.define_flag -> unit
  val pp_print_param : Format.formatter -> Types.param -> unit
  val pp_print_params : Format.formatter -> Types.param list -> unit
  val pp_print_arg : Format.formatter -> Types.arg -> unit
  val pp_print_args : Format.formatter -> Types.arg list -> unit
  val pp_print_exp : Format.formatter -> Types.exp -> unit
  val pp_print_exp_list : Format.formatter -> Types.exp list -> unit
  val pp_print_table_exp : Format.formatter -> Types.exp Types.SMap.t -> unit
  val pp_print_prog : Format.formatter -> Types.prog -> unit
  val pp_print_simple_arg : Format.formatter -> Types.arg -> unit
  val pp_print_simple_args : Format.formatter -> Types.arg list -> unit
  val pp_print_simple_exp : Format.formatter -> Types.exp -> unit
  val pp_print_simple_exp_list : Format.formatter -> Types.exp list -> unit
  val pp_print_token : Format.formatter -> Parse.token -> unit
end

module Util : sig

  val loc_of_exp : Types.exp -> Types.location
  val key_of_exp : Types.exp -> string
  val name_of_param : Types.param -> string
end

module Lex : sig
  exception Unknown_token of Types.location * Parse.token

  val debug_lex : bool ref

  val parse_file   : string -> Types.prog
  val parse_string : string -> Types.prog

  val parse_deps   : string ->
                     (Types.exp * Types.exp * Types.location) list

  (*
   * Shell gets its own handle.
   *)
  type session

  val current_location : session -> Types.location
  val create_shell     : unit -> session
  val parse_shell      : session -> string -> Types.exp list

end
