(* This is a modified copy of omake_ast_util.ml and covered by the GPL *) (* -*- tuareg -*- *)

(*
 * Lexer for the omake language.
 * This is a little difficult because indentation is
 * significant, and we want it to work in interactive mode
 * too.
 *
 * GS. Also includes the entry points for parsing:
 * - parse_ast:     parse a file
 * - parse_string:  parse a string
 *)

{
open Types

exception Unknown_token of location * Parse.token

let debug_lex = ref false

(*
 * Current mode:
 *    ModeNormal: normal lexing mode
 *    ModeString s: parsing a literal string, dollar sequences are still expanded,
 *       s is the quotation delimiter
 *    ModeSkipString s :parsing a literal string, dollar sequences are still expanded,
 *       s is the quotation delimiter, skip the token if it is a quote that is not s
 *    ModeQuote s: parsing a literal string, dollar sequences are still expanded,
 *       escape sequences are allowed, s is the quotation delimiter.
 *
 * GS. The main entry is lex_line (below). Depending on the current mode,
 * a different lexer function is invoked:
 *
 * ModeNormal:      calls lex_main
 * ModeString:      calls lex_string, for text in $+dquote (e.g. $"")
 * ModeSkipString:  calls lex_skip_string. This is used after newlines inside
 *                  $-dquoted-text for checking whether the matching end
 *                  quote is following. Fairly technical.
 * ModeQuote:       calls lex_quote, for text after dquote
 *)
type mode =
  ModeNormal
| ModeSkipString of string
| ModeString of string
| ModeQuote of string

(*
 * The lexing mode.
 *    ModeInitial: lexbuf is ready to be used
 *    ModeIndent i: initial indentation has been scanned
 *    ModeNormal: normal processing
 *
 * GS. LexModeInitial means we are at the beginning of the line. LexModeNormal
 * means that we've just lexed the left indentation.
 *)
type lexmode =
   LexModeInitial
 | LexModeNormal of int

(*
 * Parsing results.
 *)
type parse_item =
   ParseExp of exp list
 | ParseError
 | ParseEOF

(*
 * This is the info for each indentation level.
 *)
type info =
   { info_mode     : mode;
     info_indent   : int;
     info_parens   : int option
   }

(*
 * State of the lexer.
 *)
type session =
  { mutable current_loc : location;
    mutable current_file : string;

    (* The current input buffer *)
    mutable current_buffer  : string;
    mutable current_index   : int;
    mutable current_prompt  : string;
    mutable current_fill_ok : bool;
    mutable current_eof     : bool;
    readline                : (string -> string option);
    mutable is_interactive  : bool;

    (* The current lexbuf *)
    mutable current_lexbuf  : Lexing.lexbuf;
    mutable current_lexmode : lexmode;
    mutable current_token   : Parse.token;

    (* The current mode *)
    mutable current_mode    : mode;
    mutable current_parens  : int option;
    mutable current_indent  : int;
    mutable current_stack   : info list
  }

(*
 * Set state.
 *)
let create name readline =
  let loc = (Lexing.dummy_pos, Lexing.dummy_pos) in
  { current_loc     = loc;
    current_file    = name;
    current_buffer  = "";
    current_index   = 0;
    current_prompt  = ">";
    current_fill_ok = true;
    current_eof     = true;
    readline        = readline;
    is_interactive  = false;
    current_lexbuf  = Lexing.from_string "";
    current_lexmode = LexModeInitial;
    current_token   = Parse.TokEof loc;
    current_mode    = ModeNormal;
    current_parens  = None;
    current_indent  = 0;
    current_stack   = []
  }

let current_location state = state.current_loc

(*
 * Save the state.
 *)
let save_mode state =
   let { current_mode   = mode';
         current_parens = parens;
         current_indent = indent;
         current_stack  = stack;
         _
       } = state in
   let info =
      { info_mode   = mode';
        info_parens = parens;
        info_indent = indent
      } in
   info :: stack

(*
 * Restore the state.
 *)
let restore_mode state stack =
   match stack with
     | info :: stack ->
         state.current_mode    <- info.info_mode;
         state.current_parens  <- info.info_parens;
         state.current_indent  <- info.info_indent;
         state.current_stack   <- stack
     | [] ->
         ()

(*
 * Push the new mode.
 *)
let push_mode state mode =
   let stack = save_mode state in
   state.current_mode   <- mode;
   state.current_parens <- None;
   state.current_stack  <- stack

(*
 * Pop the mode.
 *)
let pop_mode state =
  restore_mode state state.current_stack

(*
 * We are moving from a quotation to normal mode.
 * Start collecting parentheses.
 *)
let push_dollar state mode =
   push_mode state mode;
   state.current_parens <- Some 0

(* GS. The reason for counting open parentheses (in current_parens) is that
   a line feed is interpreted differently while there is an open parenthesis.
 *)

(*
 * Push a paren.
 *)
let push_paren state =
  let { current_parens = parens ; _} = state in
  match parens with
      Some i ->
      state.current_parens <- Some (succ i)
    | None ->
        ()

(*
 * When a paren is popped, if the level becomes zero,
 * then return to the previous mode.
 *)
let pop_paren state =
   let { current_parens = parens ; _} = state in
   match parens with
     | Some i ->
         let i = pred i in
         if i = 0 then
           pop_mode state
         else
           state.current_parens <- Some i
     | None ->
         ()

(*
 * Get the location of the current lexeme.
 * We assume it is all on one line.
 *)
let lexeme_loc state lexbuf =
  let l1 = Lexing.lexeme_start_p lexbuf in
  let l2 = Lexing.lexeme_end_p lexbuf in
  let l1 = { l1 with Lexing.pos_fname = state.current_file } in
  let l2 = { l2 with Lexing.pos_fname = state.current_file } in
  let loc = (l1, l2) in
  state.current_loc <- loc;
  loc


(*
 * Raise a syntax error exception.
 *)
let parse_error state =
  let lexbuf = state.current_lexbuf in
  let loc = lexeme_loc state lexbuf in
  raise (Unknown_token (loc, state.current_token))

let syntax_error state s lexbuf =
  raise (Syntax_error (lexeme_loc state lexbuf, s, Error_unit))

(*
 * Get the string in the lexbuf.
 *)
let lexeme_string state lexbuf =
  let loc = lexeme_loc state lexbuf in
  let s = Lexing.lexeme lexbuf in
  s, loc

(*
 * Process a name.
 *)
let lexeme_name state lexbuf =
  let id, loc = lexeme_string state lexbuf in
  match id with
    | "if"
    | "elseif"
    | "else"
    | "switch"
    | "match"
    | "select"
    | "case"
    | "default"
    | "section"
    | "include"
    | "import"
    | "try"
    | "when"
    | "finally"
    | "raise"
    | "return"
    | "export"
    | "open"
    | "autoload"
    | "declare"
    | "value"
    | "with"
    | "as"
    | "while"
    | "do"
    | "set"
    | "program-syntax" ->
        Parse.TokKeyword (id, loc)
    | "catch" ->
        Parse.TokCatch (id, loc)
    | "extends" ->
        Parse.TokExtends (id, loc)
    | "class" ->
        Parse.TokClass (id, loc)
    | _ ->
        Parse.TokId (id, loc)

let lexeme_key state lexbuf =
    let id, loc = lexeme_string state lexbuf in
    Parse.TokKey (id, loc)

(*
 * Get the escaped char.
 * GS. e.g. "\X" -> "X"
 *)
let lexeme_esc state lexbuf =
  let s, loc = lexeme_string state lexbuf in
  String.make 1 s.[1], loc

(*
 * Single character variable.
 * GS. $x (not $(...)). Also $`x and $,x.
 *)
let lexeme_var state lexbuf =
  let s, loc = lexeme_string state lexbuf in
  let strategy, s =
    match s.[1] with
      | '`' -> LazyApply, String.sub s 2 1
      | ',' -> EagerApply, String.sub s 2 1
      | _ -> NormalApply, String.sub s 1 1 in
  Parse.TokVar (strategy, s, loc)

(*
 * Dollar sequence.
 *)
let lexeme_dollar_pipe state lexbuf =
  let s, loc = lexeme_string state lexbuf in
  let len = String.length s in
  let strategy, off =
    if len >= 2 then
      match s.[1] with
        | '`'  -> LazyApply, 2
        | ','  -> EagerApply, 2
        | '|'  -> NormalApply, 1
        | _    -> syntax_error state ("illegal character: " ^ s) lexbuf
    else
      NormalApply, 1 in
  let s = String.sub s off (String.length s - off) in
  strategy, s, loc

(* GS. Unclear why there are two versions of this function. lexeme_dollar
   seems to be the usual function, for all of $` $, $$
 *)

let lexeme_dollar state lexbuf =
  let s, loc = lexeme_string state lexbuf in
  let len = String.length s in
  if len >= 2 then
    match s.[1] with
      | '`'  -> Parse.TokDollar (s, LazyApply, loc)
      | ','  -> Parse.TokDollar (s, EagerApply, loc)
      | '$'  -> Parse.TokString ("$", loc)
      | _    -> syntax_error state ("illegal character: " ^ s) lexbuf
  else
    Parse.TokDollar (s, NormalApply, loc)

(*
 * Special character.
 * Keep track of paren nesting.
 *)
let lexeme_char state lexbuf =
  let s, loc = lexeme_string state lexbuf in
  match s.[0] with
    | '$' ->
        Parse.TokDollar (s, NormalApply, loc)
    | ':' ->
        Parse.TokColon (s, loc)
    | ',' ->
        Parse.TokComma (s, loc)
    | '=' ->
        Parse.TokEq (s, loc)
    | '.' ->
        Parse.TokDot (s, loc)
    | '%' ->
        Parse.TokVar (NormalApply, s, loc)
    | '(' ->
        push_paren state;
        Parse.TokLeftParen (s, loc)
    | ')' ->
        pop_paren state;
        Parse.TokRightParen (s, loc)
    | _   ->
        Parse.TokOp (s, loc)

(*
 * Special string.
 *)
let lexeme_special_string state lexbuf =
  let s, loc = lexeme_string state lexbuf in
  match s with
    | "=>" ->
        Parse.TokArrow (s, loc)
    | "::" ->
        Parse.TokDoubleColon (s, loc)
    | "+=" ->
        Parse.TokEq (s, loc)
    | "[]" ->
        Parse.TokArray (s, loc)
    | _ ->
        Parse.TokOp (s, loc)

(*
 * Count the indentation in a string of characters.
 *)
let indent_of_string s =
  let len = String.length s in
  let rec loop col i =
    if i = len then
      col
    else
      match s.[i] with
        | '\r'
        | '\n' ->
            loop 0 (succ i)
        | '\t' ->
            loop ((col + 8) land (lnot 7)) (succ i)
        | _ ->
            loop (succ col) (succ i) in
  loop 0 0


let lexeme_pos lexbuf =
  let s = Lexing.lexeme lexbuf in
  let l1 = Lexing.lexeme_start_p lexbuf in
  let l2 = Lexing.lexeme_end_p lexbuf in
  s, (l1,l2)
}

(***********************************************************************)

(*
 * White space.
 * Line is terminated by '\n' or eof,
 * but be nice to DOS.
 *)
let whitec          = [' ' '\t' '\012']
let white           = whitec +
let opt_white       = whitec *

let strict_nl       = "\r\n" | ['\n' '\r']
let white_nl        = opt_white strict_nl
let strict_eol      = strict_nl | eof

(*
 * Identifiers and keywords.
 *)
let name_prefix     = ['_' 'A'-'Z' 'a'-'z' '0'-'9' '@']
let name_suffix     = ['_' 'A'-'Z' 'a'-'z' '0'-'9' '-' '~' '@']
let name            = name_prefix name_suffix* | '[' | ']'
let key             = ['~' '?'] name_suffix+
  (* GS. Named function arguments, as in OCaml *)

(*
 * Numbers.
 *)
let binary          = "0b" ['0'-'1']*
let octal           = "0o" ['0'-'7']*
let decimal         = ['0'-'9']+
let hex             = "0x" ['0'-'9' 'a'-'f' 'A'-'F']*
let integer         = binary | octal | decimal | hex

let float_exp       = ['e' 'E'] ['-' '+']? ['0'-'9']+
let float1          = ['0'-'9']* '.' ['0'-'9'] float_exp?
let float2          = ['0'-'9']+ float_exp
let float           = float1 | float2

(*
 * Comments begin with a # symbol and continue to end-of-line.
 * Comments are relaxed w.r.t. leading whitespace.
 *)
let comment         = opt_white '#' [^ '\n']*
let comment_nl      = comment strict_nl
let comment_eol     = comment strict_eol

(*
 * Quotes.
 *)
let squote         = ['\'']+
let dquote         = ['"']+
let pipe           = ['|']+
let quote          = squote | dquote | pipe
let quote_opt      = quote?

(*
 * Special variables.
 * GS. This refers to one-character dollar refs, without parentheses.
 *)
let dollar         = '$' ['`' ',' '$']
let paren_dollar   = '$' ['`' ',']?
let special_sym    = ['@' '&' '*' '<' '^' '+' '?' 'A'-'Z' 'a'-'z' '_' '0'-'9'
                      '~' '[' ']']
let special_var    = paren_dollar special_sym

(*
 * Named colon separators.
 *)
let special_colon   = ':' name ':'

(*
 * Escape sequences.
 *)
let esc_char    = '\\' ['$' '(' ')' ':' ',' '=' '#' '\\' '\'' '"' ' ' '\t']
let esc_quote   = '\\' ['\\' '\'' '"']
let esc_line    = '\\' strict_eol

(*
 * Special sequences.
 *)
let special_char    = ['$' '(' ')' ':' ',' ';' '=' '.' '%' '+' '-' '*' '/'
                       '<' '>' '^' '&' '|']
let special_string  = "=>" | "::" | "+=" | "[]" | "<<" | ">>" | ">>>" | "&&" |
                      "||" | "..." | "[...]"

(*
 * Other stuff that is not names or special characters.
 *)
let other_char          = [^ ' ' '\t' '\012' '\n' '\r'
                           '_' 'A'-'Z' 'a'-'z' '0'-'9' '-' '?' '@' '~'
                           '$' '(' ')' ':' ',' ';' '=' '\\' '#' '%' '[' ']'
                           '.' '"' '\''
			   '<' '>' '^' '|' '&' '*' '/' '+']
let other_drive         = ['A'-'Z' 'a'-'z'] ':' ['\\' '/']
let other_prefix        = other_char | other_drive
let other_special       = ['~' '?']
let other_suffix1       = name_suffix | other_prefix | other_special
let other_suffix2       = other_prefix | other_special
let other               = other_prefix other_suffix1 * |
                          other_special other_suffix2 *

(*
 * A string is anything but a quote, dollar, or backslash.
 *)
let string_text = [^ '\'' '"' '$' '\\' '\r' '\n']+
let literal_text  = [^ '\'' '"' '|' '\r' '\n']+

(*
 * Main lexer.
 *)
rule lex_main state = parse
 | white_nl
 | comment_nl
   { let loc = state.current_loc in
     let _ = lexeme_loc state lexbuf in
     Lexing.new_line lexbuf;
     Parse.TokEol loc
   }
 | white
   { let s, loc = lexeme_string state lexbuf in
     Parse.TokWhite (s, loc)
   }
   (* Note: many numbers are also identifiers,
    * like the decimal numbers, etc.  We can define the
    * regular expressions normally, but give precedence
    * to identifiers. *)
   (* GS TODO. This doesn't seem to be correct. The regexp for [name] also
      parses [integer] and some forms of [float]. Because ocamllex picks
      the first regexp when two regexps match the same string, [name] is
      always preferred.
    *)
 | name
   { lexeme_name state lexbuf }
 | integer
   { let s, loc = lexeme_string state lexbuf in
     Parse.TokInt (s, loc)
   }
 | float
   { let s, loc = lexeme_string state lexbuf in
     Parse.TokFloat (s, loc)
   }
 | key        (* GS: name prefixed with '?' or '~' (named arguments) *)
   { lexeme_key state lexbuf }
 | ['\'' '"']
   { let id, loc = lexeme_string state lexbuf in
     let mode = ModeQuote id in
     push_mode state mode;
     Parse.TokBeginQuoteString (id, loc)
   }
 (* GS. Remember dquote and squote can be several quotes *)
 | '$' dquote
   { let id, loc = lexeme_string state lexbuf in
     let id = String.sub id 1 (pred (String.length id)) in
              (* GS TODO: use "as" *)
     let mode = ModeString id in
     push_mode state mode;
     Parse.TokBeginQuote ("", loc)
   }
 | '$' squote
   { let id, _ = lexeme_string state lexbuf in
     let id = String.sub id 1 (pred (String.length id)) in
              (* GS TODO: use "as" *)
     let s, loc = lex_literal state (Buffer.create 32) id lexbuf in
     (* GS: lex_literal is a sublexer. Returns the quoted string *)
     Parse.TokStringQuote (s, loc)
   }
 (* GS: e.g. $||text||, this is used for map keys *)
 | paren_dollar pipe
   { let strategy, id, _ = lexeme_dollar_pipe state lexbuf in
     let s, loc = lex_literal state (Buffer.create 32) id lexbuf in
     Parse.TokVarQuote (strategy, s, loc)
   }
 (* $ plus a single character *)
 | special_var
   { lexeme_var state lexbuf }
 (* other $ *)
 | dollar
   { lexeme_dollar state lexbuf }
 (* $ ( ) : , ; = . % + - * / < > ^ & | *)
 | special_char
   { lexeme_char state lexbuf }
 (* => :: += [] << >> >>> && || ... [...] *)
 | special_string
   { lexeme_special_string state lexbuf }
 | special_colon
   { let s, loc = lexeme_string state lexbuf in
     Parse.TokNamedColon (s, loc)
   }
 | other
   { let s, loc = lexeme_string state lexbuf in
     Parse.TokString (s, loc)
   }
 | '\\'
   { let s, loc = lexeme_string state lexbuf in
     Parse.TokString (s, loc)
   }
 | esc_char
   { let s, loc = lexeme_esc state lexbuf in
     Parse.TokStringQuote (s, loc)
   }
 | esc_line
   { let loc = lexeme_loc state lexbuf in
     Lexing.new_line lexbuf;
     state.current_prompt <- "\\";
     state.current_fill_ok <- true;
     Parse.TokString (" ", loc)
   }
 | eof
   { let loc = lexeme_loc state lexbuf in
     match state.current_token with
       | Parse.TokEol _
       | Parse.TokEof _ ->
           Parse.TokEof loc
       | _ ->
           Parse.TokEol loc
   }
 | _
   { let s, _ = lexeme_string state lexbuf in
     syntax_error state ("illegal character: " ^ String.escaped s) lexbuf
   }

(*
 * Inline text.  We allow any text, but dollars are expanded,
 * escape sequences are allowed, and unescaped newlines are
 * not allowed (this is the normal shell definition of
 * a quoted string).
 *
 * GS: text after double quotes
 *)
and lex_quote state = parse
 | strict_nl
   { Lexing.new_line lexbuf;
     syntax_error state "unterminated string" lexbuf
   }
 | '\\'
 | string_text
   { let s, loc = lexeme_string state lexbuf in
     Parse.TokString (s, loc)
   }
 | ['\'' '"']
   { let s, loc = lexeme_string state lexbuf in
     match state.current_mode with
       | ModeQuote s' when s' = s ->
           pop_mode state;
           Parse.TokEndQuoteString (s, loc)
       | _ ->
           Parse.TokString (s, loc)
   }
 | "$$"
   { let loc = lexeme_loc state lexbuf in
     Parse.TokString ("$", loc)
   }
 | special_var
   { lexeme_var state lexbuf }
 | paren_dollar
   { push_dollar state ModeNormal;
     lexeme_dollar state lexbuf
   }
 | esc_quote
   { let s, loc = lexeme_esc state lexbuf in
     Parse.TokString (s, loc)
   }
 | esc_line
   { let loc = lexeme_loc state lexbuf in
     Lexing.new_line lexbuf;
     state.current_fill_ok <- true;
     Parse.TokString ("", loc)
   }
 | eof
   { syntax_error state "unterminated string" lexbuf }
 | _
   { let s, _ = lexeme_string state lexbuf in
     syntax_error
       state
       ("illegal character in string constant: " ^ String.escaped s)
       lexbuf
   }

(*
 * Inline text.  We allow any text, but dollars are expanded.
 * Escape sequence other than an escaped newline are not
 * processed.
 *
 * GS: text after $ + double quotes
 *)
and lex_string state = parse
 | '\\'
 | string_text
   { let s, loc = lexeme_string state lexbuf in
     Parse.TokString (s, loc)
   }
 | quote
   { let s, loc = lexeme_string state lexbuf in
     match state.current_mode with
       | ModeString s' when s' = s ->
           pop_mode state;
           Parse.TokEndQuote ("", loc)
       | _ ->
           Parse.TokString (s, loc)
   }
 | "$$"
   { let loc = lexeme_loc state lexbuf in
     Parse.TokString ("$", loc)
   }
 | special_var
   { lexeme_var state lexbuf }
 | paren_dollar
   { push_dollar state ModeNormal;
     lexeme_dollar state lexbuf
   }
 | strict_nl
   { let s, loc = lexeme_string state lexbuf in
     let () =
       match state.current_mode with
         | ModeString s ->
             push_mode state (ModeSkipString s)
         | _ ->
             (* GS CHECK: When is this possible? *)
             () in
     Lexing.new_line lexbuf;
     state.current_fill_ok <- true;
     Parse.TokString (s, loc)
   }
 | esc_line              (* GS: Backslash before newline *)
   { let loc = lexeme_loc state lexbuf in
     let () =
       match state.current_mode with
         | ModeString s ->
             push_mode state (ModeSkipString s)
         | _ ->
             () in
     Lexing.new_line lexbuf;
     state.current_fill_ok <- true;
     Parse.TokString ("", loc)
   }
 | eof
   { syntax_error state "unterminated string" lexbuf }
 | _
   { let s, _ = lexeme_string state lexbuf in
     syntax_error state ("illegal character: " ^ String.escaped s) lexbuf
   }

and lex_skip_string state = parse
  (* GS. This also matches the empty string, so this rule is always matched.
     This is a technical sub-lexer of lex_string
   *)
   quote_opt
   { let s, loc = lexeme_string state lexbuf in
     pop_mode state;
     match state.current_mode with
       | ModeString s' when s' = s ->
           pop_mode state;
           Parse.TokEndQuote ("", loc)
       | _ ->
           Parse.TokString ("", loc)
   }

(*
 * Text, but we don't expand variables.
 * GS. E.g. after $''. Parses until matching ''
 *)
and lex_literal state buf equote = parse
 | strict_nl
   { let s, _ = lexeme_string state lexbuf in
     Lexing.new_line lexbuf;
     state.current_fill_ok <- true;
     Buffer.add_string buf s;
     lex_literal_skip state buf equote lexbuf
   }
 | literal_text
   { let s, _ = lexeme_string state lexbuf in
     Buffer.add_string buf s;
     lex_literal state buf equote lexbuf
   }
 | quote
   { let s, loc = lexeme_string state lexbuf in
     if s = equote then
       let s = Buffer.contents buf in
       s, loc
     else
       begin
         Buffer.add_string buf s;
         lex_literal state buf equote lexbuf
       end
   }
 | eof
   { syntax_error state "unterminated string" lexbuf }
 | _
   { let s, _ = lexeme_string state lexbuf in
     syntax_error state ("illegal character: " ^ String.escaped s) lexbuf
   }

and lex_literal_skip state buf equote = parse
 | quote_opt
   { let s, loc = lexeme_string state lexbuf in
     if s = equote then
       let s = Buffer.contents buf in
       s, loc
     else
       lex_literal state buf equote lexbuf
   }

(*
 * Parse the whitespace at the beginning of the line.
 *)
and lex_indent state = parse
 | comment_eol
 | white_nl
   { Lexing.new_line lexbuf;
     state.current_fill_ok <- true;
     lex_indent state lexbuf
   }
 | opt_white
   { let s, _ = lexeme_string state lexbuf in
     let indent = indent_of_string s in
     indent
   }

(*
 * For speed, define a scanner just for dependency files.
 *)
and lex_deps = parse
 | name
 | white
 | other_drive
 | '\\'
   { let s, loc = lexeme_pos lexbuf in
     Parse.TokString (s, loc)
   }
 | "\\:"
   { let _, loc = lexeme_pos lexbuf in
     Parse.TokString (":", loc)
   }
 | ':'
   { let s, loc = lexeme_pos lexbuf in
     Parse.TokColon (s, loc)
   }
 | ['"' '\'']
   { let s, loc = lexeme_pos lexbuf in
     let buf = Buffer.create 64 in
     Buffer.add_string buf s;
     lex_deps_quote s buf lexbuf;
     Parse.TokString (Buffer.contents buf, loc)
   }
 | white_nl
 | comment_nl
   { let _, loc = lexeme_pos lexbuf in
     Parse.TokEol loc
   }
 | esc_char
   { let s, loc = lexeme_pos lexbuf in
     let s = String.make 1 s.[1] in
     Parse.TokStringQuote (s, loc)
   }
 | esc_line
   { let _, loc = lexeme_pos lexbuf in
     Parse.TokWhite (" ", loc)
   }
 | _
   { let s, loc = lexeme_pos lexbuf in
     Parse.TokString (s, loc)
   }
 | eof
   { let _, loc = lexeme_pos lexbuf in
     Parse.TokEof loc
   }

and lex_deps_quote term buf = parse
 | '\\'
 | '\\' ['"' '\'']
 | [^ '\\' '"' '\'']+
   { let s, _ = lexeme_pos lexbuf in
     Buffer.add_string buf s;
     lex_deps_quote term buf lexbuf
   }
 | ['\'' '"']
   { let s, _ = lexeme_pos lexbuf in
     Buffer.add_string buf s;
     if s <> term then
       lex_deps_quote term buf lexbuf
   }
 | _
 | eof
   { raise Parsing.Parse_error }

{
(************************************************************************
 * Prompts.
 *)

(*
 * Lex and parse a line for the shell.
 *)
let tabstop = 3

let prompt_ext s =
  s ^ "> "

(* Prune the prompt to a reasonable length *)
let prompt_prune prompt indent =
  let max_len = 8 in
  let s = Bytes.make (indent * tabstop + max_len + 2) ' ' in
  let length = String.length prompt in
  if length > max_len then
    begin
      Bytes.blit_string prompt 0 s 0 max_len;
      Bytes.set s max_len '>'
    end
  else
    Bytes.blit_string prompt 0 s 0 length;
  Bytes.to_string s

let prompt_indent prompt root indent =
  if root then
    prompt
  else
    prompt_prune prompt indent

let prompt_string state root nest e =
  let prompt = prompt_ext (Util.key_of_exp e) in
  if state.is_interactive && root then
    Format.printf "%s%s@?" (prompt_prune prompt nest) state.current_buffer;
  prompt

(*
 * Parser for the body of an expression.
 *)
let body_parser state body =
  match body with
    | NoBody ->
        None
    | OptBody ->
        if state.is_interactive then
          None
        else
          Some Parse.shell
    | ColonBody ->
        Some Parse.shell
    | ArrayBody ->
        Some Parse.string

(************************************************************************
 * Lexing input.
 *)

(*
 * Copy into the lexbuf.
 *)
let lex_fill state buf len =
  let { current_buffer = buffer;
        current_index = index;
        _
      } = state in
  let length = String.length buffer in
  let amount = min (length - index) len in
  if amount = 0 then
    state.current_eof <- true
  else
    begin
      String.blit buffer index buf 0 amount;
      state.current_index <- index + amount
    end;
  amount

(*
 * Refill the buffer using the readline function.
 *)
let state_refill state =
  let { current_fill_ok = fill_ok;
        current_prompt = prompt;
        readline = readline;
        _
      } = state in
  if fill_ok then
    match readline prompt with
      | Some line ->
          let line =
            if state.is_interactive && line = ".\n" then
              ""
            else
              line in
          state.current_buffer <- line;
          state.current_index <- 0;
          state.current_fill_ok <- false
      | None ->  (* eof *)
          ()

(*
 * Lexer function to refill the buffer.
 * GS. This is for Lexing.from_function.
 *)
let lex_refill state buf len =
  let { current_buffer = buffer;
        current_index = index;
        _
      } = state in
  let length = String.length buffer in
  let amount = length - index in
  if amount = 0 then
    state_refill state;
  lex_fill state buf len

(************************************************************************
 * Main lexer.
 *)

(*
 * Get the input.
 *)
let lex_line state lexbuf =
  let tok =
    match state.current_mode with
      | ModeNormal ->
          lex_main state lexbuf
      | ModeString _ ->
          lex_string state lexbuf
      | ModeSkipString _ ->
          lex_skip_string state lexbuf
      | ModeQuote _ ->
          lex_quote state lexbuf in
  if !debug_lex then
    Format.eprintf "Token: %a@." Print.pp_print_token tok;
  state.current_token <- tok;
  tok

(************************************************************************
 * Parse main loop.
 *)

(*
 * Make sure the lexbuf is valid.
 *)
let parse_refill state prompt root nest =
  if state.current_eof then
    begin
      let open Lexing in
      let olb = state.current_lexbuf in
      let lexbuf = Lexing.from_function (lex_refill state) in
      let lexbuf = { lexbuf with
                     lex_start_p =
                       { olb.lex_start_p with
                         pos_bol = olb.lex_start_p.pos_bol -
                                     olb.lex_start_p.pos_cnum;
                         pos_cnum = 0;
                       };
                     lex_curr_p =
                       { olb.lex_curr_p with
                         pos_bol = olb.lex_curr_p.pos_bol -
                                     olb.lex_curr_p.pos_cnum;
                         pos_cnum = 0;
                       }
                   } in
      state.current_eof     <- false;
      state.current_fill_ok <- true;
      state.current_prompt  <- prompt_indent prompt root nest;
      state.current_lexbuf  <- lexbuf;
      state.current_lexmode <- LexModeInitial;
    end

(*
 * Get the current indentation level.
 *)
let parse_indent state prompt root nest =
  parse_refill state prompt root nest;
  match state.current_lexmode with
    | LexModeInitial ->
        let indent =
          (* Interactive shell ignores indentation *)
          if state.is_interactive then
            nest
          else
            lex_indent state state.current_lexbuf in
        if !debug_lex then
          Format.eprintf "indent: %d@." indent;
        (* Remember that we scanned now the indentation, and need not to
           do it again for the same line:
         *)
        state.current_lexmode <- LexModeNormal indent;
        indent
    | LexModeNormal indent ->
        indent

(* GS. In the following, parse = Omake_ast_parse.shell, i.e. the ocamlyacc
   generated parser
 *)

(*
 * Parse a single expression.
 * GS. an "expression" is not just a $-expression, but any code block, which
 * may span several lines.
 *)
let rec parse_exp state parse prompt root nest =
  let indent = parse_indent state prompt root nest in
  if indent > state.current_indent then
    syntax_error state "illegal indentation" state.current_lexbuf
  else if indent < state.current_indent then
    None
  else
    parse_exp_indent state parse prompt root nest

and parse_exp_indent state parse _ root nest =
  (* GS: after the indentation... *)
  match parse (lex_line state) state.current_lexbuf with
    | Some (code, e) ->
        (* GS: e is the parsed expression *)
        let code = Util.scan_body_flag code e in
        let parse = body_parser state code in
        (* GS. parse is now None, or Some Omake_ast_parse.shell or .string *)
        ( match parse with
            | Some parse ->
                let prompt = prompt_string state root nest e in
                let body = parse_body state parse prompt nest in
                let e = Util.update_body e code body in
                ( match Util.can_continue e with
                    | Some prompt ->
                        ( match
                            parse_exp state parse (prompt_ext prompt) false nest
                          with
                            | Some cont ->
                                Some(e :: cont)
                            | None ->
                                Some [e]
                        )
                    | None ->
                        Some [e]
                )
            | None ->
                Some [e]
        )
    | None ->
        None
    | exception Parsing.Parse_error ->
        parse_error state

and parse_body state parse prompt nest =
  let nest = succ nest in
  let indent = parse_indent state prompt false nest in
  (* GS. The body must be further indented, otherwise it is not a body
     of the preceding expr
   *)
  if indent > state.current_indent then
    begin
      push_mode state ModeNormal;
      state.current_indent <- indent;
      parse_body_indent state parse prompt nest []
    end
  else
    []

and parse_body_indent state parse prompt nest el =
  (* GS TODO: reformulate with "match ... with exception" *)
  let e =
    match parse_exp state parse prompt false nest with
      | Some e ->
          ParseExp e
      | None ->
          (* decreased level of indentation *)
          if state.is_interactive then
            Format.printf ".@.";
          pop_mode state;
          ParseEOF
(*
      | Omake_value_type.OmakeException _ as exn when state.is_interactive ->
          Format.eprintf "%a@." Omake_exn_print.pp_print_exn exn;
          ParseError
 *) in
  match e with
    | ParseExp e ->
        parse_body_indent state parse prompt nest (List.rev_append e el)
    | ParseError ->
        parse_body_indent state parse prompt nest el
    | ParseEOF ->
        List.rev el

(*
 * Parse a file.
 * GS: Entry point
 *)
let parse_file name =
  let inx = open_in name in
  let readline _ =
    try Some(input_line inx ^ "\n") with
      | End_of_file -> None in
  let state = create name readline in
  let el = parse_body_indent state Parse.shell "<prompt>" 0 [] in
  close_in inx;
  el

(*
 * Parse a string.
 * GS: Entry point
 *)
let parse_string s =
  let len = String.length s in
  let index = ref 0 in
  let readline _ =
    let start = !index in
    let rec search i =
      if i = len then
        if start < i then
          begin
            index := i;
            Some(String.sub s start (i - start) ^ "\n")
          end
        else
          None
      else if s.[i] = '\n' then
        begin
          index := i + 1;
          Some(String.sub s start (i - start + 1))
        end
      else
        search (succ i) in
    search start in
  let state = create "-" readline in
  parse_body_indent state Parse.shell "<prompt>" 0 []

(* Stubs for readline integration *)
let stub_readline prompt =
  print_string prompt;
  flush stdout;
  try Some(input_line stdin)
  with End_of_file -> None

let stub_is_interactive() =
  Unix.isatty Unix.stdin

let stub_flush() =
  flush stdout

(*
 * Parse an expression.
 *)
let create_shell () =
  let state = create "-" stub_readline in
  state.is_interactive <- stub_is_interactive ();
  state

(*
 * Copy the state, if an exception happens, then
 * restore the initial state.
 *)
let parse_shell state prompt =
  let stack = save_mode state in
  state.current_fill_ok <- true;
  try
    match parse_exp state Parse.shell prompt true 0 with
      | Some l -> l
      | None -> []   (* CHECK. Unclear case *)
  with
    | exn ->
        stub_flush ();
        restore_mode state stack;
        state.current_buffer <- "";
        state.current_index <- 0;
        raise exn

(*
 * Just dependency analysis.
 *)
let parse_deps name =
  let inx = open_in name in
  let lexbuf = Lexing.from_channel inx in
  let deps =
    try Parse.deps lex_deps lexbuf
    with
      | exn ->
          close_in inx;
          Format.eprintf
            "%s: char %d: scanner dependency syntax error@."
            name (Lexing.lexeme_end lexbuf);
          raise exn in
  close_in inx;
  deps
}
