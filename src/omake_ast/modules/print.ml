(* This is a modified copy of omake_ast_print.ml and covered by the GPL *)

open Types

let pp_print_location buf (l1,l2) =
  let open Lexing in
  if l1.pos_fname = l2.pos_fname then (
    Format.fprintf buf "File %s: " l1.pos_fname;
    if l1.pos_lnum = l2.pos_lnum then
      Format.fprintf buf "line %d, characters %d-%d"
                     l1.pos_lnum
                     (l1.pos_cnum - l1.pos_bol)
                     (l2.pos_cnum - l2.pos_bol)
    else
      Format.fprintf buf "lines %d:%d-%d:%d"
                     l1.pos_lnum
                     (l1.pos_cnum - l1.pos_bol)
                     l2.pos_lnum
                     (l2.pos_cnum - l2.pos_bol)
  ) else (
    Format.fprintf buf "File %s: " l1.pos_fname;
    Format.fprintf buf "line %d, character %d until "
                     l1.pos_lnum
                     (l1.pos_cnum - l1.pos_bol);
    Format.fprintf buf "file %s: " l2.pos_fname;
    Format.fprintf buf "line %d, character %d "
                     l2.pos_lnum
                     (l2.pos_cnum - l2.pos_bol);
  )

let pp_print_symbol = Format.pp_print_string

let pp_print_qvar buf v =
  if v.qcurry then
    Format.fprintf buf "curry.";
  let p =
    match v.qprefix with
      | Global -> "global."
      | Private -> "private."
      | Shared -> "shared."
      | This -> "this."
      | Primitive -> "primitive."
      | Unqualified -> "" in
  Format.fprintf buf "%s%s" p v.qname

let pp_print_method_name buf (v,vl) =
  pp_print_qvar buf v;
  Format.fprintf buf "%s" (String.concat "." vl)

let pp_print_strategy buf s =
  match s with
    | LazyApply -> Format.pp_print_char buf '\''
    | EagerApply -> Format.pp_print_char buf ','
    | NormalApply -> ()
    | CommandApply -> Format.pp_print_char buf '#'

let pp_print_define_kind buf flag =
  match flag with
    | DefineString ->
        ()
    | DefineArray ->
        Format.pp_print_string buf "[]"

let pp_print_define_flag buf flag=
  let s =
    match flag with
      | DefineNormal -> "="
      | DefineAppend -> "+=" in
  Format.pp_print_string buf s

let print_location = ref false

let rec pp_print_exp buf e =
  if !print_location then
    Format.fprintf buf "<%a>" pp_print_location (Util.loc_of_exp e);
  match e with
    | NullExp _ ->
        Format.pp_print_string buf "<null>"
    | IntExp (i, _) ->
        Format.fprintf buf "(int %d)" i
    | FloatExp (x, _) ->
        Format.fprintf buf "(float %f)" x
    | StringOpExp (s, _) ->
        Format.fprintf buf "(string-op \"%s\")" (String.escaped s)
    | StringIdExp (s, _) ->
        Format.fprintf buf "(string-id \"%s\")" (String.escaped s)
    | StringIntExp (s, _) ->
        Format.fprintf buf "(string-int \"%s\")" (String.escaped s)
    | StringFloatExp (s, _) ->
        Format.fprintf buf "(string-float \"%s\")" (String.escaped s)
    | StringWhiteExp (s, _) ->
        Format.fprintf buf "(string-white \"%s\")" (String.escaped s)
    | StringOtherExp (s, _) ->
        Format.fprintf buf "(string-other \"%s\")" (String.escaped s)
    | StringKeywordExp (s, _) ->
        Format.fprintf buf "(string-keyword \"%s\")" (String.escaped s)
    | QuoteExp (el, _) ->
        Format.fprintf buf "@[<hv 3>(quote";
        List.iter (fun e ->
            Format.fprintf buf "@ %a" pp_print_exp e) el;
        Format.fprintf buf ")@]"
    | QuoteStringExp (c, el, _) ->
        Format.fprintf buf "@[<hv 3>(quoted-string %c" c;
        List.iter (fun e ->
            Format.fprintf buf "@ %a" pp_print_exp e) el;
        Format.fprintf buf "%c)@]" c
    | SequenceExp (el, _) ->
        Format.fprintf buf "@[<hv 3>(sequence";
        List.iter (fun e ->
            Format.fprintf buf "@ %a" pp_print_exp e) el;
        Format.fprintf buf ")@]"
    | ArrayExp (el, _) ->
        Format.fprintf buf "@[<hv 3>(array";
        List.iter (fun e ->
            Format.fprintf buf "@ %a" pp_print_exp e) el;
        Format.fprintf buf ")@]"
    | ApplyExp (LazyApply, v, [], _) ->
        Format.fprintf buf "$%a" pp_print_qvar v
    | ApplyExp (s, v, args, _) ->
        Format.fprintf
          buf "@[<hv 3>%a%a(%a)@]"
          pp_print_qvar v
          pp_print_strategy s
          pp_print_args args
    | SuperApplyExp (s, super, v, args, _) ->
        Format.fprintf
          buf "@[<hv 3>%a%a::%a(%a)@]"
          pp_print_symbol super
          pp_print_strategy s
          pp_print_symbol v
          pp_print_args args
    | MethodApplyExp (s, v, vl, args, _) ->
        Format.fprintf
          buf "@[<hv 3>%a%a(%a)@]"
          pp_print_method_name (v,vl)
          pp_print_strategy s
          pp_print_args args
    | CommandExp (v, arg, commands, _) ->
        Format.fprintf
          buf "@[<hv 0>@[<hv 3>command %a(%a) {%a@]@ }@]"
          pp_print_qvar v
          pp_print_exp arg
          pp_print_exp_list commands
    | VarDefExp (v, kind, flag, e, _) ->
        Format.fprintf
          buf "@[<hv 3>let %a%a %a@ %a@]"
          pp_print_qvar v
          pp_print_define_kind kind
          pp_print_define_flag flag
          pp_print_exp e
    | VarDefBodyExp (v, kind, flag, el, _) ->
        Format.fprintf
          buf "@[<hv 3>let %a%a %a@ %a@]"
          pp_print_qvar v
          pp_print_define_kind kind
          pp_print_define_flag flag
          pp_print_exp_list el
    | KeyExp (strategy, v, _) ->
        Format.fprintf buf "$%a|%s|" pp_print_strategy strategy v
    | KeyDefExp (v, kind, flag, e, _) ->
        Format.fprintf
          buf "@[<hv 3>\"%s\"%a %a@ %a@]"
          v
          pp_print_define_kind kind
          pp_print_define_flag flag
          pp_print_exp e
    | KeyDefBodyExp (v, kind, flag, el, _) ->
        Format.fprintf
          buf "@[<hv 3>key \"%s\"%a %a@ %a@]"
          v
          pp_print_define_kind kind
          pp_print_define_flag flag
          pp_print_exp_list el
    | ObjectDefExp (v, flag, el, _) ->
        Format.fprintf
          buf "@[<hv 3>let %a. %a@ %a@]"
          pp_print_qvar v
          pp_print_define_flag flag
          pp_print_exp_list el;
    | FunDefExp (v, vars, el, _) ->
        Format.fprintf
          buf "@[<hv 3>let %a(%a) ="
          pp_print_qvar v
          pp_print_params vars;
        List.iter (fun e -> Format.fprintf buf "@ %a" pp_print_exp e) el;
        Format.fprintf buf "@]"
    | RuleExp (multiple, target, pattern, source, commands, _) ->
        Format.fprintf
          buf "@[<hv 0>\
               @[<hv 3>rule {@ multiple = %b;@ \
               @[<hv 3>target =@ %a;@]@ \
               @[<hv 3>pattern =@ %a;@]@ \
               @[<hv 3>source =@ %a@]@ %a@]@ }@]"
          multiple
          pp_print_exp target
          pp_print_exp pattern
          pp_print_table_exp source
          pp_print_exp_list commands
    | BodyExp (body, _) ->
        Format.fprintf buf "@[<v 3>body";
        List.iter (fun e -> Format.fprintf buf "@ %a" pp_print_exp e) body;
        Format.fprintf buf "@]"
    | ShellExp (e, _) ->
        Format.fprintf buf "@[<hv 3>shell %a@]" pp_print_exp e
    | CatchExp (name, v, body, _) ->
        Format.fprintf
          buf "@[<v 3>catch %a(%a)@ %a@]"
          pp_print_symbol name
          pp_print_symbol v
          pp_print_exp_list body
    | ClassExp (names, _) ->
        Format.fprintf buf "@[<hv 3>class";
        List.iter (fun v -> Format.fprintf buf "@ %a" pp_print_symbol v) names;
        Format.fprintf buf "@]"
    | ExtendsExp (e, _) ->
        Format.fprintf buf "@[<hv 3>extends %a@]" pp_print_exp e

(*
 * Parameters.
 *)
and pp_print_param buf param =
  match param with
    | OptionalParam (v, e, _) ->
        Format.fprintf buf "@[<hv 3>?%a =@ %a@]"
                       pp_print_symbol v pp_print_exp e
    | RequiredParam (v, _) ->
        Format.fprintf buf "~%a" pp_print_symbol v
    | NormalParam (v, _) ->
        pp_print_symbol buf v

and pp_print_params buf vars =
  match vars with
    | [v] ->
        pp_print_param buf v
    | v :: vars ->
        Format.fprintf buf "%a,@ " pp_print_param v;
        pp_print_params buf vars
    | [] ->
        ()

and pp_print_arrow_arg buf params e =
  Format.fprintf buf "@[<hv 3>%a =>@ %a@]" pp_print_params params pp_print_exp e

and pp_print_arg buf arg =
  match arg with 
    | KeyArg (v, e) ->
        Format.fprintf buf "@[<hv 3>~%a =@ %a@]"
                       pp_print_symbol v pp_print_exp e
    | ExpArg e ->
        pp_print_exp buf e
    | ArrowArg (params, e) ->
        pp_print_arrow_arg buf params e

and pp_print_args buf args =
  match args with
    | [arg] ->
        pp_print_arg buf arg
    | arg :: args ->
        pp_print_arg buf arg;
        Format.fprintf buf ",@ ";
        pp_print_args buf args
    | [] ->
        ()

and pp_print_exp_list buf commands =
  List.iter (fun e -> Format.fprintf buf "@ %a" pp_print_exp e) commands

and pp_print_table_exp buf source =
  Format.fprintf buf "@[<hv 0>@[<hv 3>{";
  SMap.iter
    (fun v e ->
      Format.fprintf buf "@ %a = %a" pp_print_symbol v pp_print_exp e
    )
    source;
  Format.fprintf buf "@]@ }@]"

(*
 * A program is a list of expressions.
 *)
let pp_print_prog buf prog =
  Format.fprintf buf "@[<v 0>Prog:";
  List.iter (fun e -> Format.fprintf buf "@ %a" pp_print_exp e) prog;
  Format.fprintf buf "@]"

(*
 * Simplified printing.
 *)
let rec pp_print_simple_exp buf e =
  if !print_location then
    Format.fprintf buf "<%a>" pp_print_location (Util.loc_of_exp e);
  match e with
    | NullExp _ -> Format.pp_print_string buf "<null>"
    | IntExp (i, _) ->
        Format.fprintf buf "%d" i
    | FloatExp (x, _) ->
        Format.fprintf buf "%f" x
    | StringOpExp (s, _)
      | StringIdExp (s, _)
      | StringIntExp (s, _)
      | StringFloatExp (s, _)
      | StringWhiteExp (s, _)
      | StringOtherExp (s, _)
      | StringKeywordExp (s, _) ->    Format.pp_print_string buf s
    | QuoteExp (el, _) ->
        Format.fprintf buf "$'%a'" pp_print_simple_exp_list el
    | QuoteStringExp (c, el, _) ->
        Format.fprintf buf "%c%a%c" c pp_print_simple_exp_list el c
    | SequenceExp (el, _) ->
        pp_print_simple_exp_list buf el
    | ArrayExp (el, _) ->
        Format.fprintf buf "@[<hv 3>(array";
        List.iter (fun e ->
            Format.fprintf buf "@ %a" pp_print_exp e) el;
        Format.fprintf buf ")@]"
    | ApplyExp (LazyApply, v, [], _) ->
        Format.fprintf buf "$%a" pp_print_qvar v
    | ApplyExp (s, v, args, _) ->
        Format.fprintf
          buf "@[<hv 3>%a%a(%a)@]"
          pp_print_strategy s
          pp_print_qvar v
          pp_print_simple_args args
    | SuperApplyExp (s, super, v, args, _) ->
        Format.fprintf
          buf "@[<hv 3>%a%a::%a(%a)@]"
          pp_print_symbol super
          pp_print_strategy s
          pp_print_symbol v
          pp_print_simple_args args
    | MethodApplyExp (s, v, vl, args, _) ->
        Format.fprintf
          buf "@[<hv 3>%a%a(%a)@]"
          pp_print_method_name (v,vl)
          pp_print_strategy s
          pp_print_simple_args args
    | CommandExp (v, arg, commands, _) ->
        Format.fprintf
          buf "@[<hv 0>@[<hv 3>command %a(%a) {%a@]@ }@]"
          pp_print_qvar v
          pp_print_simple_exp arg
          pp_print_simple_exp_list commands
    | VarDefExp (v, kind, flag, e, _) ->
        Format.fprintf
          buf "@[<hv 3>let %a%a %a@ %a@]"
          pp_print_qvar v
          pp_print_define_kind kind
          pp_print_define_flag flag
          pp_print_simple_exp e
    | VarDefBodyExp (v, kind, flag, el, _) ->
        Format.fprintf
          buf "@[<hv 3>let %a%a %a@ %a@]"
          pp_print_qvar v
          pp_print_define_kind kind
          pp_print_define_flag flag
          pp_print_simple_exp_list el
    | KeyExp (strategy, v, _) ->
        Format.fprintf
          buf "$%a|%s|" pp_print_strategy strategy v
    | KeyDefExp (v, kind, flag, e, _) ->
        Format.fprintf
          buf "@[<hv 3>\"%s\"%a %a@ %a@]"
          v
          pp_print_define_kind kind
          pp_print_define_flag flag
          pp_print_simple_exp e
    | KeyDefBodyExp (v, kind, flag, el, _) ->
        Format.fprintf
          buf "@[<hv 3>key \"%s\"%a %a@ %a@]"
          v
          pp_print_define_kind kind
          pp_print_define_flag flag
          pp_print_simple_exp_list el
    | ObjectDefExp (v, flag, el, _) ->
        Format.fprintf
          buf "@[<hv 3>let %a. %a@ %a@]"
          pp_print_qvar v
          pp_print_define_flag flag
          pp_print_simple_exp_list el
    | FunDefExp (v, vars, el, _) ->
        Format.fprintf
          buf "@[<hv 3>let %a(%a) ="
          pp_print_qvar v
          pp_print_params vars;
        List.iter (fun e -> Format.fprintf buf "@ %a" pp_print_exp e) el;
        Format.fprintf buf "@]"
    | RuleExp (multiple, target, pattern, source, commands, _) ->
        Format.fprintf
          buf "@[<hv 0>@[<hv 3>rule {@ multiple = %b;@ \
               @[<hv 3>target =@ %a;@]@ \
               @[<hv 3>pattern =@ %a;@]@ \
               @[<hv 3>source =@ %a@]@ %a@]@ }@]"
          multiple
          pp_print_simple_exp target
          pp_print_simple_exp pattern
          pp_print_table_exp source
          pp_print_simple_exp_list commands
    | BodyExp (body, _) ->
        Format.fprintf buf "@[<v 3>body";
        List.iter
          (fun e -> Format.fprintf buf "@ %a" pp_print_simple_exp e)
          body;
        Format.fprintf buf "@]"
    | ShellExp (e, _) ->
        Format.fprintf buf "@[<hv 3>shell %a@]" pp_print_simple_exp e
    | CatchExp (name, v, body, _) ->
        Format.fprintf
          buf "@[<v 3>catch %a(%a)@ %a@]"
          pp_print_symbol name
          pp_print_symbol v
          pp_print_simple_exp_list body
    | ClassExp (names, _) ->
        Format.fprintf buf "@[<hv 3>class";
        List.iter (fun v -> Format.fprintf buf "@ %a" pp_print_symbol v) names;
        Format.fprintf buf "@]"
    | ExtendsExp (e, _) ->
        Format.fprintf buf "@[<hv 3>extends %a@]" pp_print_simple_exp e

and pp_print_simple_exp_list buf el =
  List.iter (pp_print_simple_exp buf) el

and pp_print_simple_args buf args =
  match args with
    | [arg] ->
        pp_print_simple_arg buf arg
    | arg :: args ->
        pp_print_simple_arg buf arg;
        Format.fprintf buf ",@ ";
        pp_print_simple_args buf args
    | [] ->
        ()

and pp_print_simple_arg buf arg =
  match arg with 
    | KeyArg (v, e) ->
        Format.fprintf
          buf "@[<hv 3>~%a =@ %a@]" pp_print_symbol v pp_print_exp e
    | ExpArg e ->
        pp_print_simple_exp buf e
    | ArrowArg (params, e) ->
        pp_print_arrow_arg buf params e
                           
(************************************************************************
 * Printing.  NOTICE: if new tokens are added, please update
 * the token list in omake_gen_parse.ml!!!
 *)
let pp_print_token buf =
  let open Parse in
  function
  | TokEof _ ->
       Format.pp_print_string buf "<eof>"
  | TokEol _ ->
       Format.pp_print_string buf "<eol>"
  | TokWhite (s, _) ->
       Format.fprintf buf "whitespace: \"%s\"" s
  | TokLeftParen (s, _) ->
       Format.fprintf buf "left parenthesis: %s" s
  | TokRightParen (s, _) ->
       Format.fprintf buf "right parenthesis: %s" s
  | TokArrow (s, _) ->
       Format.fprintf buf "arrow: %s" s
  | TokComma (s, _) ->
       Format.fprintf buf "comma: %s" s
  | TokColon (s, _) ->
       Format.fprintf buf "colon: %s" s
  | TokDoubleColon (s, _) ->
       Format.fprintf buf "doublecolon: %s" s
  | TokNamedColon (s, _) ->
       Format.fprintf buf "named colon: %s" s
  | TokDollar (s, strategy, _) ->
       Format.fprintf buf "dollar: %s%a" s pp_print_strategy strategy
  | TokEq (s, _) ->
       Format.fprintf buf "equals: %s" s
  | TokArray (s, _) ->
       Format.fprintf buf "array: %s" s
  | TokDot (s, _) ->
       Format.fprintf buf "dot: %s" s
  | TokId (s, _) ->
       Format.fprintf buf "id: %s" s
  | TokInt (s, _) ->
       Format.fprintf buf "int: %s" s
  | TokFloat (s, _) ->
       Format.fprintf buf "float: %s" s
  | TokKey (s, _) ->
       Format.fprintf buf "key: %s" s
  | TokKeyword (s, _) ->
       Format.fprintf buf "keyword: %s" s
  | TokCatch (s, _) ->
       Format.fprintf buf "catch: %s" s
  | TokClass (s, _) ->
       Format.fprintf buf "class: %s" s
  | TokExtends (s, _) ->
       Format.fprintf buf "extends: %s" s
  | TokVar (_, s, _) ->
       Format.fprintf buf "var: %s" s
  | TokOp (s, _) ->
       Format.fprintf buf "op: %s" s
  | TokString (s, _) ->
       Format.fprintf buf "string: \"%s\"" (String.escaped s)
  | TokBeginQuote (s, _) ->
       Format.fprintf buf "begin-quote: %s" s
  | TokEndQuote (s, _) ->
       Format.fprintf buf "end-quote: %s" s
  | TokBeginQuoteString (s, _) ->
       Format.fprintf buf "begin-quote-string: %s" s
  | TokEndQuoteString (s, _) ->
       Format.fprintf buf "end-quote-string: %s" s
  | TokStringQuote (s, _) ->
       Format.fprintf buf "quote: %s" s
  | TokVarQuote (_, s, _) ->
       Format.fprintf buf "key: %s" s

