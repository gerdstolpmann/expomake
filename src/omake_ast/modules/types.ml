(* This is a modified copy of omake_ast.ml and covered by the GPL *)

type var = string

type body_flag =
  | NoBody
  | OptBody
  | ColonBody
  | ArrayBody

type apply_strategy =
  | LazyApply
  | EagerApply
  | NormalApply
  | CommandApply

type define_kind =
  | DefineString
  | DefineArray

type define_flag =
  | DefineNormal
  | DefineAppend

type location = Lexing.position * Lexing.position

type qprefix =
  | Private
  | Global
  | Shared
  | This
  | Primitive
  | Unqualified

type qvar =
  { qprefix : qprefix;
    qcurry : bool;
    qname : string
  }

module SMap = Map.Make(String)

type exp =
  | NullExp         of location
  | IntExp          of int * location
  | FloatExp        of float * location
  | StringOpExp     of string * location
  | StringIdExp     of string * location
  | StringIntExp    of string * location
  | StringFloatExp  of string * location
  | StringWhiteExp  of string * location
  | StringOtherExp  of string * location
  | StringKeywordExp of string * location
  | QuoteExp        of exp list * location
  | QuoteStringExp  of char * exp list * location
  | SequenceExp     of exp list * location
  | ArrayExp        of exp list * location
  | ApplyExp        of apply_strategy * qvar * arg list * location
  | SuperApplyExp   of apply_strategy * var * var * arg list * location
  | MethodApplyExp  of apply_strategy * qvar * var list * arg list * location
  | CommandExp      of qvar * exp * exp list * location
  | VarDefExp       of qvar * define_kind * define_flag * exp * location
  | VarDefBodyExp   of qvar * define_kind * define_flag * exp list * location
  | ObjectDefExp    of qvar * define_flag * exp list * location
  | FunDefExp       of qvar * params * exp list * location
  | RuleExp         of bool * exp * exp * exp SMap.t * exp list * location
  | BodyExp         of exp list * location
  | ShellExp        of exp * location
  | CatchExp        of var * var * exp list * location
  | ExtendsExp      of exp * location
  | ClassExp        of string list * location
  | KeyExp          of apply_strategy * string * location
  | KeyDefExp       of string * define_kind * define_flag * exp * location
  | KeyDefBodyExp   of string * define_kind * define_flag * exp list * location

and params = param list

(* GS. param = formal param of a lambda *)
and param =
  | OptionalParam of var * exp * location
  | RequiredParam of var * location
  | NormalParam   of var * location

(* GS. arg = actual argument of an application *)
and arg      =
  | KeyArg      of var * exp
  | ExpArg      of exp
  | ArrowArg    of param list * exp

and parse_arg =
  | IdArg       of string * (string * location) option * location   (* Second string is always whitespace *)
  | NormalArg   of arg

and args     = arg list

type prog = exp list

type error_arg =
  | Error_unit
  | Error_string of string
  | Error_ast of exp

exception Syntax_error of location * string * error_arg

let unqual_qvar id =
  { qprefix = Unqualified; qcurry = false; qname = id }

let parse_qvar l =
  let multi_error() =
    failwith "Several qualifiers found" in
  let rec parse qvar l =
    match l with
      | "curry" :: l' ->
          if qvar.qcurry then
            failwith "The modifier 'curry' is given twice";
          parse { qvar with qcurry=true } l'
      | "global" :: l' ->
          if qvar.qprefix <> Unqualified then
            multi_error();
          parse { qvar with qprefix = Global } l'
      | "private" :: l' ->
          if qvar.qprefix <> Unqualified then
            multi_error();
          parse { qvar with qprefix = Private } l'
      | "shared" :: l' ->
          if qvar.qprefix <> Unqualified then
            multi_error();
          parse { qvar with qprefix = Shared } l'
      | "this" :: l' ->
          if qvar.qprefix <> Unqualified then
            multi_error();
          parse { qvar with qprefix = This } l'
      | "primitive" :: l' ->
          if qvar.qprefix <> Unqualified then
            multi_error();
          parse { qvar with qprefix = Primitive } l'
      | name :: l' ->
          { qvar with qname = name }, l'
      | [] ->
          failwith "Missing variable name" in
  parse (unqual_qvar "") l

let parse_qvar_from_string s =
  parse_qvar (String.split_on_char '.' s)

let words e =
  (* This respects quoting *)
  let buf = Buffer.create 32 in
  let list = ref [] in
  let next() =
    let s = Buffer.contents buf in
    Buffer.clear buf;
    if s <> "" then
      list := s :: !list in
  let rec collect_exp quoted e =
    match e with
      | NullExp _ ->
          ()
      | StringOpExp (s, _)
      | StringIdExp (s, _)
      | StringIntExp (s, _)
      | StringFloatExp (s, _)
      | StringOtherExp (s, _)
      | StringKeywordExp (s, _) ->
          Buffer.add_string buf s
      | StringWhiteExp (s, _) ->
          if quoted then
            Buffer.add_string buf s
          else
            next()
      | QuoteExp (el, _) ->
          List.iter (collect_exp true) el
      | QuoteStringExp (c, el, _) ->
          Buffer.add_char buf c;
          List.iter (collect_exp true) el;
          Buffer.add_char buf c;
      | SequenceExp (el, _) ->
          List.iter (collect_exp quoted) el
      | IntExp (_, loc)
      | FloatExp (_, loc)
      | ArrayExp (_, loc)
      | ApplyExp (_, _, _, loc)
      | SuperApplyExp (_, _, _, _, loc)
      | MethodApplyExp (_, _, _, _, loc)
      | BodyExp (_, loc)
      | KeyExp (_, _, loc)
      | CommandExp (_, _, _, loc)
      | VarDefExp (_, _, _, _, loc)
      | VarDefBodyExp (_, _, _, _, loc)
      | KeyDefExp (_, _, _, _, loc)
      | KeyDefBodyExp (_, _, _, _, loc)
      | ObjectDefExp (_, _, _, loc)
      | FunDefExp (_, _, _, loc)
      | RuleExp (_, _, _, _, _, loc)
      | ShellExp (_, loc)
      | CatchExp (_, _, _, loc)
      | ClassExp (_, loc)
      | ExtendsExp (_, loc) ->
          failwith "misplaced expression" in
  collect_exp false e;
  next();
  List.rev !list
