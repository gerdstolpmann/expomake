module Types : sig
  exception Busy
  (** Raised by functions when the last I/O operation is still active *)

  exception Closed
  (** Raised by functions when the handle is already closed *)

  type handle =
    { close : unit -> unit;       (** Close the handle *)
      busy : unit -> bool;        (** Whether the I/O operation is active *)
      cancel : unit -> unit;      (** Cancel the I/O operation *)
      wait : unit -> wait_state   (** Start a wait for the completion of I/O *)
    }
    (** A handle for asynchronous I/O. Generally the idea is as follows:
       - First we need to request the I/O (e.g. [request_read])
       - As a consequence of this the [busy] flag is immediately switching to
         [true], and most functions will raise [Busy] if called. The notable
         exceptions are [close], [busy], [cancel], and [wait]. Under the hood
         anything is set up for running the I/O operation.
       - In user code, we would now typically add the handle to an
         [Async_set.t], and start waiting for the completion of I/O.
         Of course, this module just uses the [wait] function to accomplish
         this.
       - The [wait_state] now indicates with the [descriptors] list which
         file descriptors must become readable or writable. We use now an
         OS function like [Unix.select] to actually perform the wait.
       - Once one of the file descriptors is readable or writable, we call
         [on_io] to tell this the handle implementation. Otherwise, if
         we run into a timeout, we tell this by calling [on_timeout].
         Note that it is possible that we call [on_io] with an empty list
         when it is waited for several handles, and one of the other handles
         has a readable or writable descriptor.
       - By calling either [on_io] the handle gets the
         chance to run any post-waiting code. It then clears the busy flag.
       - The result of the I/O operation can be inspected with the status
         methods
     *)

   and wait_state =
     { descriptors : (wait_tag * Unix.file_descr * direction) list;
       abs_timeout : float;   (** absolute time of timeout. Set to [infinity ] for no timeout *)
       on_io : wait_tag list -> unit;
       on_timeout : float -> unit
     }

   and wait_tag_element = Wait_int of int | Wait_string of string
   and wait_tag = wait_tag_element list
   and direction = Read | Write

end

module Async_read_buffer : sig
  type t

  (* TODO: type operation = Read | Recv | ... *)

  val of_file_descr : Unix.file_descr -> t
  val handle : t -> Types.handle
  val request_read : t -> int -> float -> unit
  val status_error : t -> Unix.error option
  val status_number : t -> int
  val status_eof : t -> bool
  val space : t -> int
  val length : t -> int
  val take : t -> int -> string
  val skip : t -> int -> unit
end

module Async_set : sig
  type 'elt t

  val create : unit -> _ t
  val add : Types.wait_tag -> 'elt -> Types.handle -> 'elt t -> 'elt t
  val get : Types.wait_tag -> 'elt t -> 'elt * Types.handle
  val handle : _ t -> Types.handle
  val wait : _ t -> float -> Types.wait_tag list
  val union : (Types.wait_tag_element * 'elt t) list -> 'elt t
end

module Handle : sig
  type t =
    | H_async_read_buffer of Async_read_buffer.t
    | H_async_set of t Async_set.t

  val handle : t -> Types.handle
end
