exception Busy
exception Closed

type handle =
  { close : unit -> unit;
    busy : unit -> bool;
    cancel : unit -> unit;
    wait : unit -> wait_state
  }

 and wait_state =
   { descriptors : (wait_tag * Unix.file_descr * direction) list;
     abs_timeout : float;
     on_io : wait_tag list -> unit;
     on_timeout : float -> unit;
   }

 and wait_tag_element = Wait_int of int | Wait_string of string
 and wait_tag = wait_tag_element list
 and direction = Read | Write

let dummy_handle =
  { close = (fun () -> assert false);
    busy = (fun () -> assert false);
    cancel = (fun () -> assert false);
    wait = (fun () -> assert false);
  }

