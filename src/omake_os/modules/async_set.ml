open Types

type 'elt t =
  (wait_tag * ('elt * handle)) list

(* TODO: use a polymorphic map implementation instead *)

let create() = []
let add tag elt handle set = (tag, (elt, handle)) :: set
let get tag set = List.assoc tag set

let union sets =
  List.flatten
    (List.map
       (fun (otag, set) ->
         List.map
           (fun (itag, pair) -> ((otag :: itag),pair))
           set
       )
       sets
    )

let handle_wait set () =
  let set = Array.of_list set in
  let wstates =
    Array.map
      (fun (_, (_,h)) ->
        if h.busy() then Some(h.wait()) else None
      )
      set in
  let descrs =
    Array.map
      (fun ws_opt ->
        match ws_opt with
          | Some ws -> ws.descriptors
          | None -> []
      )
      wstates in
  let num_descrs =
    Array.fold_left (fun acc dl -> acc + List.length dl) 0 descrs in
  let fdarray =
    Array.make
      num_descrs
      ((-1), [], [], Unix.stdin, Read) in
  let j = ref 0 in
  Array.iteri
    (fun i da ->
      List.iter
        (fun (orig_w, fd, dir) ->
          fdarray.( !j ) <- (i, [Wait_int (!j)], orig_w, fd, dir);
          incr j
        )
        da
    )
    descrs;
  let descriptors =
    Array.to_list
      (Array.map (fun (_, w, _, fd, dir) -> (w, fd, dir)) fdarray) in
  let abs_timeout =
    Array.fold_left
      (fun acc ws_opt ->
        match ws_opt with
          | Some ws -> min acc ws.abs_timeout
          | None -> acc
      )
      infinity
      wstates in
  { descriptors;
    abs_timeout;
    on_io =
      (fun tags ->
        (* group by set members: *)
        let ht = Hashtbl.create 31 in
        List.iter
          (fun tag ->
            match tag with
              | [Wait_int w] ->
                  assert(w >= 0 && w < Array.length fdarray);
                  let (i, _, orig_w, _, _) = fdarray.(w) in
                  let l = try Hashtbl.find ht i with Not_found -> [] in
                  Hashtbl.replace ht i (orig_w :: l)
              | _ -> assert false
          )
          tags;
        Hashtbl.iter
          (fun i i_tags ->
            let ws =
              match wstates.(i) with
                | None -> assert false
                | Some ws -> ws in
            ws.on_io (List.rev i_tags)
          )
          ht
      );
    on_timeout =
      (fun t1 ->
        Array.iter
          (function
           | None -> ()
           | Some ws ->
               if t1 >= ws.abs_timeout then
                 ws.on_timeout t1
               else
                 ws.on_io []
          )
          wstates
      )
  }

let handle set =
  { close = (fun () -> List.iter (fun (_,(_,h)) -> h.close()) set);
    busy = (fun () -> List.exists (fun (_,(_,h)) -> h.busy()) set);
    cancel = (fun () -> List.iter (fun (_,(_,h)) -> h.cancel()) set);
    wait = handle_wait set;
  }

let wait set tmo =
  let set = Array.of_list set in
  let wstates =
    Array.map
      (fun (_, (_,h)) ->
        if h.busy() then Some(h.wait()) else None
      )
      set in
  let descrs =
    Array.map
      (fun ws_opt ->
        match ws_opt with
          | Some ws -> ws.descriptors
          | None -> []
      )
      wstates in
  let num_descrs =
    Array.fold_left (fun acc dl -> acc + List.length dl) 0 descrs in
  let index =
    Array.make num_descrs ((-1), []) in
  let pa =
    Netsys_posix.create_poll_array num_descrs in
  let j = ref 0 in
  Array.iteri
    (fun k dl ->
      List.iter
        (fun (tag, fd, dir) ->
          let pc =
            { Netsys_posix.poll_fd = fd;
              poll_req_events = (match dir with
                                   | Read ->
                                       Netsys_posix.poll_req_events
                                         true false false
                                   | Write ->
                                       Netsys_posix.poll_req_events
                                         false true false
                                );
              poll_act_events = Netsys_posix.poll_null_events()
            } in
          Netsys_posix.set_poll_cell pa !j pc;
          index.( !j ) <- (k, tag);
          incr j
        )
        dl
    )
    descrs;
  let t0 = Unix.gettimeofday() in
  let abs_tmo = if tmo < 0.0 then infinity else t0 +. tmo in
  let eff_abs_tmo =
    Array.fold_left
      (fun acc ws_opt ->
        match ws_opt with
          | Some ws -> min acc ws.abs_timeout
          | None -> acc
      )
      abs_tmo
      wstates in
  let eff_tmo = max (eff_abs_tmo -. t0) 0.0 in
  let eff_tmo' = if eff_tmo = infinity then -1.0 else eff_tmo in
  let _n = Netsys_posix.restarting_poll pa num_descrs eff_tmo' in
  let t1 = Unix.gettimeofday() in
  let results = Array.map (fun _ -> []) wstates in
  for j = 0 to num_descrs - 1 do
    let k, tag = index.(j) in
    let pc = Netsys_posix.get_poll_cell pa j in
    if Netsys_posix.poll_result Netsys_posix.(pc.poll_act_events) then
      results.(k) <- tag :: results.(k)
  done;
  let finished = ref [] in
  for k = 0 to Array.length wstates - 1 do
    match wstates.(k) with
      | None -> ()
      | Some ws ->
          let tags = List.rev results.(k) in
          if tags <> [] then
            finished := fst set.(k) :: !finished;
          if tags = [] && t1 >= ws.abs_timeout then (
            ws.on_timeout t1;
            finished := fst set.(k) :: !finished;
          ) else
            ws.on_io tags
  done;
  List.rev !finished


