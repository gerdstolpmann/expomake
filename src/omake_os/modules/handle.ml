type t =
  | H_async_read_buffer of Async_read_buffer.t
  | H_async_set of t Async_set.t

let handle =
  function
  | H_async_read_buffer b -> Async_read_buffer.handle b
  | H_async_set s -> Async_set.handle s
