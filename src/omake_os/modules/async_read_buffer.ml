open Types

type state =
  | Working of { number:int;
                 abs_timeout:float;
                 eof : bool;
                 old_state : state;
               }
  | Done of { errno : Unix.error option;
              number:int;
              eof : bool
            }
  | Down

type t =
  { fd : Unix.file_descr;
    buffer : Netpagebuffer.t;
    mutable state : state;
    mutable handle : handle;
  }

let close b () =
  if b.state <> Down then (
    Unix.close b.fd;
    b.state <- Down
  )

let busy b () =
  match b.state with
    | Working _ -> true
    | _ -> false

let cancel b () =
  match b.state with
    | Working { old_state; _ } ->
        b.state <- old_state
    | Done _ ->
        ()
    | Down ->
        raise Closed

let read b =
  match b.state with
    | Working w ->
        ( try
            let mem, pos, len = Netpagebuffer.page_for_additions b.buffer in
            let len = min len w.number in
            let n = Netsys_mem.mem_read b.fd mem pos len in
            let new_state =
              Done { errno = None;
                     number = n;
                     eof = w.eof || (len > 0 && n=0)
                   } in
            b.state <- new_state
          with
            | Unix.Unix_error (errno, _, _) ->
                let new_state =
                  Done { errno = Some errno;
                         number = 0;
                         eof = w.eof
                       } in
                b.state <- new_state
        )
    | _ ->
        assert false

let wait b () =
  match b.state with
    | Working w ->
        { descriptors = [ [Wait_int 0], b.fd, Read ];
          abs_timeout = w.abs_timeout;
          on_io =
            (fun l ->
              if l <> [] then (
                assert(l = [[Wait_int 0]]);
                read b;
              )
            );
          on_timeout =
            (fun _ ->
              let new_state = Done { errno=None; number=0; eof=w.eof } in
              b.state <- new_state
            );
        }
    | Done d ->
        { descriptors = [];
          abs_timeout = infinity;
          on_io =
            (fun l ->
              assert(l = []);
              let new_state = Done { errno=None; number=0; eof=d.eof } in
              b.state <- new_state
            );
          on_timeout =
            (fun _ ->
              let new_state = Done { errno=None; number=0; eof=d.eof } in
              b.state <- new_state
            );
        }
    | Down ->
        raise Closed

let of_file_descr fd =
  let b =
    { fd;
      buffer = Netpagebuffer.create Netsys_mem.default_block_size;
      state = Done { errno = None;
                     number = 0;
                     eof = false
                   };
      handle = dummy_handle;
    } in
  b.handle <- { close = close b;
                busy = busy b;
                cancel = cancel b;
                wait = wait b
              };
  b

let handle b = b.handle

let request_read b number timeout =
  let abs_timeout =
    if timeout < 0.0 then
      infinity
    else
      Unix.gettimeofday() +. timeout in
  match b.state with
    | Working _ ->
        raise Busy
    | Done d ->
        let newstate =
          Working { number; abs_timeout; old_state = b.state; eof = d.eof } in
        b.state <- newstate
    | Down ->
        raise Closed

let status_error b =
  match b.state with
    | Working _ ->
        raise Busy
    | Done { errno; _ } ->
        errno
    | Down ->
        raise Closed

let status_number b =
  match b.state with
    | Working _ ->
        raise Busy
    | Done { number; _ } ->
        number
    | Down ->
        raise Closed

let status_eof b =
  match b.state with
    | Working _ ->
        raise Busy
    | Done { eof; _ } ->
        eof
    | Down ->
        raise Closed

let space b =
  let _, _, s = Netpagebuffer.page_for_additions b.buffer in
  s

let length b =
  Netpagebuffer.length b.buffer

let take b n =
  let s = Netpagebuffer.sub b.buffer 0 n in
  Netpagebuffer.delete_hd b.buffer n;
  s

let skip b n =
  Netpagebuffer.delete_hd b.buffer n

