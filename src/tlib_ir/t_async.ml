(* Asynchrnonous functions in Omake_os *)

open Omake_ir.Types
open Omake_ir.Compiler
open Printf

module Seq = Omake_vm.Seq
module T = Omake_vm.Types
module C = T_compiler

let generator n fd =
  Thread.create
    (fun () ->
      let buf = Bytes.create 1 in
      for i = 1 to n do
        let _ = Unix.select [] [] [] (0.1) in
        Bytes.set buf 0 (Char.chr (48+i));
        let _ = Unix.select [] [fd] [] (-1.0) in
        ignore(Unix.single_write fd buf 0 1);
        ()
      done;
      Unix.close fd
    )
    ()

let test001() =
  let stl =
    [ Value (MonOp {monop=MWait;
                    arg=Apply { var="handle"; vartype=None; named=[]; anon=[] }
            })
    ] in
  let sn, glb = C.compile stl in
  let fd_rd, fd_wr = Unix.pipe() in
  Unix.set_nonblock fd_rd;
  let thr = generator 1 fd_wr in
  let h1 = Omake_os.Async_read_buffer.of_file_descr fd_rd in
  Omake_os.Async_read_buffer.request_read h1 1 infinity;
  let h2 = Omake_os.Handle.H_async_read_buffer h1 in
  let v1 = T.valuebox T.R_handle h2 in
  let s_handle = Omake_vm.Global.add_variable_slot "handle" glb in
  let glb = Omake_vm.Global.add_variable s_handle v1 glb in
  let v2 = C.run (sn, glb) in
  Thread.join thr;
  Unix.close fd_rd;
  let a2 = T.array_of_vbox C.cfg v2 in
  assert(Seq.length a2 = 1);
  assert(T.string_of_vbox C.cfg (Seq.get a2 0) = "READY");
  true


let test002() =
  let stl =
    [ Let { var="f"; vartype=Some P;
            expr=Abstract
                   { named=[]; anon=["dummy"]; arity_min=1;
                     autocurry=false;
                     body={C.empty_block with
                            st=[ Value (MonOp {monop=MWait;
                                               arg=Apply { var="ohandle";
                                                           vartype=None;
                                                           named=[]; anon=[] }
                                       })
                               ]
                          }
                   }
          };
      Effect (Thread { var="f"; vartype=None; named=[];
                       anon=[ Literal(T.v_int 0) ]
                     });
      Value (MonOp {monop=MWait;
                    arg=Apply { var="handle"; vartype=None; named=[]; anon=[] }
            })
    ] in
  let sn, glb = C.compile stl in

  let fd_rd_1, fd_wr_1 = Unix.pipe() in
  Unix.set_nonblock fd_rd_1;
  let thr_1 = generator 1 fd_wr_1 in
  let h1_1 = Omake_os.Async_read_buffer.of_file_descr fd_rd_1 in
  Omake_os.Async_read_buffer.request_read h1_1 1 infinity;
  let h2_1 = Omake_os.Handle.H_async_read_buffer h1_1 in
  let v1 = T.valuebox T.R_handle h2_1 in

  let fd_rd_2, fd_wr_2 = Unix.pipe() in
  Unix.set_nonblock fd_rd_2;
  let thr_2 = generator 1 fd_wr_2 in
  let h1_2 = Omake_os.Async_read_buffer.of_file_descr fd_rd_2 in
  Omake_os.Async_read_buffer.request_read h1_2 1 infinity;
  let h2_2 = Omake_os.Handle.H_async_read_buffer h1_2 in
  let v2 = T.valuebox T.R_handle h2_2 in

  let s_handle = Omake_vm.Global.add_variable_slot "handle" glb in
  let s_ohandle = Omake_vm.Global.add_variable_slot "ohandle" glb in
  let glb = Omake_vm.Global.add_variable s_handle v1 glb in
  let glb = Omake_vm.Global.add_variable s_ohandle v2 glb in

  let v3 = C.run (sn, glb) in
  Thread.join thr_1;
  Thread.join thr_2;
  Unix.close fd_rd_1;
  Unix.close fd_rd_2;

  let a3 = T.array_of_vbox C.cfg v3 in
  assert(Seq.length a3 = 1);
  assert(T.string_of_vbox C.cfg (Seq.get a3 0) = "READY");
  true


let tests =
  [ "test001", test001;
    "test002", test002;
  ]





