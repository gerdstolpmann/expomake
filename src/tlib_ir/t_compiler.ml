open Omake_ir.Types
open Omake_ir.Compiler
open Printf

module T = Omake_vm.Types

let cfg = Omake_vm.Config.standard_config

let compile stlist =
  let alloc = T.create_alloc() in
  let sh = Omake_vm.Shared.create alloc in
  let glb = Omake_vm.Global.create sh in
  let sn = create_snippet glb in
  compile_snippet sn stlist;
  (sn, glb)

let run (sn, glb) =
  run_snippet glb sn;
  value_of_snippet sn

let cycle stlist =
  run (compile stlist)

(*
module L = Omake_vm.Lookup_table;;
let Some clos0 = L.get 0 glb.shared.shared_functions;;
Omake_vm.Vm.instructions clos0.clos_code;;
 *)

(* Variables, scopes, exports *)

let private_x = Apply { var="x"; vartype=Some P; named=[]; anon=[]}
let private_y = Apply { var="y"; vartype=Some P; named=[]; anon=[]}
let private_z = Apply { var="z"; vartype=Some P; named=[]; anon=[]}
let global_x = Apply { var="x"; vartype=Some G; named=[]; anon=[]}
let global_y = Apply { var="y"; vartype=Some G; named=[]; anon=[]}
let global_z = Apply { var="z"; vartype=Some G; named=[]; anon=[]}
let any_x = Apply { var="x"; vartype=None; named=[]; anon=[]}
let any_y = Apply { var="y"; vartype=None; named=[]; anon=[]}
let any_z = Apply { var="z"; vartype=None; named=[]; anon=[]}
let local_this = Apply { var="this"; vartype=Some L; named=[]; anon=[]}

let empty_block =
  { export = []; export_glb = false; export_obj = false; st = [] }

                       
let test001() =
  let stl =
    [ Let { var="x"; vartype=Some P; expr=Literal (T.v_int 42) };
      Value (BinOp { binop=BAdd;
                     arg1=private_x;
                     arg2=Literal (T.v_int 5)
            })
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 47

let test002() =
  let stl =
    [ Let { var="x"; vartype=Some P; expr=Literal (T.v_int 42) };
      Let { var="x"; vartype=Some P;
            expr= (BinOp { binop=BAdd;
                           arg1=private_x;
                           arg2=Literal (T.v_int 5)
                  })
             };
      Value private_x;
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 47

let test003() =
  let stl =
    [ Let { var="x"; vartype=Some P; expr=Literal (T.v_int 42) };
      Section
        { empty_block with
          st = [ Let { var="x"; vartype=Some P;
                       expr= (BinOp { binop=BAdd;
                                      arg1=private_x;
                                      arg2=Literal (T.v_int 5)
                             })
               }];
        };
      Value private_x;
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 42

let test004() =
  let stl =
    [ Let { var="x"; vartype=Some P; expr=Literal (T.v_int 42) };
      Section
        { empty_block with
          export = ["x", None];
          st = [ Let { var="x"; vartype=Some P;
                       expr= (BinOp { binop=BAdd;
                                      arg1=private_x;
                                      arg2=Literal (T.v_int 5)
                             })
               }];
        };
      Value private_x;
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 47

let test005() =
  let stl =
    [ Let { var="x"; vartype=Some P; expr=Literal (T.v_int 42) };
      Section
        { empty_block with
          export = ["x", None];
          st = [ Let { var="y"; vartype=Some G; expr=Literal (T.v_int 1) };
                 Section
                   { empty_block with
                     export = [ "x", None ];
                     st = [ Let { var="x"; vartype=Some P;
                                  expr= (BinOp { binop=BAdd;
                                                 arg1=private_x;
                                                 arg2=Literal (T.v_int 5)
                                        })
                          }]
               }];
        };
      Value private_x;
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 47

let test011() =
  let stl =
    [ Let { var="x"; vartype=Some G; expr=Literal (T.v_int 42) };
      Value (BinOp { binop=BAdd;
                     arg1=global_x;
                     arg2=Literal (T.v_int 5)
            })
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 47

let test012() =
  let stl =
    [ Let { var="x"; vartype=Some G; expr=Literal (T.v_int 42) };
      Let { var="x"; vartype=Some G;
            expr= (BinOp { binop=BAdd;
                           arg1=global_x;
                           arg2=Literal (T.v_int 5)
                  })
          };
      Value global_x;
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 47

let test013() =
  let stl =
    [ Let { var="x"; vartype=Some G; expr=Literal (T.v_int 42) };
      Section
        { empty_block with
          st = [ Let { var="x"; vartype=Some G;
                       expr= (BinOp { binop=BAdd;
                                      arg1=global_x;
                                      arg2=Literal (T.v_int 5)
                             })
               }];
        };
      Value global_x;
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 42

let test014() =
  let stl =
    [ Let { var="x"; vartype=Some G; expr=Literal (T.v_int 42) };
      Section
        { empty_block with
          export = ["x", None];
          st = [ Let { var="x"; vartype=Some G;
                       expr= (BinOp { binop=BAdd;
                                      arg1=global_x;
                                      arg2=Literal (T.v_int 5)
                             })
               }];
        };
      Value global_x;
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 47

let test015() =
  let stl =
    [ Let { var="x"; vartype=Some G; expr=Literal (T.v_int 42) };
      Section
        { empty_block with
          export = ["x", None];
          st = [ Let { var="y"; vartype=Some P; expr=Literal (T.v_int 1) };
                 Section
                   { empty_block with
                     export = [ "x", None ];
                     st = [ Let { var="x"; vartype=Some G;
                                  expr= (BinOp { binop=BAdd;
                                                 arg1=global_x;
                                                 arg2=Literal (T.v_int 5)
                                        })
                          }]
               }];
        };
      Value global_x;
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 47

let test021() =
  let stl =
    [ Let { var="x"; vartype=Some G; expr=Literal (T.v_int 42) };
      Let { var="x"; vartype=Some P; expr=Literal (T.v_int 52) };
      Section
        { empty_block with
          export = ["x", None];  (* this is a private export! *)
          st = [ Let { var="x"; vartype=Some P; expr=Literal (T.v_int 1) };
                 Section
                   { empty_block with
                     export = [ "x", None ];
                     st = [ Let { var="x"; vartype=Some G;
                                  expr= (BinOp { binop=BAdd;
                                                 arg1=global_x;
                                                 arg2=Literal (T.v_int 5)
                                        })
                          }]
               }];
        };
      Value private_x;
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 1

(* exports from blocks in expressions *)

let test031() =
  let stl =
    [ Let { var="x"; vartype=Some P; expr=Literal (T.v_int 42) };
      Let { var="y"; vartype=Some P; expr=Literal (T.v_int 32) };
      Let { var="z"; vartype=Some P;
            expr=BinOp { binop=BAdd;
                         arg1=private_y;
                         arg2=Block
                                { empty_block with
                                  export = [ "x", None ];
                                  st = [ Let { var="x";
                                               vartype=Some P;
                                               expr=Literal (T.v_int 6);
                                             };
                                         Value (Literal (T.v_int 7));
                                       ]
                                }
                       }
             };
      Value (BinOp { binop=BMul; arg1=private_x; arg2=private_z });
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = (39 * 6)

let test032() =
  let stl =
    [ Let { var="x"; vartype=Some P; expr=Literal (T.v_int 42) };
      Let { var="y"; vartype=Some P; expr=Literal (T.v_int 32) };
      Let { var="z"; vartype=Some P; expr=Literal (T.v_int 0) };
      Section
        { empty_block with
          export = [ "x", None; "z", None ];
          st = [ Let { var="z"; vartype=Some P;
                       expr=BinOp { binop=BAdd;
                                    arg1=private_y;
                                    arg2=Block
                                           { empty_block with
                                             export = [ "x", None ];
                                             st = [ Let { var="x";
                                                          vartype=Some P;
                                                          expr=Literal (T.v_int 6);
                                                        };
                                                    Value (Literal (T.v_int 7));
                                                  ]
                                           }
                                  }
                     };
               ]
        };
      Value (BinOp { binop=BMul; arg1=private_x; arg2=any_z });
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = (39 * 6)

let test033() =
  let stl =
    [ Let { var="x"; vartype=Some G; expr=Literal (T.v_int 42) };
      Let { var="y"; vartype=Some G; expr=Literal (T.v_int 32) };
      Let { var="z"; vartype=Some G;
            expr=BinOp { binop=BAdd;
                         arg1=global_y;
                         arg2=Block
                                { empty_block with
                                  export = [ "x", None ];
                                  st = [ Let { var="x";
                                               vartype=Some G;
                                               expr=Literal (T.v_int 6);
                                             };
                                         Value (Literal (T.v_int 7));
                                       ]
                                }
                       }
          };
      Value (BinOp { binop=BMul; arg1=global_x; arg2=global_z });
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = (39 * 6)

let test034() =
  let stl =
    [ Let { var="x"; vartype=Some G; expr=Literal (T.v_int 42) };
      Let { var="y"; vartype=Some G; expr=Literal (T.v_int 32) };
      Let { var="z"; vartype=Some G; expr=Literal (T.v_int 0) };
      Section
        { empty_block with
          export = [ "x", None; "z", None ];
          st = [ Let { var="z"; vartype=Some G;
                       expr=BinOp { binop=BAdd;
                                    arg1=global_y;
                                    arg2=Block
                                           { empty_block with
                                             export = [ "x", None ];
                                             st = [ Let { var="x";
                                                          vartype=Some G;
                                                          expr=Literal (T.v_int 6);
                                                        };
                                                    Value (Literal (T.v_int 7));
                                                  ]
                                           }
                                  }
                     };
               ]
        };
      Value (BinOp { binop=BMul; arg1=global_x; arg2=any_z });
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = (39 * 6)

let test035() = (* "if" + "export" *)
  let stl =
    [ Let { var="x"; vartype=Some P; expr=Literal (T.v_int 42) };
      Let { var="y"; vartype=Some P; expr=Literal (T.v_int 32) };
      Let { var="z"; vartype=Some P; expr=Literal (T.v_true) };
      Section
        { empty_block with
          export = [ "x", None; "y", None ];
          st=[ Value (If { cond=private_z;
                           ontrue=Block { empty_block with
                                          export=[ "x", None ];
                                          st=[Let { var="x"; vartype=None;
                                                    expr=Literal(T.v_int 65)
                                                  }
                                             ]
                                        };
                           onfalse=Block { empty_block with
                                           export=[ "y", None ];
                                           st=[Let { var="y"; vartype=None;
                                                     expr=Literal(T.v_int 66)
                                                   };
                                              ]
                                         }
                     });
             ]
        };
      Value(BinOp { binop=BMul; arg1=any_x; arg2=any_y });
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 65 * 32


(* functions w/ named args and full anon args *)

let f_xsquareplusy_anon =
  Abstract
    { named=[]; anon=["x"; "y"]; arity_min=2;
      autocurry=false;
      body={ empty_block with
             st=[ Value(BinOp
                          {binop=BAdd;
                           arg1=BinOp
                                  { binop=BMul;
                                    arg1=any_x;
                                    arg2=any_x
                                  };
                           arg2=any_y
                       })
                ]
           }
    }

let f_xsquareplusy_named =
  Abstract
    { named=["x"; "y"]; anon=[]; arity_min=0;
      autocurry=false;
      body={ empty_block with
             st=[ Value(BinOp
                          {binop=BAdd;
                           arg1=BinOp
                                  { binop=BMul;
                                    arg1=any_x;
                                    arg2=any_x
                                  };
                           arg2=any_y
                       })
                ]
           }
    }

let f_xsquareplusy_mixed =
  Abstract
    { named=["x"]; anon=["y"]; arity_min=1;
      autocurry=false;
      body={ empty_block with
             st=[ Value(BinOp
                          {binop=BAdd;
                           arg1=BinOp
                                  { binop=BMul;
                                    arg1=any_x;
                                    arg2=any_x
                                  };
                           arg2=any_y
                       })
                ]
           }
    }
    
let test041() =
  let stl =
    [ Let
        { var="f";
          vartype=Some P;
          expr=f_xsquareplusy_anon;
        };
      Value(Apply{var="f";
                  vartype=None;
                  named=[];
                  anon=[ Literal(T.v_int 5); Literal(T.v_int 6) ]
                 })
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 31

let test042() =
  let stl =
    [ Let
        { var="f";
          vartype=Some P;
          expr=f_xsquareplusy_named;
        };
      Value(Apply{var="f";
                  vartype=None;
                  named=[ "y", Literal(T.v_int 6);
                          "x", Literal(T.v_int 5)
                        ];
                  anon = [];
                 })
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 31

let test043() =
  let stl =
    [ Let
        { var="f";
          vartype=Some P;
          expr=f_xsquareplusy_mixed;
        };
      Value(Apply{var="f";
                  vartype=None;
                  named=[ "x", Literal(T.v_int 5)
                        ];
                  anon = [ Literal(T.v_int 6) ];
                 })
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 31

let f_call2xy =
  Abstract
    { named=["f"]; anon=["x"; "y"]; arity_min=2;
      autocurry=false;
      body={ empty_block with
             st=[ Value(Apply{var="f";
                              vartype=Some P;
                              named=[];
                              anon=[ BinOp{binop=BMul;
                                           arg1=Literal(T.v_int 2);
                                           arg2=private_x};
                                     BinOp{binop=BMul;
                                           arg1=Literal(T.v_int 2);
                                           arg2=private_y};
                       ]})
           ]}
    }

let test044() =
  let stl =
    [ Let
        { var="f";
          vartype=Some P;
          expr=f_call2xy;
        };
      Value(Apply{var="f";
                  vartype=None;
                  named=[ "f", f_xsquareplusy_anon];
                  anon=[ Literal(T.v_int 5); Literal(T.v_int 6) ]
                 })
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 112
    
(* exports from function bodies *)

let f_setx =
  Abstract
    { named=[]; anon=["dummy"]; arity_min=1;
      autocurry=false;
      body={ empty_block with
             export=[ "x", None ];
             st=[ Let { var="x"; vartype=None;
                        expr=Literal(T.v_int 65)
                      }
                ]
           }
    }

let test051() =
  let stl =
    [ Let { var="x"; vartype=Some P; expr=Literal(T.v_int 2) };
      Let { var="f"; vartype=Some P; expr=f_setx };
      Value (Apply {var="f"; vartype=None;
                    named=[]; anon=[ Literal(T.v_int 0) ] } );
      Value private_x
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 65

let test052() =
  let stl =
    [ Let { var="x"; vartype=Some G; expr=Literal(T.v_int 2) };
      Let { var="f"; vartype=Some P; expr=f_setx };
      Value (Apply {var="f"; vartype=None;
                    named=[]; anon=[ Literal(T.v_int 0) ] } );
      Value global_x
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 65
                      
let f_setx_cond = (* conditional export *)
  Abstract
    { named=[]; anon=["y"]; arity_min=1;
      autocurry=false;
      body={ empty_block with
             export=[ "x", None ];
             st=[ Value (If { cond=private_y;
                              ontrue=Block { empty_block with
                                             export=[ "x", None ];
                                             st=[Let { var="x"; vartype=None;
                                                       expr=Literal(T.v_int 65)
                                                     }
                                                ]
                                           };
                              onfalse=Literal (T.v_int 0)
                        })
                ]
           }
    }

let test053() =
  let stl =
    [ Let { var="x"; vartype=Some P; expr=Literal(T.v_int 2) };
      Let { var="f"; vartype=Some P; expr=f_setx_cond };
      Value (Apply {var="f"; vartype=None;
                    named=[]; anon=[ Literal(T.v_true) ] } );
      Value private_x
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 65

let test054() =
  let stl =
    [ Let { var="x"; vartype=Some P; expr=Literal(T.v_int 2) };
      Let { var="f"; vartype=Some P; expr=f_setx_cond };
      Value (Apply {var="f"; vartype=None;
                    named=[]; anon=[ Literal(T.v_false) ] } );
      Value private_x
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 2

let test055() =
  let stl =
    [ Let { var="x"; vartype=Some G; expr=Literal(T.v_int 2) };
      Let { var="f"; vartype=Some P; expr=f_setx_cond };
      Value (Apply {var="f"; vartype=None;
                    named=[]; anon=[ Literal(T.v_true) ] } );
      Value global_x
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 65

let test056() =
  let stl =
    [ Let { var="x"; vartype=Some G; expr=Literal(T.v_int 2) };
      Let { var="f"; vartype=Some P; expr=f_setx_cond };
      Value (Apply {var="f"; vartype=None;
                    named=[]; anon=[ Literal(T.v_false) ] } );
      Value global_x
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 2
                      

(* returning from functions at unusual places *)

let f_setx_return = (* conditional export with return *)
  Abstract
    { named=[]; anon=["y"]; arity_min=1;
      autocurry=false;
      body={ empty_block with
             export=[ "x", None ];
             st=[ Value (If { cond=private_y;
                              ontrue=Block { empty_block with
                                             export=[ "x", None ];
                                             st=[Let { var="x"; vartype=None;
                                                       expr=Literal(T.v_int 65)
                                                     }
                                                ]
                                           };
                              onfalse=Block { empty_block with
                                              export=[ "x", None ];
                                              st=[Let { var="x"; vartype=None;
                                                        expr=Literal(T.v_int 66)
                                                      };
                                                  Return
                                                 ]
                                            }
                        });
                  Let { var="x"; vartype=None;
                        expr=Literal(T.v_int 67)
                      }
                ]
           }
    }

let test061() =
  let stl =
    [ Let { var="x"; vartype=Some P; expr=Literal(T.v_int 2) };
      Let { var="f"; vartype=Some P; expr=f_setx_return };
      Value (Apply {var="f"; vartype=None;
                    named=[]; anon=[ Literal(T.v_true) ] } );
      Value private_x
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 67

let test062() =
  let stl =
    [ Let { var="x"; vartype=Some P; expr=Literal(T.v_int 2) };
      Let { var="f"; vartype=Some P; expr=f_setx_return };
      Value (Apply {var="f"; vartype=None;
                    named=[]; anon=[ Literal(T.v_false) ] } );
      Value private_x
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 66

(* setting default args of functions *)

let f_xsquareplusy_default =
  Abstract
    { named=["x"; "y"]; anon=[]; arity_min=0;
      autocurry=false;
      body={ empty_block with
             st=[ Effect(If{cond=IsUninitialized private_x;
                            ontrue=Block {
                                       empty_block with
                                       export=[ "x", None ];
                                       st=[ Let { var="x";
                                                  vartype=Some P;
                                                  expr=Literal(T.v_int 8);
                                          }]
                                     };
                            onfalse=Literal(T.v_false)
                        });
                  Value(BinOp
                          {binop=BAdd;
                           arg1=BinOp
                                  { binop=BMul;
                                    arg1=any_x;
                                    arg2=any_x
                                  };
                           arg2=any_y
                       })
                ]
           }
    }

let test071() =
  let stl =
    [ Let { var="y"; vartype=Some P; expr=Literal(T.v_int 2) };
      Let { var="f"; vartype=Some P; expr=f_xsquareplusy_default };
      Value (Apply {var="f"; vartype=None;
                    named=[ "y", Literal(T.v_int 2) ]; anon=[] } );
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 66

let test072() =
  let stl =
    [ Let { var="y"; vartype=Some P; expr=Literal(T.v_int 2) };
      Let { var="f"; vartype=Some P; expr=f_xsquareplusy_default };
      Value (Apply {var="f"; vartype=None;
                    named=[ "x", Literal(T.v_int 3);
                            "y", Literal(T.v_int 2);
                          ]; anon=[] } );
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 11


(* functions catching environments *)

let f_xsquareplusy_env =
  Abstract
    { named=[]; anon=["dummy"]; arity_min=1;
      autocurry=false;
      body={ empty_block with
             st=[ Value(BinOp
                          {binop=BAdd;
                           arg1=BinOp
                                  { binop=BMul;
                                    arg1=any_x;
                                    arg2=any_x
                                  };
                           arg2=any_y
                       })
                ]
           }
    }

let test081() =
  let stl =
    [ Let { var="f";
            vartype=Some P;
            expr =
              Block {
                  empty_block with
                  st = [ Let { var="x"; vartype=Some P; expr=Literal(T.v_int 2) };
                         Let { var="y"; vartype=Some P; expr=Literal(T.v_int 9) };
                         Value f_xsquareplusy_env
                       ];
                }
          };
      Let { var="x"; vartype=Some P; expr=Literal(T.v_int 7) };
      Let { var="y"; vartype=Some P; expr=Literal(T.v_int 7) };
      Value (Apply {var="f"; vartype=None;
                    named=[]; anon=[ Literal T.v_false ] } );
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 13

let test082() =  (* similar to 81 but with globals it doesn't work! *)
  let stl =
    [ Let { var="f";
            vartype=Some P;
            expr =
              Block {
                  empty_block with
                  st = [ Let { var="x"; vartype=Some G; expr=Literal(T.v_int 2) };
                         Let { var="y"; vartype=Some G; expr=Literal(T.v_int 9) };
                         Value f_xsquareplusy_env
                       ];
                }
          };
      Let { var="x"; vartype=Some G; expr=Literal(T.v_int 7) };
      Let { var="y"; vartype=Some G; expr=Literal(T.v_int 7) };
      Value (Apply {var="f"; vartype=None;
                    named=[]; anon=[ Literal T.v_false ] } );
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 56

(* uninitialized globals *)

let test091() =
  let stl =
    [ Value (Defined global_x) ] in
  try
    let _v = cycle stl in
    false
  with
      (* | T.Undefined_value -> *)
    | Failure "uncaught VM exception" ->    (* FIXME *)
        true

(* forward private variables; recursive private functions *)

let fib =
  Abstract
    { named=[]; anon=["x"]; arity_min=1;
      autocurry=false;
      body={ empty_block with
             st=[ Value(If{cond=BinOp{binop=BLe;
                                      arg1=private_x;
                                      arg2=Literal(T.v_int 2);
                                     };
                           ontrue=Literal(T.v_int 1);
                           onfalse=BinOp{binop=BAdd;
                                         arg1=Apply{var="fib";
                                                    vartype=None;
                                                    named=[];
                                                    anon=[BinOp{binop=BSub;
                                                                arg1=private_x;
                                                                arg2=Literal(T.v_int 2)
                                                               }
                                                         ]
                                                   };
                                         arg2=Apply{var="fib";
                                                    vartype=None;
                                                    named=[];
                                                    anon=[BinOp{binop=BSub;
                                                                arg1=private_x;
                                                                arg2=Literal(T.v_int 1)
                                                               }
                                                         ]
                                                   };
                                        }
                       })
                ]
           }
    }

let test101() =
  (* this works with global vars only! *)
  let stl =
    [ Let { var="fib"; vartype=Some G; expr=fib };
      Value ( Apply{var="fib";
                    vartype=None;
                    named=[];
                    anon=[Literal(T.v_int 7)]
                   }
            )
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 13

let recursion_combinator =
  Let { var="reccomb"; vartype=Some G;
        expr=Abstract
               { named=[]; anon=["f"]; arity_min=1;
                 autocurry=false;
                 body={ empty_block with
                        st=[ Value(Apply
                                     { var="f";
                                       vartype=Some P;
                                       named=[];
                                       anon=[ Lazy(Apply
                                                     { var="reccomb";
                                                       vartype=Some G;
                                                       named=[];
                                                       anon=[ Apply{var="f";
                                                                    vartype=Some P;
                                                                    named=[];
                                                                    anon=[]
                                                                   }
                                                            ]
                                            })]
                                  })
                           ]
                      }
               }
      }

let fib_wrapper =
  Abstract
    { named=[]; anon=["fib"]; arity_min=1;
      autocurry=false;
      body={ empty_block with
             st=[ Value fib ]
           }
    }

let test102() =
  (* the workaround for recursive private functions *)
  let stl =
    [ recursion_combinator;
      Let { var="fib"; vartype=Some P;
            expr= Apply{var="reccomb";
                        vartype=Some G;
                        named=[];
                        anon=[ fib_wrapper ]
                       }
          };
      Value ( Apply{var="fib";
                    vartype=None;
                    named=[];
                    anon=[Literal(T.v_int 7)]
                   }
            )
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 13

(* auto currying *)

let auto1 =
  Let { var="auto1";
        vartype=Some P;
        expr=Abstract { named=[]; anon=["x"; "y"]; arity_min=2;
                        autocurry=true;
                        body={ empty_block with
                               st=[ Value(BinOp{binop=BAdd;
                                                arg1=private_x;
                                                arg2=private_y
                                               }
                                         )
                                  ]
                             }
                      }
      }

let test111() =
  let st =
    [ auto1;
      Let { var="z";
            vartype=Some P;
            expr=Apply { var="auto1";
                         vartype=None;
                         named=[];
                         anon=[Literal(T.v_int 9)]
                       }
          };
      Value(Apply { var="z";
                    vartype=None;
                    named=[];
                    anon=[Literal(T.v_int 13)]
           })
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 22

let auto2 =
  Let { var="auto2";
        vartype=Some P;
        expr=Abstract
               { named=[]; anon=["x"]; arity_min=1;
                 autocurry=true;
                 body={ empty_block with
                        st=[ Value(Abstract
                                     { named=[];
                                       anon=["y"];
                                       arity_min=1;
                                       autocurry=true;
                                       body={ empty_block with
                                              st=[ Value(
                                                       BinOp{binop=BAdd;
                                                             arg1=private_x;
                                                             arg2=private_y
                                                            }
                                                     )
                                                 ]
                                            }
                                  })
                           ]
                      }
               }
      }

let test112() =
  let st =
    [ auto2;
      Value(Apply { var="auto2";
                    vartype=None;
                    named=[];
                    anon=[Literal(T.v_int 54); Literal(T.v_int 13)]
           })
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 67

let f_xyz =
  BinOp{binop=BMul;
        arg1=BinOp{binop=BAdd;
                   arg1=private_x;
                   arg2=private_y
                  };
        arg2=private_z
       }

let auto3 =
  Let
    { var="auto3";
      vartype=Some P;
      expr=Abstract
             { named=[]; anon=["x"]; arity_min=1;
               autocurry=true;
               body={ empty_block with
                      st=[ Value
                             (Abstract
                                { named=[];
                                  anon=["y"; "z"];
                                  arity_min=2;
                                  autocurry=true;
                                  body={ empty_block with
                                         st=[ Value f_xyz ]
                                       }
                                }
                             )
                         ]
                    }
             }
    }

let test113() =
  let st =
    [ auto3;
      Let { var="p";
            vartype=Some P;
            expr=Apply { var="auto3";
                         vartype=None;
                         named=[];
                         anon=[Literal(T.v_int 2); Literal(T.v_int 13)]
                       }
          };
      Value(Apply { var="p";
                    vartype=None;
                    named=[];
                    anon=[Literal(T.v_int 5)]
           })
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = (2+13)*5
    
let auto4 =
  Let
    { var="auto4";
      vartype=Some P;
      expr=Abstract
             { named=[]; anon=["x"]; arity_min=1;
               autocurry=true;
               body={ empty_block with
                      st=[ Value
                             (Abstract
                                { named=[];
                                  anon=["y"];
                                  arity_min=1;
                                  autocurry=true;
                                  body={ empty_block with
                                         st=[  Value
                                                 (Abstract
                                                    { named=[];
                                                      anon=["z"];
                                                      arity_min=1;
                                                      autocurry=true;
                                                      body={ empty_block with
                                                             st=[ Value f_xyz ]
                                                           }
                                                    })
                                            ]
                                       }
                                }
                             )
                         ]
                    }
             }
    }

let test114() =
  let st =
    [ auto4;
      Value(Apply { var="auto4";
                    vartype=None;
                    named=[];
                    anon=[Literal(T.v_int 2); Literal(T.v_int 14);
                          Literal(T.v_int 5)]
           })
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = (2+14)*5

(* lazy evaluation *)

let test121() =
  let st =
    [ Let { var="x"; vartype=Some G;
            expr=Lazy( BinOp {binop=BAdd;
                              arg1=global_y;
                              arg2=global_z
                     })
          };
      Let { var="y"; vartype=Some G; expr=Literal(T.v_int 34) };
      Let { var="z"; vartype=Some G; expr=Literal(T.v_int 6) };
      Value ( BinOp {binop=BMul;
                     arg1=global_x;
                     arg2=Literal(T.v_int 2)
            })
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = (34+6)*2

let test122() =
  let st =
    [ Let { var="x"; vartype=Some G;
            expr=Lazy( Lazy (BinOp {binop=BAdd;
                                    arg1=global_y;
                                    arg2=global_z
                     }))
          };
      Let { var="y"; vartype=Some G; expr=Literal(T.v_int 34) };
      Let { var="z"; vartype=Some G; expr=Literal(T.v_int 6) };
      Value ( BinOp {binop=BMul;
                     arg1=global_x;
                     arg2=Literal(T.v_int 2)
            })
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = (34+6)*2
                             

(* try/catch in local context *)

let test131() =
  let st =
    [ Let { var="x"; vartype=Some P; expr=Literal(T.v_int 6) };
      Value (Try { trial=Raise(Literal(T.v_int 5));
                   catch=Block { empty_block with
                                 export=["x", None];
                                 st=[ Let { var="x"; vartype=Some P;
                                            expr=CaughtValue
                                          }
                                    ]
                               };
                   finally=Literal(T.v_false)
                 }
            );
      Value private_x;
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 5

let test132() =
  let st =
    [ Let { var="x"; vartype=Some P; expr=Literal(T.v_int 6) };
      Value (Try {trial=Try { trial=Raise(Literal(T.v_int 5));
                              catch=Block { empty_block with
                                            export=["x", None];
                                            st=[ Let { var="x";
                                                       vartype=Some P;
                                                       expr=CaughtValue
                                                     }
                                               ]
                                          };
                              finally=Literal(T.v_false)
                            };
                  catch=Block { empty_block with
                                export=["x",None];
                                st=[Let{var="x"; vartype=Some P;
                                        expr=Literal(T.v_int 9)
                                       }
                                   ]
                              };
                  finally=Literal(T.v_false)
                 }
            );
      Value private_x;
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 5

let test133() =
  let st =
    [ Let { var="x"; vartype=Some P; expr=Literal(T.v_int 6) };
      Value (Try {trial=Try { trial=Raise(Literal(T.v_int 5));
                              catch=Block
                                      { empty_block with
                                        export=["x", None];
                                        st=[ Effect(Raise(Literal(T.v_int 19)))
                                           ]
                                      };
                              finally=Literal(T.v_false)
                            };
                  catch=Block { empty_block with
                                export=["x",None];
                                st=[Let{var="x"; vartype=Some P;
                                        expr=Literal(T.v_int 9)
                                       }
                                   ]
                              };
                  finally=Literal(T.v_false)
                 }
            );
      Value private_x;
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 9
                      
(* try/catch between functions *)

let fail =
  Let { var="fail"; vartype=Some P;
        expr=Abstract
               { named=[]; anon=["x"]; arity_min=1; autocurry=false;
                 body={ empty_block with
                        st=[ Effect(Raise private_x) ]
                      }
               }
      }

let fail_with arg =
  Apply { var="fail"; vartype=Some P;
          named=[]; anon=[arg]
        }

let test141() =
  let st =
    [ fail;
      Let { var="x"; vartype=Some P; expr=Literal(T.v_int 6) };
      Value (Try { trial=fail_with(Literal(T.v_int 5));
                   catch=Block { empty_block with
                                 export=["x", None];
                                 st=[ Let { var="x"; vartype=Some P;
                                            expr=CaughtValue
                                          }
                                    ]
                               };
                   finally=Literal(T.v_false)
                 }
            );
      Value private_x;
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 5

let test142() =
  let st =
    [ fail;
      Let { var="x"; vartype=Some P; expr=Literal(T.v_int 6) };
      Value (Try {trial=Try { trial=fail_with(Literal(T.v_int 5));
                              catch=Block { empty_block with
                                            export=["x", None];
                                            st=[ Let { var="x";
                                                       vartype=Some P;
                                                       expr=CaughtValue
                                                     }
                                               ]
                                          };
                              finally=Literal(T.v_false)
                            };
                  catch=Block { empty_block with
                                export=["x",None];
                                st=[Let{var="x"; vartype=Some P;
                                        expr=Literal(T.v_int 9)
                                       }
                                   ]
                              };
                  finally=Literal(T.v_false)
                 }
            );
      Value private_x;
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 5

let test143() =
  let st =
    [ fail;
      Let { var="x"; vartype=Some P; expr=Literal(T.v_int 6) };
      Value (Try {trial=Try { trial=fail_with(Literal(T.v_int 5));
                              catch=Block
                                      { empty_block with
                                        export=["x", None];
                                        st=[ Effect(fail_with(Literal(T.v_int 19)))
                                           ]
                                      };
                              finally=Literal(T.v_false)
                            };
                  catch=Block { empty_block with
                                export=["x",None];
                                st=[Let{var="x"; vartype=Some P;
                                        expr=Literal(T.v_int 9)
                                       }
                                   ]
                              };
                  finally=Literal(T.v_false)
                 }
            );
      Value private_x;
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 9

(* try/catch/finally *)

let test151() =
  let st =
    [ Let { var="x"; vartype=Some P; expr=Literal(T.v_int 6) };
      Value (Try { trial=Raise(Literal(T.v_int 5));
                   catch=Block { empty_block with
                                 export=["x", None];
                                 st=[ Let { var="x"; vartype=Some P;
                                            expr=CaughtValue
                                          }
                                    ]
                               };
                   finally=Block { empty_block with
                                   export=["x", None];
                                   st=[ Let { var="x"; vartype=Some P;
                                              expr=Literal(T.v_int 18)
                                            }
                                      ]
                                 };
                 }
            );
      Value private_x;
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 18

let test152() =
  let st =
    [ fail;
      Let { var="x"; vartype=Some P; expr=Literal(T.v_int 6) };
      Value (Try { trial=fail_with(Literal(T.v_int 5));
                   catch=Block { empty_block with
                                 export=["x", None];
                                 st=[ Let { var="x"; vartype=Some P;
                                            expr=CaughtValue
                                             }
                                    ]
                               };
                   finally=Block { empty_block with
                                   export=["x", None];
                                   st=[ Let { var="x"; vartype=Some P;
                                              expr=Literal(T.v_int 18)
                                            }
                                      ]
                                 };
                 }
            );
      Value private_x;
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 18
                      
let test153() =
  let st =
    [ Value (Try { trial=Raise(Literal(T.v_int 5));
                   catch=Block { empty_block with
                                 st=[ Value CaughtValue ]
                               };
                   finally=Block { empty_block with
                                   st=[ Value(Literal(T.v_int 18)) ]
                                 };
                 }
            )
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 5
                      
let test154() =
  let st =
    [ Let { var="x"; vartype=Some P; expr=Literal(T.v_int 6) };
      Value (Try {trial=Try { trial=Raise(Literal(T.v_int 5));
                              catch=Block { empty_block with
                                            export=["x", None];
                                            st=[ Value(Raise CaughtValue)
                                               ]
                                          };
                              finally=Block { empty_block with
                                              export=["x", None];
                                              st=[ Let
                                                     { var="x";
                                                       vartype=Some P;
                                                       expr=Literal(T.v_int 18)
                                                     }
                                                 ]
                                            };
                            };
                  catch=Block { empty_block with
                                export=["x",None];
                                st=[Let{var="x"; vartype=Some P;
                                           expr=BinOp{binop=BSub;
                                                      arg1=private_x;
                                                      arg2=Literal(T.v_int 1)
                                                     }
                                          }
                                   ]
                              };
                  finally=Literal(T.v_false)
                 }
            );
      Value private_x;
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 17

let test155() =
  let st =
    [ fail;
      Let { var="x"; vartype=Some P; expr=Literal(T.v_int 6) };
      Value (Try {trial=Try { trial=fail_with(Literal(T.v_int 5));
                              catch=Block { empty_block with
                                            export=["x", None];
                                            st=[ Value(fail_with CaughtValue)
                                               ]
                                          };
                              finally=Block { empty_block with
                                              export=["x", None];
                                              st=[ Let
                                                     { var="x";
                                                       vartype=Some P;
                                                       expr=Literal(T.v_int 18)
                                                     }
                                                 ]
                                            };
                            };
                  catch=Block { empty_block with
                                export=["x",None];
                                st=[Let{var="x"; vartype=Some P;
                                        expr=BinOp{binop=BSub;
                                                   arg1=private_x;
                                                   arg2=Literal(T.v_int 1)
                                                  }
                                          }
                                   ]
                              };
                  finally=Literal(T.v_false)
                 }
            );
      Value private_x;
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 17

let test156() =
  let st =
    [ Let { var="x"; vartype=Some P; expr=Literal(T.v_int 6) };
      Value (Try { trial=Block { empty_block with
                                 export=["x", None];
                                 st=[ Let { var="x"; vartype=Some P;
                                            expr=Literal(T.v_int 34)
                                          }
                                    ]
                               };
                   catch=Block { empty_block with
                                 export=["x", None];
                                 st=[ Let { var="x"; vartype=Some P;
                                            expr=CaughtValue
                                          }
                                    ]
                               };
                   finally=Block { empty_block with
                                   export=["x", None];
                                   st=[Let{var="x"; vartype=Some P;
                                              expr=BinOp{binop=BSub;
                                                         arg1=private_x;
                                                         arg2=Literal(T.v_int 1)
                                                        }
                                             }
                                      ]
                                 };
                 }
            );
      Value private_x;
    ] in
  let v = cycle st in
  T.int_of_vbox cfg v = 33
                      
(* coercion *)

let test161() =
  let stl =
    [ Value (BinOp{binop=BAdd;
                   arg1=Coerce{usertype=T.T_int;
                               arg=Literal(T.v_string "123")
                              };
                   arg2=Literal(T.v_int 5)
                  }
            )
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 128

(* objects, fields *)

let test171() =
  let stl =
    [ Let {var="x"; vartype=Some P;
           expr=Exemplar { cls=None;
                           extends=Object "Object";
                           body={empty_block with
                                  st=[ Let { var="fx"; vartype=Some T;
                                            expr=Literal(T.v_int 56)
                                          }
                                     ];
                                }
                         }
          };
      Value (Field {this=private_x;
                    cls=None;
                    name="fx"
            })
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 56


let test172() =
  let fy =
    Abstract
      { named=[]; anon=[]; arity_min=0; autocurry=true;
        body={ empty_block with
               st=[ Value(Field{this=local_this;
                                cls=None;
                                name="fx"
                               })
                  ];
             }
      } in
  let stl =
    [ Let {var="x"; vartype=Some P;
           expr=Exemplar { cls=None;
                           extends=Object "Object";
                           body={empty_block with
                                  st=[ Let { var="fy"; vartype=Some T;
                                             expr=fy
                                           };
                                       Let { var="fx"; vartype=Some T;
                                             expr=Literal(T.v_int 56)
                                           };
                                     ];
                                }
                         }
          };
      Value (Field {this=private_x;
                    cls=None;
                    name="fy"
            })
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 56


let test173() =
  let fy =
    Abstract
      { named=[]; anon=["y"]; arity_min=1; autocurry=true;
        body={ empty_block with
               st=[ Value(BinOp{binop=BAdd;
                                arg1=Field{this=local_this;
                                           cls=None;
                                           name="fx"
                                          };
                                arg2=private_y})
                  ];
             }
      } in
  let stl =
    [ Let {var="x"; vartype=Some P;
           expr=Exemplar { cls=None;
                           extends=Object "Object";
                           body={empty_block with
                                  st=[ Let { var="fy"; vartype=Some T;
                                             expr=fy
                                           };
                                       Let { var="fx"; vartype=Some T;
                                             expr=Literal(T.v_int 56)
                                           };
                                     ];
                                }
                         }
          };
      Let {var="z"; vartype=Some P;
           expr=Field {this=private_x;
                       cls=None;
                       name="fy"
                      };
          };
      Value(Apply{var="z"; vartype=None;
                  named=[]; anon=[Literal(T.v_int 3)]
                 })

    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 59

(* update of fields *)

let test181() =
  let fy =
    Abstract
      { named=[]; anon=["y"]; arity_min=1; autocurry=true;
        body={ empty_block with
               st=[ Let { var="fx"; vartype=Some T;
                          expr=private_y
                        };
                    Value(local_this);
                  ]
             }
      } in
  let stl =
    [ Let {var="x"; vartype=Some P;
           expr=Exemplar { cls=None;
                           extends=Object "Object";
                           body={empty_block with
                                  st=[ Let { var="fy"; vartype=Some T;
                                             expr=fy
                                           };
                                       Let { var="fx"; vartype=Some T;
                                             expr=Literal(T.v_int 56)
                                           };
                                     ];
                                }
                         }
          };
      Let {var="z"; vartype=Some P;
           expr=Field {this=private_x;
                       cls=None;
                       name="fy"
                      };
          };
      Value(Field{this=Apply{var="z"; vartype=None;
                             named=[]; anon=[Literal(T.v_int 3)]
                            };
                  cls=None;
                  name="fx"
                 })

    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 3


let test182() =
  let fy =
    Abstract
      { named=[]; anon=["y"]; arity_min=1; autocurry=true;
        body={ empty_block with
               st=[ Let { var="fx"; vartype=Some T;
                          expr=private_y
                        };
                    Value(local_this);
                  ]
             }
      } in
  let stl =
    [ Let {var="x"; vartype=Some P;
           expr=Exemplar { cls=Some "foo";
                           extends=Object "Object";
                           body={empty_block with
                                  st=[ Let { var="fy"; vartype=Some T;
                                             expr=fy
                                           };
                                       Let { var="fx"; vartype=Some T;
                                             expr=Literal(T.v_int 56)
                                           };
                                     ];
                                }
                         }
          };
      Let {var="z"; vartype=Some P;
           expr=Field {this=private_x;
                       cls=None;
                       name="fy"
                      };
          };
      Value(Field{this=Apply{var="z"; vartype=None;
                             named=[]; anon=[Literal(T.v_int 3)]
                            };
                  cls=Some "foo";
                  name="fx"
                 })

    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 56
                          
(* export of fields *)

let test191() =
  let fy =
    Abstract
      { named=[]; anon=["y"]; arity_min=1; autocurry=true;
        body={ empty_block with
               st=[ Section
                      { empty_block with
                        st=[Let { var="fx"; vartype=Some T;
                                  expr=private_y
                                };
                           ];
                        export=[ "fx", Some T ];
                      };
                    Value(local_this);
                  ]
             }
      } in
  let stl =
    [ Let {var="x"; vartype=Some P;
           expr=Exemplar { cls=None;
                           extends=Object "Object";
                           body={empty_block with
                                  st=[ Let { var="fy"; vartype=Some T;
                                             expr=fy
                                           };
                                       Let { var="fx"; vartype=Some T;
                                             expr=Literal(T.v_int 56)
                                           };
                                     ];
                                }
                         }
          };
      Let {var="z"; vartype=Some P;
           expr=Field {this=private_x;
                       cls=None;
                       name="fy"
                      };
          };
      Value(Field{this=Apply{var="z"; vartype=None;
                             named=[]; anon=[Literal(T.v_int 3)]
                            };
                  cls=None;
                  name="fx"
                 })

    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 3

let f_setfx =
  Abstract
    { named=[]; anon=["dummy"]; arity_min=1;
      autocurry=false;
      body={ empty_block with
             export=[ "fx", None ];
             st=[ Let { var="fx"; vartype=Some T;
                        expr=Literal(T.v_int 65)
                      }
                ]
           }
    }

let test192() =
  let fy =
    Abstract
      { named=[]; anon=["y"]; arity_min=1; autocurry=true;
        body={ empty_block with
               st=[ Let{var="setfx"; vartype=Some P;
                        expr=f_setfx
                       };
                    Effect(Apply{var="setfx"; vartype=Some P;
                                 named=[]; anon=[Literal(T.v_int 0)]});
                    Value(local_this);
                  ]
             }
      } in
  let stl =
    [ Let {var="x"; vartype=Some P;
           expr=Exemplar { cls=None;
                           extends=Object "Object";
                           body={empty_block with
                                  st=[ Let { var="fy"; vartype=Some T;
                                             expr=fy
                                           };
                                       Let { var="fx"; vartype=Some T;
                                             expr=Literal(T.v_int 56)
                                           };
                                     ];
                                }
                         }
          };
      Let {var="z"; vartype=Some P;
           expr=Field {this=private_x;
                       cls=None;
                       name="fy"
                      };
          };
      Value(Field{this=Apply{var="z"; vartype=None;
                             named=[]; anon=[Literal(T.v_int 3)]
                            };
                  cls=None;
                  name="fx"
                 })

    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 65


(* inheritance *)

let test201() =
  let fy =
    Abstract
      { named=[]; anon=["y"]; arity_min=1; autocurry=true;
        body={ empty_block with
               st=[ Value(BinOp{binop=BAdd;
                                arg1=Field {this=local_this;
                                            cls=None;
                                            name="fx"
                                           };
                                arg2=private_y
                               });
                  ]
             }
      } in
  let stl =
    [ Let {var="x"; vartype=Some P;
           expr=Exemplar { cls=Some "foo";
                           extends=Object "Object";
                           body={empty_block with
                                  st=[ Let { var="fy"; vartype=Some T;
                                             expr=fy
                                           };
                                       Let { var="fx"; vartype=Some T;
                                             expr=Literal(T.v_int 56)
                                           };
                                     ];
                                }
                         }
          };
      Let {var="y"; vartype=Some P;
           expr=Exemplar { cls=Some "bar";
                           extends=private_x;
                           body={empty_block with
                                  st=[ Let { var="fx"; vartype=Some T;
                                             expr=Literal(T.v_int 87)
                                           };
                                     ];
                                }
                         }
          };
      Let {var="z"; vartype=Some P;
           expr=Field {this=private_y;
                       cls=None;
                       name="fy"
                      };
          };
      Value(Apply{var="z"; vartype=None;
                  named=[]; anon=[Literal(T.v_int 3)]
                 });
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 90


let test202() =
  let fy =
    Abstract
      { named=[]; anon=["y"]; arity_min=1; autocurry=true;
        body={ empty_block with
               st=[ Value(BinOp{binop=BAdd;
                                arg1=Field {this=local_this;
                                            cls=Some "foo"; (* diff to 201 *)
                                            name="fx"
                                           };
                                arg2=private_y
                               });
                  ]
             }
      } in
  let stl =
    [ Let {var="x"; vartype=Some P;
           expr=Exemplar { cls=Some "foo";
                           extends=Object "Object";
                           body={empty_block with
                                  st=[ Let { var="fy"; vartype=Some T;
                                             expr=fy
                                           };
                                       Let { var="fx"; vartype=Some T;
                                             expr=Literal(T.v_int 56)
                                           };
                                     ];
                                }
                         }
          };
      Let {var="y"; vartype=Some P;
           expr=Exemplar { cls=Some "bar";
                           extends=private_x;
                           body={empty_block with
                                  st=[ Let { var="fx"; vartype=Some T;
                                             expr=Literal(T.v_int 87)
                                           };
                                     ];
                                }
                         }
          };
      Let {var="z"; vartype=Some P;
           expr=Field {this=private_y;
                       cls=None;
                       name="fy"
                      };
          };
      Value(Apply{var="z"; vartype=None;
                  named=[]; anon=[Literal(T.v_int 3)]
                 });
    ] in
  let v = cycle stl in
  T.int_of_vbox cfg v = 59
                          

let tests =
  [ "test001", test001;
    "test002", test002;
    "test003", test003;
    "test004", test004;
    "test005", test005;
    "test011", test011;
    "test012", test012;
    "test013", test013;
    "test014", test014;
    "test015", test015;
    "test021", test021;
    "test031", test031;
    "test032", test032;
    "test033", test033;
    "test034", test034;
    "test035", test035;
    "test041", test041;
    "test042", test042;
    "test043", test043;
    "test044", test044;
    "test051", test051;
    "test052", test052;
    "test053", test053;
    "test054", test054;
    "test055", test055;
    "test056", test056;
    "test061", test061;
    "test062", test062;
    "test071", test071;
    "test072", test072;
    "test081", test081;
    "test082", test082;
    "test091", test091;
    "test101", test101;
    "test102", test102;
    "test111", test111;
    "test112", test112;
    "test113", test113;
    "test114", test114;
    "test121", test121;
    "test122", test122;
    "test131", test131;
    "test132", test132;
    "test133", test133;
    "test141", test141;
    "test142", test142;
    "test143", test143;
    "test151", test151;
    "test152", test152;
    "test153", test153;
    "test154", test154;
    "test155", test155;
    "test156", test156;
    "test161", test161;
    "test171", test171;
    "test172", test172;
    "test173", test173;
    "test181", test181;
    "test182", test182;
    "test191", test191;
    "test192", test192;
    "test201", test201;
    "test202", test202;
  ]
