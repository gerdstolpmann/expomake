(* UNCLEAR: How to create objects step by step

   let this = ref object
   this += (name -> field)
   ...
   !this

   Be careful with exports:
    - Exports outside a method of "this" must not destroy the reference
      (use a special scope Classdef to catch this)
    - Methods must not export fields at all (do not allow writable env vars!)

   Idea Class:
    - Initially defines $(this) as reference
    - every statement sees this reference
    - every statement can define a new $(this), and the reference is set
      to this value
 *)

module Types : sig
  type statement =
    | Let of { var:string; vartype:vartype option; expr:expr }
    | Declare of { var:string; vartype:vartype }
    | Qualify of { vartype:vartype }
    | Section of block
    | Value of expr
    | Effect of expr
    | Return
    | Extstm of extstm

   and block =
     { st : statement list;
       export : (string * vartype option) list;
       export_glb : bool;   (** export all globals *)
       export_obj : bool;   (** export all fields of the object *)
     }

   and expr =
     | Abstract of { named : string list; anon : string list; arity_min : int;
                     autocurry : bool; body : block }
     | Apply of { var : string; vartype:vartype option;
                  named : (string * expr) list; anon : expr list }
     | Thread of { var : string; vartype:vartype option;
                   named : (string * expr) list; anon : expr list }
     | LetLocal of { var:string; expr:expr; inexpr:expr }
     | If of { cond:expr; ontrue:expr; onfalse:expr }
     | Try of { trial:expr; catch:expr; finally:expr}
     | Lazy of expr
     | IsUninitialized of expr
     | Literal of Omake_vm.Types.stringval
     | Raise of expr
     | CaughtValue     (** the caught exception in [catch] *)
     | Coerce of { arg : expr; usertype : Omake_vm.Types.usertype }
     | Defined of expr
     | Block of block
     | MultiOp of { multiop:multiop; args:expr list }
     | MonOp of { monop:monop; arg:expr }
     | BinOp of { binop:binop; arg1:expr; arg2:expr }
     | Object of string          (** the empty object of this class *)
     | Exemplar of { cls : string option;   (** override class of [extends] *)
                     extends : expr;
                     body : block;
                   }
     | Field of { this:expr; cls:string option; name:string }
     | Extexpr of extexpr

   (* | ClassOf of expr *)

   and multiop =
     | NSeq
     | NArray
     | NConcat

   and monop =
     | MQuote of char
     | MLength
     | MWait
     | MTest
     | MArraySeq

   and binop =
     | BNth   (** n-th element of sequence or array *)
     | BAdd   (** integer addition *)
     | BSub   (** integer subtraction *)
     | BMul   (** integer multiplication *)
     | BDiv   (** integer division *)
     | BMod   (** integer modulus *)
     | BEq    (** integer equality *)
     | BNe    (** integer inequality *)
     | BLt    (** integer less than *)
     | BLe    (** integer less or equal than *)

   and vartype =
     | P      (** private *)
     | L      (** local (compiler artifact) *)
     | G      (** global *)
     | S      (** shared *)
     | T      (** this *)
     | Prim   (** primitive *)

   and extstm = ..
   and extexpr = ..

end

module Compiler : sig
  module SMap : Map.S with type key = string
  module SSet : Set.S with type elt = string

  exception Compile_error of string

  type manager = private
       { free_reg : int;
         num_reg : int ref;
         num_lab : int ref;
         glb_reg : int;
         privars : (int * scope) SMap.t;
         locvars : (int * scope * bool) SMap.t;
         privenv : envvar SMap.t;
         locenv : envvar SMap.t;
         val_reg : int;
         catch_reg : int option;
         exit_label : string option;
         trap_labels : string list;
         num_funs : int ref;
         alloc : Omake_vm.Types.alloc;
         shared : Omake_vm.Types.shared;
         prims : int SMap.t;
       }

   and snippet = private
       { config : Omake_vm.Types.config;
         mutable manager : manager;
         mutable regs : Omake_vm.Types.valuebox array;
         mutable code : Omake_vm.Vm.instruction array;
       }

   and envvar
   and scope = Current | Outer | Far

  val create_snippet : ?config:Omake_vm.Types.config ->
                       Omake_vm.Types.global -> snippet
  val compile_snippet : snippet -> Types.statement list -> unit
  val run_snippet : Omake_vm.Types.global -> snippet -> unit
  val global_of_snippet : snippet -> Omake_vm.Types.global
  val value_of_snippet : snippet -> Omake_vm.Types.valuebox

end
                    
