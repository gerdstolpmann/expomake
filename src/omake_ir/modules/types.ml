module Seq = Omake_vm.Seq
module Lookup_table = Omake_vm.Lookup_table
module VT = Omake_vm.Types

type statement =
  | Let of { var:string; vartype:vartype option; expr:expr }
  | Declare of { var:string; vartype:vartype }
  | Qualify of { vartype:vartype }
  | Section of block
  | Value of expr
  | Effect of expr
  | Return
  | Extstm of extstm

 and block =
   { st : statement list;
     export : (string * vartype option) list;
     export_glb : bool;
     export_obj : bool;
   }

 and expr =
   | Abstract of { named : string list; anon : string list; arity_min : int;
                   autocurry : bool; body : block }
   | Apply of { var : string; vartype:vartype option;
                named : (string * expr) list; anon : expr list }
   | Thread of { var : string; vartype:vartype option;
                 named : (string * expr) list; anon : expr list }
   | LetLocal of { var:string; expr:expr; inexpr:expr }
   | If of { cond:expr; ontrue:expr; onfalse:expr }
   | Try of { trial:expr; catch:expr; finally:expr}
   | Lazy of expr
   | IsUninitialized of expr
   | Literal of VT.stringval
   | Raise of expr
   | CaughtValue
   | Coerce of { arg : expr; usertype : VT.usertype }
   | Defined of expr
   | Block of block
   | MultiOp of { multiop:multiop; args:expr list }
   | MonOp of { monop:monop; arg:expr }
   | BinOp of { binop:binop; arg1:expr; arg2:expr }
   | Object of string
   | Exemplar of { cls : string option;
                   extends : expr;
                   body : block;
                 }
   | Field of { this:expr; cls:string option; name:string }
   | Extexpr of extexpr
   (* | ClassOf of expr *)

 and multiop =
   | NSeq
   | NArray
   | NConcat

 and monop =
   | MQuote of char
   | MLength
   | MWait
   | MTest
   | MArraySeq

 and binop =
   | BNth
   | BAdd
   | BSub
   | BMul
   | BDiv
   | BMod
   | BEq
   | BNe
   | BLt
   | BLe

 and vartype =
   | P
   | L
   | G
   | S
   | T
   | Prim

 and extstm = ..
 and extexpr = ..
