module Seq = Omake_vm.Seq
module Lookup_table = Omake_vm.Lookup_table
module Vm = Omake_vm.Vm
module VT = Omake_vm.Types

module SMap = Map.Make(String)
module SSet = Set.Make(String)

open Types
open Printf

(* Local variables:

   - local variables must always be qualified
   - local variables behave like private variables in a separate namespace.
     Same rules for exports.
   - local variables can occur in environments
   - special feature: local variables can be references. In this case,
     exports mutate the reference
   - they are intended for compiler-generated local helper values
   - already predefined: "this"
 *)


type manager =
     { free_reg : int;
       num_reg : int ref;
       num_lab : int ref;
       glb_reg : int;
       privars : (int * scope) SMap.t;
       locvars : (int * scope * bool) SMap.t;
       privenv : envvar SMap.t;
       locenv : envvar SMap.t;
       val_reg : int;
       catch_reg : int option;
       exit_label : string option;
       trap_labels : string list;
       num_funs : int ref;
       alloc : VT.alloc;
       shared : VT.shared;
       prims : int SMap.t;
     }

 and snippet =
   { config : Omake_vm.Types.config;
     mutable manager : manager;
     mutable regs : VT.valuebox array;
     mutable code : Vm.instruction array;
   }

 and envvar =
   { env_rd : int;
     env_wr : int option;    (* only for exported variables *)
     env_ref : bool;         (* whether env_rd is a mutable reference *)
   }

 and scope =
   | Current    (* current block *)
   | Outer      (* the block (of the same fun) surrounding the current block *)
   | Far        (* any blocks (of the same fun) surrounding the outer block *)

type extstm +=
   | Copy_field of { var : string; src_var : string; name : string }

type extexpr +=
   | Abstract_local of { named : string list; anon : string list;
                         arity_min : int;
                         autocurry : bool; body : block }
   | Update_obj of { var : string; name : string; expr : expr }
   | Extend_obj of { var : string; cls : string option; name : string;
                     expr : expr }


type orw_manager =
  { exemplar : exemplar option;
    inobj : bool;
  }

 and exemplar =
   { exclass : string option }


exception Compile_error of string

let compile_error msg =
  raise(Compile_error msg)

let domain map =
  SMap.fold
    (fun key _ set -> SSet.add key set)
    map
    SSet.empty

let double_occurrences l =
  let ht = Hashtbl.create 7 in
  List.filter
    (fun x ->
      if Hashtbl.mem ht x then
        true
      else (
        Hashtbl.add ht x ();
        false
      )
    )
    l

let map_sub_expr f expr =
  (* This function does NOT enter blocks! *)
  match expr with
    | Abstract _ -> expr
    | Apply p -> Apply { p with
                         named = List.map (fun (n,e) -> (n,f e)) p.named;
                         anon = List.map f p.anon
                       }
    | Thread p -> Thread { p with
                           named = List.map (fun (n,e) -> (n,f e)) p.named;
                           anon = List.map f p.anon
                         }
    | LetLocal p -> LetLocal { p with
                               expr = f p.expr;
                               inexpr = f p.inexpr;
                             }
    | If p -> If { cond = f p.cond;
                   ontrue = f p.ontrue;
                   onfalse = f p.onfalse
                 }
    | Try p -> Try { trial = f p.trial;
                     catch = f p.catch;
                     finally = f p.finally
                   }
    | Lazy e -> Lazy (f e)
    | IsUninitialized e -> IsUninitialized(f e)
    | Literal _ -> expr
    | Raise e -> Raise(f e)
    | CaughtValue -> expr
    | Coerce p -> Coerce { p with arg = f p.arg }
    | Defined e -> Defined (f e)
    | Block _ -> expr
    | MultiOp p -> MultiOp { p with args = List.map f p.args }
    | MonOp p -> MonOp { p with arg = f p.arg }
    | BinOp p -> BinOp { p with arg1 = f p.arg1; arg2 = f p.arg2 }
    | Object _ -> expr
    | Exemplar p -> Exemplar { p with extends = f p.extends }
    | Field p -> Field { p with this = f p.this }
    | Extexpr(Abstract_local p) -> expr
    | Extexpr(Update_obj p) -> Extexpr(Update_obj { p with expr = f p.expr })
    | Extexpr(Extend_obj p) -> Extexpr(Extend_obj { p with expr = f p.expr })
    | Extexpr _ -> failwith "Omake_ir.Compiler: unknown extension"


let get_sub_expr expr =
  match expr with
    | Abstract _ -> []
    | Apply p -> List.map snd p.named @ p.anon
    | Thread p -> List.map snd p.named @ p.anon
    | LetLocal p -> [ p.expr; p.inexpr ]
    | If p -> [ p.cond; p.ontrue; p.onfalse ]
    | Try p -> [ p.trial; p.catch; p.finally ]
    | Lazy e -> [e]
    | IsUninitialized e -> [e]
    | Literal _ -> []
    | Raise e -> [e]
    | CaughtValue -> []
    | Coerce p -> [p.arg]
    | Defined e -> [e]
    | Block _ -> []
    | MultiOp p -> p.args
    | MonOp p -> [ p.arg ]
    | BinOp p -> [ p.arg1; p.arg2 ]
    | Object _ -> []
    | Exemplar p -> [ p.extends ]
    | Field p -> [ p.this ]
    | Extexpr(Abstract_local p) -> []
    | Extexpr(Update_obj p) -> [ p.expr ]
    | Extexpr(Extend_obj p) -> [ p.expr ]
    | Extexpr _ -> failwith "Omake_ir.Compiler: unknown extension"


let blocks_of_expr expr =
  let l = ref [] in
  let rec recurse expr =
    match expr with
      | Abstract p -> l := p.body :: !l
      | Block b -> l := b :: !l
      | Exemplar p -> l := p.body :: !l
      | _ -> List.iter recurse (get_sub_expr expr) in
  recurse expr;
  List.rev !l


let block_exports_this block =
  block.export_obj ||
    List.exists (fun (_,vt) -> vt = Some T) block.export


let rec qualify_vars_of_block qual default block =
  (* figures the vartype out for all unqualified variables. [qual]
     maps variable names to vartypes. [default] is the default
     qualification. This function removes [Qualify] statements and
     ensures that [vartype] is never [None].
   *)
  let qual', default', st' = qualify_vars_of_stlist qual default block.st in
  let export' =
    List.map
      (fun (var,vartype) ->
        let vt =
          try SMap.find var qual'
          with Not_found -> default' in
        (var, Some vt)
      )
      block.export in
  { block with st = st'; export = export' }

and qualify_vars_of_stlist qual default stlist =
  let (qual,default,stlist) =
    List.fold_left
      (fun (qual,default,acc) st ->
        match st with
          | Let { var; vartype=Some vt; expr} ->
              let st' =
                Let { var; vartype=Some vt;
                      expr=qualify_vars_of_expr qual default expr
                    } in
              let qual' = SMap.add var vt qual in
              (qual', default, st'::acc)
          | Let { var; vartype=None; expr} ->
              let vt =
                try SMap.find var qual
                with Not_found -> default in
              let st' =
                Let { var; vartype=Some vt;
                      expr=qualify_vars_of_expr qual default expr
                    } in
              let qual' = SMap.add var vt qual in
              (qual', default, st'::acc)
          | Declare { var; vartype } ->
              let qual' = SMap.add var vartype qual in
              (qual', default, st::acc)
          | Qualify { vartype } ->
              (qual, vartype, acc)     (* remove Qualify *)
          | Section block ->
              let block' = qualify_vars_of_block qual default block in
              let st' = Section block' in
              (qual, default, st'::acc)
          | Value expr ->
              let expr' = qualify_vars_of_expr qual default expr in
              let st' = Value expr' in
              (qual, default, st'::acc)
          | Effect expr ->
              let expr' = qualify_vars_of_expr qual default expr in
              let st' = Effect expr' in
              (qual, default, st'::acc)
          | Return ->
              (qual, default, st::acc)
          | Extstm _ ->
              failwith "Omake_ir.Compiler: unknown extension"
      )
      (qual,default,[])
      stlist in
  let stlist = List.rev stlist in
  (* also interpret any exports from blocks in subexpressions: *)
  let sub_blocks =
    List.flatten
      (List.map
         (function
          | Let { expr; _ } -> blocks_of_expr expr
          | Value expr -> blocks_of_expr expr
          | _ -> []  (* Section already handled! *)
         )
         stlist
      ) in
  let qual =
    List.fold_left
      (fun acc block ->
        List.fold_left
          (fun acc (n, vt_opt) ->
            match vt_opt with
              | None -> acc
              | Some vt ->
                  (* exports cannot override the variable qualification *)
                  if SMap.mem n acc then acc else
                    SMap.add n vt acc
          )
          acc
          block.export
      )
      qual
      sub_blocks in
  (qual, default, stlist)

and qualify_vars_of_expr qual default expr =
  match expr with
    | Abstract p ->
        let qual' =
          List.fold_left
            (fun qual n -> SMap.add n P qual)
            qual
            (p.named @ p.anon) in
        let body' = qualify_vars_of_block qual' default p.body in
        Abstract { p with body = body' }
    | Apply p ->
        let vt =
          match p.vartype with
            | None ->
                ( try SMap.find p.var qual
                  with Not_found -> default
                )
            | Some vt -> vt in
        let expr' = Apply { p with vartype = Some vt } in
        map_sub_expr (qualify_vars_of_expr qual default) expr'
    | Thread p ->
        let vt =
          match p.vartype with
            | None ->
                ( try SMap.find p.var qual
                  with Not_found -> default
                )
            | Some vt -> vt in
        let expr' = Thread { p with vartype = Some vt } in
        map_sub_expr (qualify_vars_of_expr qual default) expr'
    | Block b ->
        Block (qualify_vars_of_block qual default b)
    | Exemplar p ->
        Exemplar { p with
                   extends = qualify_vars_of_expr qual default p.extends;
                   body = qualify_vars_of_block qual T p.body
                 }
    | _ ->
        map_sub_expr (qualify_vars_of_expr qual default) expr


let rec used_static_vars_of_block vt vars block =
  (* return the subset of [vars] that is really accessed in [block],
     assuming that [vars] are the vars of type [vt] around [block] (i.e. the
     potentially free vars in [block])
   *)
  used_static_vars_of_stlist vt vars block.st

and used_static_vars_of_stlist vt vars stlist =
  match stlist with
    | Let { var; vartype=Some t; expr } :: stlist' when t=vt ->
        let s1 = used_static_vars_of_expr vt vars expr in
        let s2 = used_static_vars_of_stlist vt (SSet.remove var vars) stlist' in
        SSet.union s1 s2
    | Let { var; vartype; expr } :: stlist' ->
        let s1 = used_static_vars_of_expr vt vars expr in
        let s2 = used_static_vars_of_stlist vt vars stlist' in
        SSet.union s1 s2
    | Declare { var; vartype=t } :: stlist' when t=vt ->
        used_static_vars_of_stlist vt (SSet.remove var vars) stlist'
    | Declare { var; vartype } :: stlist' ->
        used_static_vars_of_stlist vt vars stlist'
    | Section block :: stlist' ->
        let s1 = used_static_vars_of_block vt vars block in
        let s2 = used_static_vars_of_stlist vt vars stlist' in
        SSet.union s1 s2
    | (Value expr | Effect expr) :: stlist' ->
        let s1 = used_static_vars_of_expr vt vars expr in
        let s2 = used_static_vars_of_stlist vt vars stlist' in
        SSet.union s1 s2
    | Qualify _ :: _ ->
        assert false
    | Return :: stlist' ->
        used_static_vars_of_stlist vt vars stlist'
    | Extstm(Copy_field { var; src_var; _ }) :: stlist' when vt=L ->
        let s = used_static_vars_of_stlist vt (SSet.remove var vars) stlist' in
        SSet.add src_var s
    | Extstm(Copy_field _) :: stlist' ->
        used_static_vars_of_stlist vt vars stlist'
    | Extstm _ :: _ ->
        failwith "Omake_ir.Compiler: unknown extension"
    | [] ->
        SSet.empty

and used_static_vars_of_expr vt vars expr =
  match expr with
    | Abstract { named; anon; body; _ } ->
        let vars' =
          if vt = P then
            List.fold_left
              (fun set param -> SSet.remove param set)
              vars
              (named @ anon)
          else
            vars in
        used_static_vars_of_block vt vars' body
    | Apply { var; vartype=Some t; named; anon } when t=vt ->
        let s1 =
          if SSet.mem var vars then SSet.singleton var else SSet.empty in
        let subexpr = List.map snd named @ anon in
        List.fold_left
          (fun set e -> SSet.union set (used_static_vars_of_expr vt vars e))
          s1
          subexpr
    | LetLocal { var; expr; inexpr } when vt=L ->
        let s1 = used_static_vars_of_expr vt vars expr in
        let s2 = used_static_vars_of_expr vt (SSet.remove var vars) inexpr in
        SSet.union s1 s2
    | Exemplar _ -> (* should be rewritten! *)
        assert false
    | Extexpr(Abstract_local{named; anon; body; _}) ->
        let vars' =
          if vt = L then
            List.fold_left
              (fun set param -> SSet.remove param set)
              vars
              (named @ anon)
          else
            vars in
        used_static_vars_of_block vt vars' body
    | Extexpr(Update_obj{var; expr; _}) ->
        let s1 = used_static_vars_of_expr vt vars expr in
        if vt=L && SSet.mem var vars then
          SSet.add var s1
        else
          s1
    | Extexpr(Extend_obj{var; expr; _}) ->
        let s1 = used_static_vars_of_expr vt vars expr in
        if vt=L && SSet.mem var vars then
          SSet.add var s1
        else
          s1
    | _ ->
        let subexprs = get_sub_expr expr in
        List.fold_left
          (fun set e -> SSet.union set (used_static_vars_of_expr vt vars e))
          SSet.empty
          subexprs

let used_private_vars_of_block privars block =
  used_static_vars_of_block P privars block

let used_local_vars_of_block locvars block =
  used_static_vars_of_block L locvars block
                            
let rec expr_might_be_lazy =
  function
  | Abstract _ -> false
  | Apply _ -> true
  | Thread _ -> false
  | Field _ -> true
  | If p -> expr_might_be_lazy p.ontrue || expr_might_be_lazy p.onfalse
  | Try p -> expr_might_be_lazy p.trial || expr_might_be_lazy p.catch
  | Defined e -> expr_might_be_lazy e
  | LetLocal p -> expr_might_be_lazy p.inexpr
  | Lazy _ -> true
  | CaughtValue -> true
  | IsUninitialized _
    | Literal _
    | Raise _
    | Coerce _
    | MultiOp _
    | MonOp _
    | BinOp _
    | Object _
    | Exemplar _ -> false
  | Block _ -> true   (* further analysis needed... *)
  | Extexpr(Abstract_local _) -> false
  | Extexpr(Update_obj _) -> false
  | Extexpr(Extend_obj _) -> false
  | Extexpr _ -> true  (* conservative... *)

let rec orewrite_block om block =
  let in_obj_context = block_exports_this block in
  if in_obj_context && not om.inobj then
    compile_error
      "Outside of an object but object fields are tried to be exported";
  let exp_fields, exp_non_fields =
    List.partition
      (fun (_,vt) -> vt=Some T)
      block.export in
  let st =
    if in_obj_context then
      [ Let { var="outer_this"; vartype=Some L;
              expr=Apply{var="this"; vartype=Some L;
                         named=[]; anon=[]
                        }
            }
      ] @
        orewrite_stlist om block.st @
          List.map
            (fun (var, _) ->
              Extstm(Copy_field { var="outer_this"; src_var="this"; name=var })
            )
            exp_fields @
            [ Let { var="this"; vartype=Some L;
                    expr=Apply{var="outer_this"; vartype=Some L;
                               named=[]; anon=[]
                              }
                  }
            ]
    else
      orewrite_stlist om block.st in
  let export =
    if in_obj_context then
      [ "this", Some L ] @ exp_non_fields
    else
      exp_non_fields in
  { block with
    export;
    export_obj = false;
    st = st
  }

and orewrite_stlist om stl =
  List.flatten
    (List.map (orewrite_st om) stl)

and orewrite_st om st =
  match st with
    | Let { var; vartype = Some T; expr } ->
        orewrite_let_this om var expr
    | Let p ->
        [ Let { p with expr = orewrite_expr om p.expr } ]
    | Section block ->
        [ Section (orewrite_block om block) ]
    | Value expr ->
        [ Value (orewrite_expr om expr) ]
    | Effect expr ->
        [ Effect (orewrite_expr om expr) ]
    | ( Declare _ | Qualify _ | Return ) ->
        [st]
    | Extstm _ ->
        failwith "Omake_ir.Compiler: unknown extension"

and orewrite_expr om expr =
  match expr with
    | Exemplar _ ->
        orewrite_exemplar om expr
    | Apply _ ->
        orewrite_apply om expr
    | Block block ->
        Block (orewrite_block om block)
    | Abstract p ->
        Abstract { p with body = orewrite_block om p.body }
    | Extexpr _ ->
        failwith "Omake_ir.Compiler: unknown extension"
    | _ ->
        map_sub_expr (orewrite_expr om) expr

and orewrite_let_this om var expr =
  (* FIXME: a simple this.field=expr needs 7 instructons in the generated
     function for storing the result of expr. This could be ONE special
     instruction!
   *)
  if not om.inobj then
    compile_error
      "Outside of an object but object fields are updated";
  let om_sub =
    { om with exemplar = None } in
  let st1, e1 =
    match expr with
      | Abstract { named=[]; anon=[]; arity_min=0; autocurry; body } ->
          let body = orewrite_block om_sub body in
          ( [],
            Extexpr
              (Abstract_local { named=[]; anon=["this"]; arity_min=1;
                                autocurry=true;
                                body
                              }
              )
          )
      | Abstract _ ->
          let expr = orewrite_expr om_sub expr in
          ( [],
            Extexpr
              (Abstract_local { named=[]; anon=["this"]; arity_min=1;
                                autocurry=true;
                                body={ st=[ Value expr ];
                                       export = [];
                                       export_glb = false;
                                       export_obj = false
                                     }
                            }
              )
          )
      | _ ->
          let expr = orewrite_expr om_sub expr in
          ( [ Let { var="value"; vartype=Some L; expr } ],
            Extexpr
              (Abstract_local { named=[]; anon=["this"]; arity_min=1;
                                autocurry=true;
                                body={ st=[ Value(Apply{var="value";
                                                        vartype=Some L;
                                                        named=[];
                                                        anon=[]
                                                 })
                                          ];
                                       export = [];
                                       export_glb = false;
                                       export_obj = false
                                     }
                              }
              )
          ) in
  let st2 =
    match om.exemplar with
      | None ->
          [ Let { var="this"; vartype=Some L;
                  expr=Extexpr(Update_obj{var="this";
                                          name=var;
                                          expr=e1})
                }
          ]
      | Some ex ->
          [ Let { var="this"; vartype=Some L;
                  expr=Extexpr(Extend_obj{var="this";
                                          name=var;
                                          expr=e1;
                                          cls=ex.exclass})
                }
          ] in
  st1 @ st2

and orewrite_exemplar om expr =
  match expr with
    | Exemplar p ->
        (* (* -- too difficult to stick to... *)
        if block_exports_this p.body then
          compile_error "An object exemplar cannot export object fields";
         *)
        let om_sub =
          { exemplar = Some { exclass = p.cls };
            inobj = true
          } in
        let b = orewrite_block om_sub p.body in
        let st =
          [ Let { var="this"; vartype=Some L; expr=p.extends } ] @
            b.st @
              [ Value(Apply { var="this"; vartype=Some L; named=[];
                              anon=[] })] in
        Block
          { b with
            st
          }
    | _ ->
        assert false

and orewrite_apply om expr =
  match expr with
    | Apply { var="this"; vartype=Some L; _ } ->
        if not om.inobj then
          compile_error "Can only access 'this' inside an object";
        map_sub_expr (orewrite_expr om) expr
    | _ ->
        map_sub_expr (orewrite_expr om) expr



let temp_regs m n =
  m.num_reg := max !(m.num_reg) (m.free_reg + n - 1);
  m.free_reg

let new_reg m =
  let r = m.free_reg in
  let m = { m with free_reg = r+1 } in
  m.num_reg := max !(m.num_reg) (r+1);
  r, m

let new_label m =
  let k = !(m.num_lab) in
  incr m.num_lab;
  sprintf "L%d" k

let global_slot m name : int =
  Lookup_table.add_slot name m.alloc.VT.alloc_variables

let not_exportable vt =
  vt = Some Prim

let rec transl_block m block =
  if List.exists (fun (_,vt) -> not_exportable vt) block.export
  then invalid_arg "transl_block";
  let exp_privars =
    List.map
      fst
      (List.filter (fun (_,vt) -> vt = Some P) block.export) in
  let exp_locvars =
    List.map
      fst
      (List.filter (fun (_,vt) -> vt = Some L) block.export) in
  let exp_globals =
    List.map
      fst
      (List.filter (fun (_,vt) -> vt = Some G) block.export) in
  let privars =
    SMap.mapi
      (fun name (reg,scope) ->
        match scope with
          | Current -> (reg, Outer)
          | Outer -> (reg, Far)
          | Far -> (reg, Far)
      )
      m.privars in
  let locvars =
    SMap.mapi
      (fun name (reg,scope,is_ref) ->
        match scope with
          | Current -> (reg, Outer,is_ref)
          | Outer -> (reg, Far,is_ref)
          | Far -> (reg, Far,is_ref)
      )
      m.locvars in
  let glb_reg, m = new_reg m in
  let lab1 = new_label m in
  let lab2 = new_label m in
  let m_start =
    { m with   (* same val_reg *)
      glb_reg;
      privars;
      locvars;
      exit_label = (if m.exit_label <> None then Some lab1 else None);
    } in
  let instr0 =
    [ Vm.I_move { reg=glb_reg; src_reg = m.glb_reg } ] in
  let (instr1, m_post) =
    transl_stlist m_start block.st in
  let instr2p = transl_export_privars m m_post exp_privars in
  let instr2l = transl_export_locvars m m_post exp_locvars in
  let instr3 =
    if block.export_glb then
      transl_export_glb m m_post
    else
      transl_export_globals m m_post exp_globals in
  let instr4 =
    [ Vm.I_jump { jump=Vm.Lab lab2 };
      Vm.I_label lab1;
    ] in
  let instr5 xlab =
    [ Vm.I_jump { jump=Vm.Lab xlab };
      Vm.I_label lab2
    ] in
  instr0 @ instr1 @ instr2p @ instr2l @ instr3 @
    ( match m.exit_label with
        | None -> []
        | Some xlab ->
            (* generate a new exit label and do all exports there: *)
            instr4 @ instr2p @ instr2l @ instr3 @ instr5 xlab
    )

and transl_export_privars m_pre m_post privars =
  List.flatten
    (List.map
       (fun var ->
         match SMap.find var m_post.privars with
           | (reg_from, Current) ->
               ( match SMap.find var m_pre.privars with
                   | (reg_to, scope_to) ->
                       assert(scope_to = Current);
                       [ Vm.I_move { reg=reg_to; src_reg=reg_from } ]
                   | exception Not_found ->
                       match SMap.find var m_pre.privenv with
                         | { env_rd; env_wr = None } ->
                             assert false
                         | { env_rd; env_wr = Some envidx } ->
                             [ Vm.I_envmove { envidx; input=reg_from } ]
                         | exception Not_found ->
                             assert false
               )
           | (_, Outer) ->  (* silently ignore this *)
               []
           | _ ->
               compile_error ("Cannot export private variable: " ^ var)
           | exception Not_found ->
               compile_error ("Cannot export private variable: " ^ var)
       )
       privars
    )

and transl_export_locvars m_pre m_post locvars =
  List.flatten
    (List.map
       (fun var ->
         match SMap.find var m_post.locvars with
           | (reg_from, Current, _) ->
               ( match SMap.find var m_pre.locvars with
                   | (reg_to, scope_to, is_ref) ->
                       assert(scope_to = Current);
                       if is_ref then
                         [ Vm.I_mutate { reg=reg_to; in1=reg_from } ]
                       else
                         [ Vm.I_move { reg=reg_to; src_reg=reg_from } ]
                   | exception Not_found ->
                       match SMap.find var m_pre.locenv with
                         | { env_rd; env_wr = None; _ } ->
                             assert false
                         | { env_rd; env_wr = Some envidx; env_ref } ->
                             if env_ref then
                               let tmpreg, _ = new_reg m_post in
                               [ Vm.I_getenv { reg=tmpreg; envidx=env_rd };
                                 Vm.I_mutate { reg=tmpreg; in1=reg_from }
                               ]
                             else
                               [ Vm.I_envmove { envidx; input=reg_from } ]
                         | exception Not_found ->
                             assert false
               )
           | (_, Outer, _) ->  (* silently ignore this *)
               []
           | _ ->
               compile_error ("Cannot export private variable: " ^ var)
           | exception Not_found ->
               compile_error ("Cannot export private variable: " ^ var)
       )
       locvars
    )

and transl_export_glb m_pre m_post =
  [ Vm.I_move { reg=m_pre.glb_reg; src_reg=m_post.glb_reg } ]

and transl_export_globals m_pre m_post globals =
  let tmpreg, m = new_reg m_post in
  List.flatten
    (List.map
       (fun var ->
         let slot = Lookup_table.find_slot var VT.(m.alloc.alloc_variables) in
         [ Vm.I_getglobalvar { reg=tmpreg; slot; glbreg=m_post.glb_reg };
           Vm.I_addglobalvar { reg=tmpreg; slot; glbreg=m_pre.glb_reg };
         ]
       )
       globals
    )

and transl_stlist m stlist =
  let (rev_instr,m) =
    List.fold_left
      (fun (rev_instr, m) st ->
        let instr, m = transl_st m st in
        (List.rev_append instr rev_instr, m)
      )
      ([], m)
      stlist in
  (List.rev rev_instr, m)

and transl_st m st =
  match st with
    | Let { var; vartype = Some P; expr } ->
        transl_let_private m var expr
    | Let { var; vartype = Some L; expr } ->
        transl_let_local m var expr
    | Let { var; vartype = Some G; expr } ->
        transl_let_global m var expr
    | Let { var; vartype = Some S; expr } ->
        assert false (* TODO *)
        (*
        let slot = shared_slot m var in
        let treg0 = temp_regs rm 1 in
        let instr = transl_expr rm env treg0 expr in
        let instr =
          instr @
            [ Vm.I_addsharedvar { reg=treg0; glbreg=rm.glb_reg; slot }
            ] in
        (instr, m)
         *)
    | Let { var; vartype = Some T; expr } ->
        assert false   (* is rewritten to something else *)
    | Let { vartype = Some Prim; _ } ->
        failwith "Omake_ir.Compiler: cannot create bindings for primitives"
    | Let { vartype = None; _ } ->
        assert false
    | Declare _ ->
        ([], m)
    | Qualify _ -> (* already removed *)
        assert false
    | Section block ->
        let instr1, m = transl_materialize_staticvars m [block] in
        let instr2 = transl_block m block in
        (instr1 @ instr2, m)
    | Value expr ->
        let blocks = blocks_of_expr expr in
        let instr1, m = transl_materialize_staticvars m blocks in
        let instr2 = transl_expr m expr in
        (instr1 @ instr2, m)
    | Effect expr ->
        let reg, m = new_reg m in
        let instr0 = [ Vm.I_move { reg; src_reg=m.val_reg } ] in
        let blocks = blocks_of_expr expr in
        let instr1, m = transl_materialize_staticvars m blocks in
        let instr2 = transl_expr m expr in
        let instr3 = [ Vm.I_move { reg=m.val_reg; src_reg=reg } ] in
        (instr0 @ instr1 @ instr2 @ instr3, m)
    | Return ->
        ( match m.exit_label with
            | None ->
                failwith "Omake_ir.Compiler: cannot return outside function"
            | Some lab ->
                let instr =
                  [ Vm.I_jump { jump = Vm.Lab lab } ] in
                (instr, m)
        )
    | Extstm (Copy_field {var; src_var; name}) ->
        transl_copy_field m var src_var name
    | Extstm _ ->
        failwith "Omake_ir.Compiler: unknown extension"

and transl_let_private m var expr =
  let reg, m =
    try
      let reg, scope = SMap.find var m.privars in
      if scope <> Current then raise Not_found;
      (reg, m)
    with Not_found -> new_reg m in
  let blocks = blocks_of_expr expr in
  let instr1, m = transl_materialize_staticvars m blocks in
  let instr2 = transl_expr m expr in
  let instr3 = [ Vm.I_move { reg; src_reg=m.val_reg } ] in
  let privars = SMap.add var (reg,Current) m.privars in
  let m = { m with privars } in
  (instr1 @ instr2 @ instr3, m)

and transl_let_local m var expr =
  let blocks = blocks_of_expr expr in
  let instr1, m = transl_materialize_staticvars m blocks in
  let instr2 = transl_expr m expr in
  let instr3, m = transl_let_local_reg m var m.val_reg in
  (instr1 @ instr2 @ instr3, m)

and transl_let_local_reg m var expr_reg =
  let reg, m =
    try
      let reg, scope, is_ref = SMap.find var m.locvars in
      if scope <> Current then raise Not_found;
      (reg, m)
    with Not_found -> new_reg m in
  let locvars = SMap.add var (reg,Current,false) m.locvars in
  let m = { m with locvars } in
  let instr = [ Vm.I_move { reg; src_reg=expr_reg } ] in
  (instr, m)

and transl_let_global m var expr =
  let slot = global_slot m var in
  let blocks = blocks_of_expr expr in
  let instr1, m = transl_materialize_staticvars m blocks in
  let instr2 = transl_expr m expr in
  let instr3 =
      [ Vm.I_addglobalvar { reg=m.val_reg; glbreg=m.glb_reg; slot }
      ] in
  (instr1 @ instr2 @ instr3, m)

and transl_copy_field m var src_var name =
  let src_reg, src_instr, m1, _ = transl_local_var m src_var in
  let dest_reg, dest_instr, m1, _ = transl_local_var m1 var in
  let tmpreg, m1 = new_reg m1 in
  let instr1 =
    [ Vm.I_objget { reg=tmpreg; in1=src_reg; name; cls=None };
      Vm.I_update { reg=tmpreg; in1=dest_reg; name; in2=tmpreg }
    ] in
  let instr2, _ = transl_let_local_reg m1 var tmpreg in
  (instr1 @ instr2, m)

and transl_materialize_staticvars m blocks =
  let instr1, m = transl_materialize_these_staticvars P m blocks in
  let instr2, m = transl_materialize_these_staticvars L m blocks in
  (instr1 @ instr2, m)

and transl_materialize_these_staticvars vartype m blocks =
  (* We need to create exported private and local variables in m (so far not yet
     existing) because export_privars expects this
   *)
  assert(vartype = P || vartype = L);
  let exp_vars1 =
    List.fold_left
      (fun acc block ->
        let vars =
          List.map
            fst
            (List.filter (fun (_,vt) -> vt = Some vartype) block.export) in
        SSet.union acc (SSet.of_list vars)
      )
      SSet.empty
      blocks in
  let exp_vars =
    SSet.diff
      exp_vars1
      (domain
         (SMap.filter
            (fun _ (_, scope) -> scope = Current)
            (if vartype=P then m.privars
             else SMap.map (fun (reg,scope,_) -> (reg,scope)) m.locvars
            )
         )
      ) in
  let stlist =
    List.map
      (fun var ->
        if not (var_exists m var vartype) then
          compile_error ("Variable is exported to outer scope but does not \
                          exist there: " ^ var);
        let expr = Apply { var; vartype=Some vartype; named=[]; anon=[] } in
        Let { var; vartype=Some vartype; expr }
      )
      (SSet.elements exp_vars) in
    transl_stlist m stlist

and transl_expr m expr =
  match expr with
    | Abstract _ ->
        transl_abstract m expr
    | Apply _ ->
        transl_apply m expr
    | Thread _ ->
        transl_thread m expr
    | LetLocal _ ->
        transl_letlocal m expr
    | If _ ->
        transl_if m expr
    | Try _ ->
        transl_try m expr
    | Lazy _ ->
        transl_lazy m expr
    | IsUninitialized _ ->
        transl_isuninitialized m expr
    | Literal _ ->
        transl_literal m expr
    | Raise _ ->
        transl_raise m expr
    | CaughtValue ->
        transl_caughtvalue m
    | Coerce _ ->
        transl_coerce m expr
    | Defined _ ->
        transl_defined m expr
    | MultiOp _ ->
        transl_multiop m expr
    | MonOp _ ->
        transl_monop m expr
    | BinOp _ ->
        transl_binop m expr
    | Block b ->
        transl_block m b
    | Object _ ->
        transl_object m expr
    | Exemplar _ ->
        assert false    (* is rewritten to something else *)
    | Field _ ->
        transl_field m expr
    | Extexpr(Abstract_local p) ->
        let expr =
          Abstract { named=p.named; anon=p.anon; arity_min=p.arity_min;
                     autocurry=p.autocurry; body=p.body
                   } in
        transl_abstract ~local_args:true m expr
    | Extexpr(Update_obj _) ->
        transl_update_obj m expr
    | Extexpr(Extend_obj _) ->
        transl_extend_obj m expr
    | Extexpr _ ->
        failwith "Omake_ir.Compiler: unknown extension"


and transl_expr_to m reg expr =
  transl_expr m expr
  @ (if reg=m.val_reg then
       []
     else
       [ Vm.I_move { reg; src_reg=m.val_reg } ]
    )

and transl_forced_expr_to m need_force reg expr =
  transl_expr_to m reg expr
  @ (if need_force then
       transl_force m reg
     else
       []
    )

and var_exists m name vartype =
  match vartype with
    | P ->
        SMap.mem name m.privars ||
          SMap.mem name m.privenv
    | L ->
        SMap.mem name m.locvars ||
          SMap.mem name m.locenv
    | G ->
        true
    | S ->
        assert false
    | T ->
        var_exists m "this" L
    | _ ->
        assert false

and transl_var m name vartype =
  match vartype with
    | P ->
        ( match SMap.find name m.privars with
            | (reg, scope) ->
                (reg, [], m)
            | exception Not_found ->
                ( match SMap.find name m.privenv with
                    | env ->
                        let reg, m = new_reg m in
                        (reg, [ Vm.I_getenv { reg; envidx=env.env_rd } ], m)
                    | exception Not_found ->
                        compile_error ("Private variable not found: " ^ name)
                )
        )
    | L ->
        let reg, instr, m, _ = transl_local_var m name in
        (reg, instr, m)
    | G ->
        let slot = Lookup_table.add_slot name VT.(m.alloc.alloc_variables) in
        let reg, m = new_reg m in
        let instr =
          [ Vm.I_getglobalvar { reg; slot; glbreg=m.glb_reg } ] in
        (reg, instr, m)
    | S ->
        assert false (* TODO *)
    | T ->
        let subst =
          Field { this=Apply { var="this"; vartype=Some L;
                               named=[]; anon=[] };
                  cls=None;
                  name
                } in
        let instr1 = transl_expr m subst in
        let reg, m = new_reg m in
        let instr =
          instr1 @
            [ Vm.I_move { reg; src_reg=m.val_reg } ] in
        (reg, instr, m)
    | Prim ->
        compile_error "Cannot access a primitive as variable"

and transl_local_var m name =
  match SMap.find name m.locvars with
    | (reg, scope, is_ref) ->
        (reg, [], m, is_ref)
    | exception Not_found ->
        ( match SMap.find name m.locenv with
            | env ->
                let reg, m = new_reg m in
                (reg, [ Vm.I_getenv { reg; envidx=env.env_rd } ], m,
                 env.env_ref)
            | exception Not_found ->
                compile_error ("Local variable not found: " ^ name)
        )

and transl_force m reg =
  let tmpreg, _ = new_reg m in
  [ Vm.I_force1 { reg; tmpreg; glbreg=m.glb_reg };
    Vm.I_force2 { reg; tmpreg; glbreg=m.glb_reg };
  ]


and transl_abstract ?(local_args=false) m expr =
  match expr with
    | Abstract p ->
        if p.arity_min < 0 || p.arity_min > List.length p.anon then
          invalid_arg "transl_abstract";
        if p.arity_min = 0 && p.named = [] then
          compile_error ("Function doesn't require parameters");
        ( match double_occurrences (p.named @ p.anon) with
            | [] -> ()
            | l ->
                compile_error ("Parameters occur twice: " ^
                                 String.concat ", " l)
        );
        let body = p.body in
        (* Prepare the environment of private variables: *)
        let exported_privars =
          SSet.of_list
            (List.map
               fst
               (List.filter (fun (n,vt) -> vt=Some P) body.export)) in
        let surround_privars =
          SSet.union (domain m.privars) (domain m.privenv) in
        let body_privars =
          if local_args then
            surround_privars
          else
            List.fold_left
              (fun set param -> SSet.remove param set)
              surround_privars
              (p.named @ p.anon) in
        let used_privars =
          used_private_vars_of_block body_privars body in
        let all_privars =
          (* these need to be put into the environment: *)
          SSet.union used_privars exported_privars in
        (* Prepare the environment of local variables: *)
        let exported_locvars =
          SSet.of_list
            (List.map
               fst
               (List.filter (fun (n,vt) -> vt=Some L) body.export)) in
        let surround_locvars =
          SSet.union (domain m.locvars) (domain m.locenv) in
        let body_locvars =
          if local_args then
            List.fold_left
              (fun set param -> SSet.remove param set)
              surround_locvars
              (p.named @ p.anon)
          else
            surround_locvars in
        let used_locvars =
          used_local_vars_of_block body_locvars body in
        let all_locvars =
          (* these need to be put into the environment: *)
          SSet.union used_locvars exported_locvars in
        (* Arrange the environment of private variables: *)
        let privenv_info, env_regs, env_len, m =
          List.fold_left
            (fun (infos, regs, i, m) var ->
              let (reg_rd, instr_rd, m) =
                transl_var m var P in
              if SSet.mem var exported_privars then
                let reg_wr, m = new_reg m in
                let instr_wr = [ Vm.I_envind { reg=reg_wr; indreg=reg_rd } ] in
                let e = { env_rd = i; env_wr = Some(i+1); env_ref = false } in
                ( (var,e,instr_rd@instr_wr) :: infos,
                  reg_wr :: reg_rd :: regs,
                  i+2,
                  m
                )
              else
                let e = { env_rd = i; env_wr = None; env_ref = false } in
                ( (var,e,instr_rd) :: infos, reg_rd :: regs, i+1, m )
            )
            ([], [], 0, m)
            (SSet.elements all_privars) in
        assert(env_len = List.length env_regs);
        (* Arrange the environment of local variables: *)
        let locenv_info, env_regs, env_len, m =
          List.fold_left
            (fun (infos, regs, i, m) var ->
              let (reg_rd, instr_rd, m, env_ref) =
                transl_local_var m var in
              if SSet.mem var exported_locvars then
                let reg_wr, m = new_reg m in
                let instr_wr = [ Vm.I_envind { reg=reg_wr; indreg=reg_rd } ] in
                let e = { env_rd = i; env_wr = Some(i+1); env_ref } in
                ( (var,e,instr_rd@instr_wr) :: infos,
                  reg_wr :: reg_rd :: regs,
                  i+2,
                  m
                )
              else
                let e = { env_rd = i; env_wr = None; env_ref } in
                ( (var,e,instr_rd) :: infos, reg_rd :: regs, i+1, m )
            )
            ([], env_regs, env_len, m)
            (SSet.elements all_locvars) in
        assert(env_len = List.length env_regs);
        (* Extract from that the registers and instructions: *)
        let env_regs = List.rev env_regs in
        let env_reg, m = new_reg m in
        let env_instrs =
          List.flatten
            (List.map (fun (_,_,instr) -> instr) privenv_info)
          @ List.flatten
              (List.map (fun (_,_,instr) -> instr) locenv_info)
          @ [ Vm.I_env { reg=env_reg; inputs=Array.of_list env_regs } ] in
        (* What needs to be passed down to the function translator: *)
        let body_privenv =
          List.fold_left
            (fun map (var,e,_) -> SMap.add var e map)
            SMap.empty
            privenv_info in
        let body_locenv =
          List.fold_left
            (fun map (var,e,_) -> SMap.add var e map)
            SMap.empty
            locenv_info in
        let clos_slot =
          transl_function
            ~local_args
            m body_privenv body_locenv
            p.named p.anon p.arity_min p.autocurry body in
        let closreg, m = new_reg m in
        env_instrs @
          [ Vm.I_getsharedfun { reg=closreg; glbreg=m.glb_reg; slot=clos_slot };
            Vm.I_closure { reg=m.val_reg; closreg; env=env_reg }
          ]
    | _ ->
        assert false

and transl_function ?(local_args=false)
                    m privenv locenv named anon arity_min autocurry block =
  let arity_names = List.length named in
  let arity_max = List.length anon in
  let param_regs =
    (List.mapi
       (fun i param -> (i+1, param))
       named
    ) @ (List.mapi
           (fun i param -> (i+1+arity_names, param))
           anon
        ) in
  let privars =
    if local_args then
      SMap.empty
    else
      List.fold_left
        (fun map (reg,param) -> SMap.add param (reg, Current) map)
        SMap.empty
        param_regs in
  let locvars =
    if local_args then
      List.fold_left
        (fun map (reg,param) -> SMap.add param (reg, Current, false) map)
        SMap.empty
        param_regs
    else
      SMap.empty in
  let framesize = arity_names + arity_max + 2 in
  let val_reg = arity_names + arity_max + 1 in
  let m_sub =
    { free_reg = framesize;
      num_reg = ref framesize;
      num_lab = ref 0;
      glb_reg = 0;
      privars;
      locvars;
      privenv;
      locenv;
      val_reg;
      catch_reg = None;
      exit_label = Some "exit";
      trap_labels = [];
      num_funs = m.num_funs;
      alloc = m.alloc;
      shared = m.shared;
      prims = m.prims;
    } in
  let instr = transl_block m_sub block in
  let instr =
    [ Vm.I_function { arity_min; arity_max; arity_names;
                      framesize = !(m_sub.num_reg) };
      Vm.I_load { reg=val_reg; value=VT.(valuebox R_seq Seq.empty) };
    ] @
      instr @
        [ Vm.I_label "exit";
          Vm.I_return { reg=val_reg; glbreg=m_sub.glb_reg };
        ] in
  let instr =
    Vm.relocate (Array.of_list instr) in
  let fun_name = sprintf "Lfun_%d" !(m.num_funs) in   (* FIXME *)
  incr m.num_funs;
  let clos =
    Vm.closure
      ~entry:0
      ~arity_min
      ~arity_max
      ~autocurry
      ~names:(Array.of_list named)
      fun_name
      (Vm.code m.alloc instr) in
  m.shared.VT.shared_functions <-
    VT.(Lookup_table.add clos.clos_slot (Some clos) m.shared.shared_functions);
  VT.(clos.clos_slot)

and prep_call_anon m anon =
  let in_anon, m =
    List.fold_left
      (fun (acc, m) expr ->
        let reg, m = new_reg m in
        let instr = transl_expr_to m reg expr in
        ((instr, reg) :: acc, m)
      )
      ([], m)
      anon in
  let in_anon = List.rev in_anon in
  let in_anon_instr = List.map fst in_anon in
  let in_anon_regs = List.map snd in_anon in
  let in_anon_regs = Array.of_list in_anon_regs in
  (in_anon_regs, List.flatten in_anon_instr, m)

and prep_call m var vartype named anon =
  let in_named, m =
    List.fold_left
      (fun (acc, m) (name, expr) ->
        let reg, m = new_reg m in
        let instr = transl_expr_to m reg expr in
        ((name, (instr, reg)) :: acc, m)
      )
      ([], m)
      named in
  let in_named = List.rev in_named in
  let in_named_instr =
    List.map (fun (_, (instr, _)) -> instr) in_named in
  let in_named_regs =
    List.map (fun (name, (_, reg)) -> (name,reg)) in_named in
  let in_named_regs =
    List.sort (fun (n1,_) (n2,_) -> String.compare n1 n2)
              in_named_regs in
  let in_named_regs =
    Array.of_list in_named_regs in
  let in_anon_regs, in_anon_instr, m = prep_call_anon m anon in
  let closreg, closinstr, m = transl_var m var vartype in
  let forceinstr = transl_force m closreg in
  let instr =
    List.flatten in_named_instr @
      in_anon_instr @
        closinstr @
          forceinstr in
  (instr, closreg, in_named_regs, in_anon_regs)


and transl_apply m expr =
  match expr with
    | Apply p ->
        let vartype =
          match p.vartype with
            | None -> assert false
            | Some vt -> vt in
        if p.named=[] && p.anon=[] then (
          let (reg, instr, m) = transl_var m p.var vartype in
          let instr =
            instr @ [ Vm.I_move { reg=m.val_reg; src_reg=reg } ] in
          instr
        ) else (
          match vartype with
            | T ->
                let subst =
                  LetLocal
                    { var="field";
                      expr=Apply { p with named=[]; anon=[] };
                      inexpr=Apply { var="field";
                                     vartype=Some L;
                                     named=p.named;
                                     anon=p.anon
                                   }
                    } in
                transl_expr m subst
            | Prim ->
                if p.named <> [] then
                  compile_error "Primitive does not expect named arguments";
                let (in_anon_regs, in_anon_instr, m) =
                  prep_call_anon m p.anon in
                let prim =
                  try SMap.find p.var m.prims
                  with Not_found ->
                       compile_error ("No such primitive: " ^ p.var) in
                in_anon_instr @
                  [ Vm.I_primitive { prim; reg=m.val_reg;
                                     args=in_anon_regs;
                                     glbreg=m.glb_reg }
                  ]
            | _ ->
                (* TODO: tail-recursion *)
                let (instr, closreg, in_named_regs, in_anon_regs) =
                  prep_call m p.var vartype p.named p.anon in
                instr @
                  [ Vm.I_apply { reg=m.val_reg; closreg;
                                 named=in_named_regs; args=in_anon_regs;
                                 glbreg=m.glb_reg }
                  ]
        )
    | _ ->
        assert false

and transl_letlocal m expr =
  match expr with
    | LetLocal p ->
        let instr1 = transl_expr m p.expr in
        let r, m = new_reg m in
        let instr2 = [ Vm.I_move { reg=r; src_reg=m.val_reg } ] in
        let m =
          { m with
            locvars = SMap.add p.var (r, Current, false) m.locvars
          } in
        let instr3 = transl_expr m p.inexpr in
        instr1 @ instr2 @ instr3
    | _ ->
        assert false

and transl_thread m expr =
  match expr with
    | Thread p ->
        let vartype =
          match p.vartype with
            | None -> assert false
            | Some vt -> vt in
        if p.named=[] && p.anon=[] then
          compile_error "Thread body needs to be a function";
        let (instr, closreg, in_named_regs, in_anon_regs) =
          prep_call m p.var vartype p.named p.anon in
        instr @
          [ Vm.I_thread { reg=m.val_reg; closreg;
                          named=in_named_regs; args=in_anon_regs;
                          glbreg=m.glb_reg }
          ]
    | _ ->
        assert false

and transl_lazy m expr =
  match expr with
    | Lazy e ->
        let body =
          { st = [ Value(Defined e) ];
            export = [];
            export_glb = false;
            export_obj = false;
          } in
        let instr =
          transl_abstract
            m
            (Abstract { named=[]; anon=[""]; arity_min=1;
                        autocurry=true; body }) in
        (* FIXME: prevent that the value of the variable "" can be
           accessed (i.e. filter out any variables with empty name)
         *)
        instr @
          [ Vm.I_lazy { reg=m.val_reg; input=m.val_reg } ]
    | _ ->
        assert false

and transl_if m expr =
  match expr with
    | If p ->
        let cond_instr = transl_expr m p.cond in
        let cond_force_instr = transl_force m m.val_reg in
        let ontrue_label = new_label m in
        let ontrue_instr = transl_expr m p.ontrue in
        let onfalse_instr = transl_expr m p.onfalse in
        let onfalse_label = new_label m in
        let cont_label = new_label m in
        cond_instr @ cond_force_instr @
          [ Vm.I_if { in1=m.val_reg; ontrue=Vm.Lab ontrue_label;
                      onfalse=Vm.Lab onfalse_label };
            Vm.I_label ontrue_label;
          ] @
            ontrue_instr @
              [ Vm.I_jump { jump=Vm.Lab cont_label };
                Vm.I_label onfalse_label;
              ] @
                onfalse_instr @
                  [ Vm.I_label cont_label ]
    | _ ->
        assert false

and transl_try m expr =
  match expr with
    | Try p ->
        let reraise_reg, m = new_reg m in
        let trap_lab = new_label m in
        let m_trial =
          { m with
            trap_labels = trap_lab :: m.trap_labels;
          } in
        let trial_instr = transl_expr m_trial p.trial in
        let fin_trap_lab = new_label m in
        let catch_reg, m_catch = new_reg m in
        let m_catch =
          { m_catch with
            catch_reg = Some catch_reg;
            trap_labels = fin_trap_lab :: m.trap_labels;
          } in
        let catch_instr = transl_expr m_catch p.catch in
        let save_reg, m_fin = new_reg m in
        let finally_instr = transl_expr m_fin p.finally in
        let finally_label = new_label m in
        let cont_label = new_label m in
        let reset_trap =
          match m.trap_labels with
            | [] ->
                Vm.I_deltrap
            | tlab :: _ ->
                Vm.I_settrap { reg=m.val_reg; handler=Vm.Lab tlab } in
        let reraise_exn =
          match m.trap_labels with
            | [] ->
                Vm.I_raise { reg=m.val_reg }
            | tlab :: _ ->
                Vm.I_jump { jump=Vm.Lab tlab } in
        [ Vm.I_settrap { reg=m.val_reg; handler=Vm.Lab trap_lab };
        ] @
          trial_instr @
            [ Vm.I_load { reg=reraise_reg; value=VT.(valuebox R_bool false) };
              Vm.I_jump { jump=Vm.Lab finally_label };
              Vm.I_label trap_lab;
              Vm.I_move { reg=catch_reg; src_reg=m.val_reg };
              Vm.I_settrap { reg=m.val_reg; handler=Vm.Lab fin_trap_lab };
            ] @
              catch_instr @
                [ Vm.I_load { reg=reraise_reg; value=VT.(valuebox R_bool false)};
                  Vm.I_jump { jump=Vm.Lab finally_label };
                  Vm.I_label fin_trap_lab;
                  Vm.I_load { reg=reraise_reg; value=VT.(valuebox R_bool true)};
                  Vm.I_label finally_label;
                  reset_trap;
                  Vm.I_move { reg=save_reg; src_reg=m.val_reg }
                ] @
                  finally_instr @
                    [ Vm.I_move { reg=m.val_reg; src_reg=save_reg };
                      Vm.I_if { in1=reraise_reg; ontrue=Vm.Rel 1;
                                onfalse=Vm.Lab cont_label };
                      reraise_exn;
                      Vm.I_label cont_label;
                    ]
    | _ ->
        assert false

and transl_raise m expr =
  match expr with
    | Raise e ->
        transl_expr m e @
          transl_force m m.val_reg @
            ( match m.trap_labels with
                | [] ->
                    [ Vm.I_raise { reg=m.val_reg } ]
                | tlab :: _ ->
                    [ Vm.I_jump { jump=Vm.Lab tlab } ]
            )
    | _ ->
        assert false

and transl_caughtvalue m =
  match m.catch_reg with
    | Some r ->
        [ Vm.I_move { reg=m.val_reg; src_reg=r } ]
    | None ->
        compile_error "can use CaughtValue only within an exception handler"
                      
and transl_isuninitialized m expr =
  match expr with
    | IsUninitialized e ->
        let instr = transl_expr m e in
        instr @
          [ Vm.I_isuninit { reg=m.val_reg; in1=m.val_reg }]
    | _ ->
        assert false

and transl_coerce m expr =
  match expr with
    | Coerce p ->
        let need_force = expr_might_be_lazy p.arg in
        transl_forced_expr_to m need_force m.val_reg p.arg
        @ [ Vm.I_coerce
              { reg=m.val_reg; input=m.val_reg; utype=p.usertype }
          ]
    | _ ->
        assert false

and transl_defined m expr =
  match expr with
    | Defined e ->
        let reg, m = new_reg m in
        let lab = new_label m in
        let need_force = expr_might_be_lazy e in
        transl_forced_expr_to m need_force m.val_reg e
        @ [ Vm.I_isuninit { reg=reg; in1=m.val_reg };
            Vm.I_if {in1=reg; ontrue=Vm.Rel 1; onfalse=Vm.Lab lab};
            Vm.I_load {reg; value=VT.valuebox VT.R_string "undefined value"};   (* FIXME: use a real exception *)
            Vm.I_raise {reg};
            Vm.I_label lab
          ]
    | _ ->
        assert false

and transl_monop m expr =
  match expr with
    | MonOp p ->
        let need_force = expr_might_be_lazy p.arg in
        transl_forced_expr_to m need_force m.val_reg p.arg
        @ ( match p.monop with
              | MQuote qchar ->
                  [ Vm.I_quote { qchar; reg=m.val_reg; input=m.val_reg } ]
              | MLength ->
                  [ Vm.I_length { reg=m.val_reg; input=m.val_reg } ]
              | MWait ->
                  [ Vm.I_wait { reg=m.val_reg; handle=m.val_reg } ]
              | MTest ->
                  [ Vm.I_test { reg=m.val_reg; handle=m.val_reg } ]
              | MArraySeq ->
                  [ Vm.I_array_seq { reg=m.val_reg; input=m.val_reg } ]
          )
    | _ ->
        assert false

and transl_binop m expr =
  match expr with
    | BinOp p ->
        let need_force1 = expr_might_be_lazy p.arg1 in
        let need_force2 = expr_might_be_lazy p.arg2 in
        let r1, m = new_reg m in
        transl_forced_expr_to m need_force1 r1 p.arg1 @
          transl_forced_expr_to m need_force2 m.val_reg p.arg2 @
            ( match p.binop with
                | BNth ->
                    [ Vm.I_nth { reg=m.val_reg; in1=r1; in2=m.val_reg } ]
                | BAdd ->
                    [ Vm.I_add { reg=m.val_reg; in1=r1; in2=m.val_reg } ]
                | BSub ->
                    [ Vm.I_sub { reg=m.val_reg; in1=r1; in2=m.val_reg } ]
                | BMul ->
                    [ Vm.I_mul { reg=m.val_reg; in1=r1; in2=m.val_reg } ]
                | BDiv ->
                    [ Vm.I_div { reg=m.val_reg; in1=r1; in2=m.val_reg } ]
                | BMod ->
                    [ Vm.I_mod { reg=m.val_reg; in1=r1; in2=m.val_reg } ]
                | BEq ->
                    [ Vm.I_eq { reg=m.val_reg; in1=r1; in2=m.val_reg } ]
                | BNe ->
                    [ Vm.I_ne { reg=m.val_reg; in1=r1; in2=m.val_reg } ]
                | BLt ->
                    [ Vm.I_lt { reg=m.val_reg; in1=r1; in2=m.val_reg } ]
                | BLe ->
                    [ Vm.I_le { reg=m.val_reg; in1=r1; in2=m.val_reg } ]
            )
    | _ ->
        assert false

and transl_multiop m expr =
  match expr with
    | MultiOp p ->
        let regs, m =
          List.fold_left
            (fun (regs, m) _ ->
              let reg, m = new_reg m in
              (reg::regs, m)
            )
            ([], m)
            p.args in
        let regs = List.rev regs in
        let instrl =
          List.map2
            (fun r e ->
              let need_force = expr_might_be_lazy e in
              transl_forced_expr_to m need_force r e
            )
            regs p.args in
        let regsa = Array.of_list regs in
        List.flatten instrl @
          ( match p.multiop with
              | NSeq ->
                  [ Vm.I_seq { reg=m.val_reg; inputs=regsa } ]
              | NArray ->
                  [ Vm.I_array { reg=m.val_reg; inputs=regsa } ]
              | NConcat ->
                  [ Vm.I_concat { reg=m.val_reg; inputs=regsa } ]
          )
    | _ ->
        assert false

and transl_literal m expr =
  match expr with
    | Literal lit ->
        [ Vm.I_load { reg=m.val_reg; value=VT.vbox_of_stringval lit } ]
    | _ ->
        assert false


and transl_object m expr =
  match expr with
    | Object cls ->
        [ Vm.I_object { reg=m.val_reg; cls } ]
    | _ ->
        assert false


and transl_field m expr =
  match expr with
    | Field p ->
        let need_force = expr_might_be_lazy p.this in
        let instr_this =
          transl_forced_expr_to m need_force m.val_reg p.this in
        let tmpreg, m = new_reg m in
        let instr_lookup =
          [ Vm.I_objget { reg=tmpreg; in1=m.val_reg; name=p.name; cls=p.cls }
          ] in
        let instr_latebinding =
          [ Vm.I_apply { reg=m.val_reg; closreg=tmpreg;
                         named=[||]; args=[|m.val_reg|]; glbreg=m.glb_reg } ] in
        instr_this @
          instr_lookup @
            instr_latebinding
    | _ ->
        assert false

and transl_update_obj m expr =
  match expr with
    | Extexpr(Update_obj p) ->
        let need_force = expr_might_be_lazy p.expr in
        let instr_expr =
          transl_forced_expr_to m need_force m.val_reg p.expr in
        let var_reg, var_instr, _, _ = transl_local_var m p.var in
        instr_expr @
          var_instr @
            [ Vm.I_update{reg=m.val_reg; in1=var_reg; name=p.name;
                          in2=m.val_reg}
            ]
    | _ ->
        assert false

and transl_extend_obj m expr =
  match expr with
    | Extexpr(Extend_obj p) ->
        let need_force = expr_might_be_lazy p.expr in
        let instr_expr =
          transl_forced_expr_to m need_force m.val_reg p.expr in
        let var_reg, var_instr, _, _ = transl_local_var m p.var in
        instr_expr @
          var_instr @
            [ Vm.I_extend{reg=m.val_reg; in1=var_reg; name=p.name;
                          in2=m.val_reg; cls=p.cls}
            ]
    | _ ->
        assert false
               

let create_snippet ?(config = Omake_vm.Config.standard_config) glb =
  let Omake_vm.Types.Config vm_conf = config in
  let vm_prims = Omake_vm.Types.(vm_conf.primitives) in
  let prims =
    Array.fold_left
      (fun acc (i, name,  _) -> SMap.add name i acc)
      SMap.empty
      (Array.mapi (fun i (n, p) -> (i, n, p)) vm_prims) in
  let m =
    { free_reg = 2;
      num_reg = ref 2;
      num_lab = ref 0;
      glb_reg = 0;
      privars = SMap.empty;
      locvars = SMap.empty;
      privenv = SMap.empty;
      locenv = SMap.empty;
      val_reg = 1;
      catch_reg = None;
      exit_label = None;
      trap_labels = [];
      num_funs = ref 0;
      alloc = VT.(glb.shared.alloc);
      shared = VT.(glb.shared);
      prims;
    } in
  { config;
    manager = m;
    regs = [| |];
    code = [| |];
  }

let compile_snippet sn stlist =
  let qual = SMap.map (fun _ -> P) sn.manager.privars in
  let _, _, stlist = qualify_vars_of_stlist qual G stlist in
  let om = { exemplar = None; inobj = false } in
  let stlist = orewrite_stlist om stlist in
  let instr, m = transl_stlist sn.manager stlist in
  sn.manager <- m;
  sn.code <- Array.of_list (instr @ [ Vm.I_endsnippet ])

let v_uninit =
  VT.(valuebox R_uninitialized ())

let run_snippet glb sn =
  if VT.(glb.shared.alloc) != sn.manager.alloc then
    invalid_arg "run_snippet: different allocator than in snippet";
  let instr = Vm.relocate sn.code in
  let code = Vm.code sn.manager.alloc instr in
  let missing_regs = !(sn.manager.num_reg) - Array.length sn.regs in
  let regs = Array.append sn.regs (Array.make missing_regs v_uninit) in
  sn.regs <- regs;
  regs.(0) <- VT.(valuebox R_global glb);
  sn.code <- [| |];
  Vm.run_snippet sn.config glb regs code

let global_of_snippet sn =
  if sn.code <> [| |] then
    failwith "global_of_snippet: code has not yet run";
  VT.global_of_vbox sn.regs.(sn.manager.glb_reg)

let value_of_snippet sn =
  if sn.code <> [| |] then
    failwith "value_of_snippet: code has not yet run";
  sn.regs.(sn.manager.val_reg)

